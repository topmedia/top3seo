<?php
$language['editSwitch'] = '���������� ����������� �� ��������� ��� �������. <a href="#">�������������</a>.';
$language['clear'] = '������� ��� ��������';
$language['confirmclear'] = '�� ������������� ������ ������� ��� ������?';
$language['paste'] = '�������� ������ �� ������� (HTML, Word, CSV)';
$language['pastehere'] = '��������:';
$language['add'] = '��������';
$language['remove'] = '�������';
$language['edit'] = 'Edit';
$language['save'] = '���������';
$language['replace'] = '��������';
$language['append'] = '��������';
$language['cancel'] = '������';
$language['word'] = 'Word/HTML';
$language['google'] = 'Google Docs';
$language['csv'] = 'CSV';
$language['dataTables'] = '{
	"sEmptyTable":     "No data available in table",
    "sInfo":           "������ � _START_ �� _END_ �� _TOTAL_ �������",
    "sInfoEmpty":      "������ � 0 �� 0 �� 0 �������",
    "sInfoFiltered":   "(������������� �� _MAX_ �������)",
    "sInfoPostFix":    "",
    "sInfoThousands":  "",
    "sLengthMenu":     "�������� _MENU_ �������",
    "sLoadingRecords": "Loading �",
    "sProcessing":     "��������� �",
    "sSearch":         "�����:",
    "sZeroRecords":    "������ �����������",
    "oPaginate": {
        "sFirst":    "������",
        "sLast":     "���������",
        "sNext":     "���������",
        "sPrevious": "����������"
    },
    "oAria": {
        "sSortAscending":  ": ������������ ��� ���������� ������� �� �����������",
        "sSortDescending": ": ������������ ��� ���������� �������� �� ��������"
    }
}';
?>
