<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
	'title' => array(
		'caption' => 'Заголовок',
		'type' => 'text'
	),
	'image_unhov' => array(
		'caption' => 'Изображение',
		'type' => 'image'
	),
	'image_hov' => array(
		'caption' => 'При наведении',
		'type' => 'image'
	),
	'text' => array(
		'caption' => 'Текст',
		'type' => 'textarea'
	)
);
$settings['templates'] = array(
	'outerTpl' => '<div id="how_work"><ul><li><a href="#tabs-1" title="">Tab 1</a></li>
			<li><a href="#tabs-2" title="">Tab 2</a></li>
			<li><a href="#tabs-3" title="">Tab 3</a></li><li><a href="#tabs-2" title="">Tab 2</a></li>
			<li><a href="#tabs-3" title="">Tab 3</a></li></ul> <div id="tabs_container">[+wrapper+]</div></div>',
	'tabsTpl' => '<li><a href="#tabs-[+iteration+]" title="">[+title+]</a></li>',
	'rowTpl' => '<div id="tabs-[+iteration+]">[+text+]</div>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false
);
?>
