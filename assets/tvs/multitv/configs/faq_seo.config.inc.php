<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
	'ques' => array(
		'caption' => 'Вопрос',
		'type' => 'text'
	),
	'answ' => array(
		'caption' => 'Ответ',
		'type' => 'textarea'
	)
);
$settings['templates'] = array(
	'outerTpl' => '<div  class="faq_spoiler">[+wrapper+]</div>',
	'rowTpl' => '<div class="question folded open">[+ques+]</div><div class="answer">[+answ+]</div>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false
);
?>
