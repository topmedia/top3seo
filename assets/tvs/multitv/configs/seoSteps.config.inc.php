<?php
$settings['display'] = 'vertical';
$settings['fields'] = array(
	'title' => array(
		'caption' => 'Заголовок блока',
		'type' => 'text'
	),
	'short_text' => array(
		'caption' => 'Основной текст',
		'type' => 'textarea'
	),
	'extended_text' => array(
		'caption' => 'Расширенный текст',
		'type' => 'textarea'
	),
	'button_text' => array(
		'caption' => 'Текст на спойлере',
		'type' => 'text'
	)
);
$settings['templates'] = array(
	'outerTpl' => '<ul class="steps" >[+wrapper+]</ul>',
	'rowTpl' => '<li>
<div class="step">
	<script>
	$(document).ready(function(){
 
    $(\'.spoiler_[+iteration+]\').hide()
     
    $(\'.knob_[+iteration+]\').click(function(){
 
        $(this).next().slideToggle("fast")
        $(this).toggleClass("active");
    })
 
})
	
	</script>
	[+title+]
	<div class="short">[+short_text+]</div>
	<div class="sp">
		<div class="knob_[+iteration+]">
			<span class="more">
				[+button_text+]
			</span>
		</div>
		<div class="spoiler_[+iteration+]">
			[+extended_text+]
		</div>
	</div>
</div>
</li>
	'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false,
	'csvseparator' => ','
);
?>
