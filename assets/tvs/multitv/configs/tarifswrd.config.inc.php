<?php
$settings['display'] = 'horizontal';
$settings['fields'] = array(
	'bonus' => array(
		'caption' => '',
		'type' => 'textarea'
	),/*
	'micro' => array(
		'caption' => '',
		'type' => 'text'
	),*/
	'optima' => array(
		'caption' => '',
		'type' => 'text'
	),
	'business' => array(
		'caption' => '',
		'type' => 'text'
	),
	'premium' => array(
		'caption' => '',
		'type' => 'text'
	)
);
$settings['templates'] = array(
	'outerTpl' => '[+wrapper+]',
	'rowTpl' => '<tr><td>[+bonus+]</td><td>[+phx:if=`[+optima+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+optima+]`+]</td><td>[+phx:if=`[+business+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+business+]`+]</td><td>[+phx:if=`[+premium+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+premium+]`+]</td></tr>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false
);
?>
