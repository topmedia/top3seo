<?php
$settings['display'] = 'horizontal';
$settings['fields'] = array(
	'text' => array(
		'caption' => 'Контакт',
		'type' => 'text'
	),
	'tip' => array(
		'caption' => 'Тип контакта',
		'type' => 'dropdown',
		'elements' => ' ||Мобильный тел==mobile||Городской тел==landline||Е-mail==email' 
	)
);
$settings['templates'] = array(
	'outerTpl' => '<ul class="contacts_list">[+wrapper+]</ul>',
	'rowTpl' => '<li class="[+tip+]">[+text+]</li>'
);
$settings['configuration'] = array(
	'enablePaste' => FALSE,
	'enableClear' => FALSE,
	'csvseparator' => ','
);
?>
