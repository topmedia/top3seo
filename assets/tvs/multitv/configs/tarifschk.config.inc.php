<?php
$settings['display'] = 'horizontal';
$settings['fields'] = array(
	'descr' => array(
		'caption' => '',
		'type' => 'textarea'
	),
	'bonus' => array(
		'caption' => '',
		'type' => 'textarea'
	),
	'micro' => array(
		'caption' => '',
		'type' => 'checkbox',
		'elements' => '&nbsp;==0'
	),/*
	'optima' => array(
		'caption' => '',
		'type' => 'checkbox',
		'elements' => '&nbsp;==0'
	),*/
	'business' => array( 
		'caption' => '',
		'type' => 'checkbox',
		'elements' => '&nbsp;==0'
	),
	'premium' => array(
		'caption' => '',
		'type' => 'checkbox',
		'elements' => '&nbsp;==0'  
	)
);
$settings['templates'] = array(
	'outerTpl' => '[+wrapper+]',
	'rowTpl' => '<tr><td><span class="tooltip" title=\'[+descr+]\'>[+bonus+]</span></td><td>[+phx:if=`[+optima+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`<img src="/images/ico/check.png">`+]</td><td>[+phx:if=`[+business+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`<img src="/images/ico/check.png">`+]</td><td>[+phx:if=`[+premium+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`<img src="/images/ico/check.png">`+]</td></tr>'
);
$settings['configuration'] = array(
	'enablePaste' => false,
	'enableClear' => false
);
?>
