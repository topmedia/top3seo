<?
/* Показатели */
$tic = $obj['tic']; // тИЦ
$pr = $obj['pr']; // PR
$trust = $obj['xt']; //траст
$i_ya = $obj['iny']; //индекс яндекса
$whois_date = $obj['date_create']; //дата регистрации домена
$tic_glue = $obj['cy_glue']; //склейка тИЦ

/* Видимость */
$raskr_ya = $obj['ry']; //раскрученность в Яндексе
	$raskr_ya_nch = $obj['ya_rate']['ryn']; //раскрученность в Яндексе по НЧ
	$raskr_ya_sch = $obj['ya_rate']['rys']; //раскрученность в Яндексе по СЧ
	$raskr_ya_vch = $obj['ya_rate']['ryv']; //раскрученность в Яндексе по ВЧ
$raskr_gl = $obj['rg']; //раскрученность в Google
	$raskr_gl_nch = $obj['go_rate']['rgn']; //раскрученность в Гугле по НЧ
	$raskr_gl_sch = $obj['go_rate']['rgs']; //раскрученность в Гугле по СЧ
	$raskr_gl_vch = $obj['go_rate']['rgv']; //раскрученность в Гугле по ВЧ

/* Санкции */
$spam = $obj['zasp']; //Заспамленность ссылками
$nepot = $obj['nepot']; //вероятность непота (%)
$ags = $obj['ags']; //АГС


/* Solomono */
$solomono_index = $obj['solomono']['index']; //в индексе
$solomono_domens_in = $obj['solomono']['links_in']; //входящих доменов
$solomono_domens_out = $obj['solomono']['links_out']; //исходящих доменов
$solomono_links_in = $obj['solomono']['links_in_all']; //всего входящих ссылок
$solomono_links_out = $obj['solomono']['links_out_all']; //всего исходящих ссылок
$solomono_links_in_main = $obj['solomono']['links_in_main']; //ссылок на главную
$solomono_links_out_main = $obj['solomono']['links_out_main']; //ссылок с главной
$solomono_anchors_in = $obj['solomono']['anchors_in']; //входящих уникальных анкоров
$solomono_anchors_out = $obj['solomono']['anchors_out']; //исходящих уникальных анкоров

/* MajesticSeo */
$mj_index = $obj['majesticseo']['index']; //в индексе
$mj_links_in = $obj['majesticseo']['links_in_all']; //всего входящих ссылок
$mj_domens_in = $obj['majesticseo']['links_in']; //входящих доменов
$mj_ip_in = $obj['majesticseo']['ips']; //входящих ip
$mj_cf = $obj['majesticseo']['cf']; // поток цитирования
$mj_tf = $obj['majesticseo']['tf']; // поток доверия

/* Alexa */
$alexa	 = $obj['alexa']['rank']; //алекса ранк

/* LiveInternet */
$vizity = $obj['liveinternet']['in_day']; //посещений в день (среднемесячное)
$hity = $obj['liveinternet']['in_day_hit']; // просмотров в день (среднемесячное)

/* Каталоги */
$yak = $obj['yac']; //ЯК
$dmoz = get_dmoz_count($_POST['url']); //DMOZ
