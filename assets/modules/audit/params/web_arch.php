<?php

function get_wa($host){

	function get_download($url){
		
		$ret = false;
		
		if( function_exists('curl_init') ){
			if( $curl = curl_init() ){
				
				if( !curl_setopt($curl,CURLOPT_URL,$url) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_RETURNTRANSFER,true) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,30) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_HEADER,false) ) return $ret;
				if( !curl_setopt($curl,CURLOPT_ENCODING,"gzip,deflate") ) return $ret;
				//if( !curl_setopt($curl,CURLOPT_FOLLOWLOCATION,true) ) return $ret; // из чекера дмоз
				
				$ret = curl_exec($curl);
				
				curl_close($curl);
			}
		}
		else{
			$u = parse_url($url);
			
			if( $fp = @fsockopen($u['host'],!empty($u['port']) ? $u['port'] : 80 ) ){
				
			    $headers = 'GET '.  $u['path'] . '?' . $u['query'] .' HTTP/1.0'. "\r\n";
			    $headers .= 'Host: '. $u['host'] ."\r\n";
			    $headers .= 'Connection: Close' . "\r\n\r\n";
				
			    fwrite($fp, $headers);
			    $ret = '';
					
				while( !feof($fp) ){
					$ret .= fgets($fp,1024);
				}
				
				$ret = substr($ret,strpos($ret,"\r\n\r\n") + 4);
				
				fclose($fp);
			}
		}
		
		return $ret;
	}

	$domain = $host;
	
	if( 'www.' == substr($host,0,4) ){
		$domain = substr($host,4);
	}
	
	$url = 'http://web.archive.org/web/*/http://' . $domain;
			
	if( $s = get_download($url)) {
		
		if( preg_match('/<a href="\/web\/(\d+)\/http:\/\/'.$domain.'.*">/i', $s, $a) ){
			
			$date_wa_arr = str_split($a[1]);
			$date_wa = $date_wa_arr[6].$date_wa_arr[7]."-".$date_wa_arr[4].$date_wa_arr[5]."-".$date_wa_arr[0].$date_wa_arr[1].$date_wa_arr[2].$date_wa_arr[3];
			return $date_wa;
		}
		else{
			return 'n/a';
		}
		

	}
	return 'n/a';
}	

?>
