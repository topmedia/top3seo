<?
/*
 * Example YandexXML use
 * Author: wmascat
 * URL: http://www.chuvyr.ru/
 * Article: http://www.chuvyr.ru/2014/01/yandexxml.html
 */

include_once('yandexxml.php');

$yxml = new YandexXML;

$yxml->set_param(array(
	'life_time' => 86400, // cache on 24 hours
	'query' => '<table>',
	'lr' => '213' // Moscow ID
));

$xml = $yxml->request();
$groups = $xml->response->results->grouping->group;
if ( sizeof($groups) > 0 ) {
	$group_num = 1;
	foreach ( $groups as $group ) {
		$docs = $group->doc;
		echo '<p><b>Group #'. $group_num .' have '. sizeof($docs) .' docs.</b></p>';
		if ( sizeof($docs) > 0 ) {
			foreach ( $docs as $doc ) {
				echo '<pre>'; print_r($doc); echo '</pre>';
			}
		}
		$group_num++;
	}
}
