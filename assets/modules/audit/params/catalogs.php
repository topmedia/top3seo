<?
## http://sabotage.name 
## 13.03.2012
## v.0.3 catalogs
function parsecatalogs ($url){
    $cats = array();
    // ЯК
    $page = GetPage("http://yaca.yandex.ru/yca?text=http%3A%2F%2F{$url}&yaca=1", 'http://yaca.yandex.ru/');
    if($page){
        if(preg_match('~Найдено\sпо\sописаниям\sсайтов\s—\s([0-9]+)</div>~iu', $page, $out)) {
            if(trim($out[1]) > 0) $cats['yak'] = '<span style="color:#ff3600;font-weight:bold">Я</span>';
        }
    }
    // Dmoz
    $page = GetPage("http://www.dmoz.org/search?q={$url}", 'http://www.dmoz.org/');
    if($page){
        if(preg_match('~Open Directory Categories~si', $page)) $cats['dmoz'] = '<span style="color:#00dd10;font-weight:bold">D</span>';
    }
    // Рамблер
    $page = GetPage("http://top100.rambler.ru/?query={$url}", 'http://top100.rambler.ru/');
    if(stripos($page, 'href="http://' . $url) !== false) {
        $cats['rambler'] = '<span style="color:#43d1fa;font-weight:bold">Р</span>';
    } else {
        if(substr($page, 0, 4) != 'www.') {
            if(stripos($page, 'href="http://www.' . $url) !== false) {
                $cats['rambler'] = '<span style="color:#43d1fa;font-weight:bold">Р</span>';
            }
        }
    }
    // Mail.ru
    $page = GetPage("http://search.list.mail.ru/?q={$url}", 'http://list.mail.ru/');
    if($page){
        if(stristr($page, '<b class="i">1.</b>')) $cats['mail'] = '<span style="color:#497BAD;font-weight:bold">M</span>';
    }
    return (count($cats) > 0) ? implode(", ",$cats)  : '-';
}

function GetPage ($url, $ref, $conv = false){
    $ua = 'Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.10.229 Version/11.60';
    $headers[] = 'Accept: text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1';
    $headers[] = 'Accept-Language: ru-RU,ru;q=0.9,en;q=0.8';

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_USERAGENT, $ua);
    curl_setopt($ch, CURLOPT_REFERER, $ref);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookies.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookies.txt');
    $r = curl_exec($ch);
    curl_close($ch);
    
    if ($conv) $r = iconv("windows-1251", "utf-8",  $r);
    return $r;
}

?> 