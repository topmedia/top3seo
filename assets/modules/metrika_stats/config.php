<?
$dbname = $modx->db->config['dbase']; //имя базы данных
$dbprefix = $modx->db->config['table_prefix']; //префикс таблиц

//таблицы

$counters = $modx->getFullTableName( 'module_metrika_counters' );
$settings = $modx->getFullTableName( 'module_metrika_settings' );


$statuses_ar = array(
	"CS_ERR_CONNECT"	=>	array("desc" => "Не удалось проверить (ошибка соединения).", "img" => "status-red.png"),
	"CS_ERR_DUPLICATED"	=>	array("desc" => "Установлен более одного раза.", "img" => "status-orange.png"),
	"CS_ERR_HTML_CODE"	=>	array("desc" => "Установлен некорректно.", "img" => "status-orange.png"),
	"CS_ERR_OTHER_HTML_CODE"	=>	array("desc" => "Уже установлен другой счетчик.", "img" => "status-orange.png"),
	"CS_ERR_TIMEOUT"	=>	array("desc" => "Не удалось проверить (превышено время ожидания).", "img" => "status-red.png"),
	"CS_ERR_UNKNOWN"	=>	array("desc" => "Неизвестная ошибка.", "img" => "status-red.png"),
	"CS_NEW_COUNTER"	=>	array("desc" => "Недавно создан.", "img" => "status-orange.png"),
	"CS_NA"	=>	array("desc" => "Не применим к данному счетчику.", "img" => "status-orange.png"),
	"CS_NOT_EVERYWHERE"	=>	array("desc" => "Установлен не на всех страницах.", "img" => "status-green.png"),
	"CS_NOT_FOUND"	=>	array("desc" => "Не установлен.", "img" => "status-red.png"),
	"CS_NOT_FOUND_HOME"	=>	array("desc" => "Не установлен на главной странице.", "img" => "status-red.png"),
	"CS_NOT_FOUND_HOME_LOAD_DATA"	=>	array("desc" => "Не установлен на главной странице, но данные поступают.", "img" => "status-orange.png"),
	"CS_OBSOLETE"	=>	array("desc" => "Установлена устаревшая версия кода счетчика.", "img" => "status-orange.png"),
	"CS_OK"	=>	array("desc" => "Корректно установлен.", "img" => "status-green.png"),
	"CS_OK_NO_DATA"	=>	array("desc" => "Установлен, но данные не поступают.", "img" => "status-green.png"),
	"CS_WAIT_FOR_CHECKING"	=>	array("desc" => "Ожидает проверки наличия.", "img" => "status-orange.png"),
	"CS_WAIT_FOR_CHECKING_LOAD_DATA"	=>	array("desc" => "Ожидает проверки наличия, данные поступают.", "img" => "status-orange.png")
);

$permission_ar = array( 
	"own"	=>	array("desc" => "собственный счетчик пользователя", "title" => "Свой"),
	"view"	=>	array("desc" => "гостевой счетчик с уровнем доступа &laquo;только просмотр&raquo;", "title" => "Гостевой -"),
	"edit"	=>	array("desc" => "гостевой счетчик с уровнем доступа &laquo;полный доступ&raquo;", "title" => "Гостевой +")
);

?>