<?
echo '
			<form name="counters" action="" method="post">
				<table class="grid" cellpadding="1" cellspacing="1">
					<thead>
						<tr>
							<td class="gridHeader" width="5%"></td>
							<td class="gridHeader" width="10%">ID</td>
							<td class="gridHeader" width="30%">Название</td>
							<td class="gridHeader" width="25%">Сайт</td>
							<td class="gridHeader" style="text-align:center; width:5%;">Статус</td>
							<td class="gridHeader" width="10%">Права</td>
							<td class="gridHeader" width="10%">Владелец</td>
						</tr>
					</thead>
					<tbody>';
				$select_counters = $modx->db->select("*", $counters);
				while ($counter = mysql_fetch_array($select_counters)){//выводим записи
					$checked = ($counter['active'] == 0) ? 'checked="checked"' : '';
					echo '
						<tr>
							<td class="gridItem" width="5%"><input type="checkbox" '.$checked.' name="excluded['.$counter['id'].']" value="0" ></td>
							<td class="gridItem" width="10%">'.$counter['id'].'</td>
							<td class="gridItem" width="35%">'.$counter['name'].'</td>
							<td class="gridItem" width="25%">'.$counter['site'].'</td>
							<td class="gridItem" style="text-align:center; width:5%;"><img style="cursor: help;" src="'.$path.'img/'.$statuses_ar[$counter['status']]['img'].'" title="'.$statuses_ar[$counter['status']]['desc'].'"></td> 
							<td class="gridItem" width="10%"><span style="cursor: help;" title="'.$permission_ar[$counter['permission']]['desc'].'">'.$permission_ar[$counter['permission']]['title'].'</td>
							<td class="gridItem" width="10%">'.$counter['login'].'</td>
						</tr>
					';
				$counters_ar[$counter['id']] = $counter;
				}
				unset($counter);
				
			echo '
						<tr>
							<td class="gridItem" width="5%"></td>
							<td class="gridItem" width="10%"></td>
							<td class="gridItem" width="30%"></td>
							<td class="gridItem" width="25%"></td>
							<td class="gridItem" style="text-align:center; width:5%;"></td>
							<td class="gridItem" width="10%"></td>
							<td class="gridItem" width="10%">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<div class="buttons">
					<button type="submit" name="save_counters" value="true" class="save"><span>Сохранить</span></button>
				</div>
				
			</form>
			<script>
					$("#load").addClass("hidden");
			</script>
';

if ($_POST['save_counters']) {
	foreach ($counters_ar as $id => $val) { 
		$active = (isset($_POST['excluded'][$id])) ? $_POST['excluded'][$id] : 1;
		$fields = array(
			"active" => $active
		);
		$query = $modx->db->update($fields, $counters, "id = ".$id."");
		unset($fields);
		unset($active);
	}
	
	header("Location: ".$_SERVER['REQUEST_URI']."");
}

?>