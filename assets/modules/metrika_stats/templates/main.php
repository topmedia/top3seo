<?



echo '

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>Настройка сбора статистики</title>
        <link rel="stylesheet" type="text/css" href="media/style/MODxRE/style.css" /> 
		<link rel="stylesheet" type="text/css" href="'.$path.'css/style.css">
		<link rel="stylesheet" type="text/css" href="'.$path.'css/jquery-ui-1.10.4.custom.min.css">
        <script type="text/javascript" src="media/script/tabpane.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>
		<script type="text/javascript" src="'.$path.'js/jquery-ui-1.10.4.custom.min.js"></script>
		
        <script>
				function refresh_counters() {
					$("#load").removeClass("hidden");
					$("#counters").load("'.$adm_patch.'&action=refresh_counters");
					return false;
				}
				function refresh_statistic() {
					$("#load").removeClass("hidden");
					$("#statistics").load("'.$adm_patch.'&action=refresh_statistic");
					return false;
				}
				function select_method(ids) {
					$(\'#input_periods *\').hide();
					$("#"+ids).css("display", "inline");
				}
		
				$(function() {
					$( "#start_date" ).datepicker({ dateFormat: "dd.mm.yy" });
				});
		</script>
       
    </head>
    <body>
        <h1>Яндекс Метрика</h1>
              
	    <div class="sectionBody">
	    <div class="tab-pane" id="MetrikaPane"> 
			
			<div class="tab-page" id="statistic">  
	            <h2 class="tab">Статистика</h2>  
				<div id="actions">
					<ul class="actionButtons">
						<li id="Button1"><a href="'.$adm_patch.'&action=refresh_statistic" onclick="refresh_statistic(); return false;"><img src="media/style/MODxRE/images/icons/refresh.png" /> Обновить статистику</a></li>
					</ul>
				</div>
				<br>
				<div id="statistics">';
			include_once($mod_path. "templates/statistic_list.php");
			echo '
				</div>
	        </div>
			
	        <div class="tab-page" id="tabTemplates">  
	            <h2 class="tab">Выбор счётчиков</h2>  
				<div id="actions">
					<ul class="actionButtons">
						<li id="Button1"><a href="#" onclick="refresh_counters(); return false;"><img src="media/style/MODxRE/images/icons/refresh.png" /> Обновить список</a></li>
					</ul>
				</div>
				Отметьте те счётчики, статистику которых учитывать не нужно<br><br>
				<div id="counters">';
			include_once($mod_path. "templates/counters_list.php");
			echo '
				</div>
	        </div>
	   
	        <div class="tab-page" id="tabTemplateVariables">  
	            <h2 class="tab">Настройки модуля</h2>
				<br>';
			include_once($mod_path. "templates/settings_list.php");
			echo '
	        </div>
	    </div>
	</div>
	<div id="load" class="hidden"><span><img src="'.$path.'img/loading.gif"><br>Обновление</span></div>
   </body>
</html>
';
?>