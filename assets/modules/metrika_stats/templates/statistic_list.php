<?

$visits_total = 0;
$visitors_total = 0;
$new_visitors_total = 0;
$referrals_total = 0;
$advertising_total = 0;
$searchengines_total = 0;
$request_total = 0;
$social_total = 0;

$select_counters = $modx->db->select("*", $counters, "active = '1'");
while ($value = mysql_fetch_array($select_counters)){//выводим записи

$json = (array) json_decode($value['stats_total'], true);
	
	$visits_total = $visits_total + $json['visits'];
	$visitors_total = $visitors_total + $json['visitors'];
	$new_visitors_total = $new_visitors_total + $json['new_visitors'];
	$referrals_total = $referrals_total + $json['referrals'];
	$advertising_total = $advertising_total + $json['advertising'];
	$searchengines_total = $searchengines_total + $json['searchengines'];
	$request_total = $request_total + $json['request'];
	$social_total = $social_total + $json['social'];
	
	$counters_arr[] = $value;
	
	$period = (array) json_decode($value['period'], true);
}

if (empty($settings_data)) {
	$select_settings = $modx->db->select("*", $settings);
	while ($setting = mysql_fetch_array($select_settings)){//выводим записи
		$settings_data[$setting['name']] = $setting['value'];
	}
}

echo '
<div class="total">
	<div class="dates">
		<span class="start">'.date("d.m.Y",$period['start']).'</span>
		<span class="def"> - </span>
		<span class="finish">'.date("d.m.Y H:i",$period['finish']).'</span>
	</div>
	<div class="big">
		<div class="cell">
			<span class="name">Посещений:</span>
			<span class="value">'.$visits_total.'</span>
		</div>
		<div class="cell">
			<span class="name">Посетителей:</span>
			<span class="value">'.$visitors_total.'</span>
		</div>
		<div class="cell">
			<span class="name">Новых:</span>
			<span class="value">'.$new_visitors_total.'</span>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="small">
		<div class="cell">
			<span class="name">по ссылкам</span>
			<span class="value">'.$referrals_total.'</span>
		</div>
		<div class="cell">
			<span class="name">из поиска</span>
			<span class="value">'.$searchengines_total.'</span>
		</div>
		<div class="cell">
			<span class="name">по рекламе</span>
			<span class="value">'.$advertising_total.'</span>
		</div>
		<div class="cell last">
			<span class="name">из соц сетей</span>
			<span class="value">'.$social_total.'</span>
		</div>
		<div class="clear"></div>
	</div>
</div>

<br><br><br>

<table width="100%" border="1px" cellpadding="1px">
	<thead>
		<tr>
			<td align="center">№</td>
			<td>Название</td>
			<td>Сайт</td>
			<td align="center">Визитов</td>
			<td align="center">Посетителей</td>
			<td align="center">Новых посетителей</td>
			<td align="center">По источникам</td>
		</tr>
	</thead>
	
	<tbody>
';


foreach ($counters_arr as $value) {

$json = (array) json_decode($value['stats_total'], true);
	echo '
		<tr>
			<td align="center">'.$value['id'].'</td>
			<td>'.$value['name'].'</td>
			<td>'.$value['site'].'</td>
			<td align="center">'.$json['visits'].'</td>
			<td align="center">'.$json['visitors'].'</td>
			<td align="center">'.$json['new_visitors'].'</td>
			<td>
				Прямые заходы: '.$json['request'].'<br>
				Переходы по ссылкам: '.$json['referrals'].'<br>
				Переходы из поиска: '.$json['searchengines'].'<br>
				Переходы по рекламе: '.$json['advertising'].'<br>
				Переходы из соц сетей: '.$json['social'].'
			</td>
		</tr>
	
	';
	
}
	
	echo '
	</tbody>
</table>

<script>
					$("#load").addClass("hidden");
			</script>
';

?>