<?
global $modx;
$path = $modx->config['site_url'].'assets/modules/metrika_stats/';
$mod_path = MODX_BASE_PATH.'assets/modules/metrika_stats/';
$adm_patch = '/manager/index.php?a=112&id=6';

include_once($mod_path. "config.php");
include_once($mod_path. "functions.php");


switch ($_GET['action']) {
    case 'refresh_counters'://обновление списка счётчиков
		include_once($mod_path. "actions/refresh_counters.php");
		include_once($mod_path. "templates/counters_list.php");
        break;
	case 'refresh_statistic'://обновление статистики
		include_once($mod_path. "actions/refresh_statistic.php");
		include_once($mod_path. "templates/statistic_list.php");
        break;
	case 'refresh_statistic_cron'://обновление статистики кроном
		include_once($mod_path. "actions/refresh_statistic.php");
        break;
    default://основная страница
		include_once($mod_path. "templates/main.php");
}



?>