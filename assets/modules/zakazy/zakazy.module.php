<?
	global $modx;
	$dbname = $modx->db->config['dbase']; //имя базы данных
	$dbprefix = $modx->db->config['table_prefix']; //префикс таблиц
	$mod_table = $dbprefix."zakazy"; //таблица модуля
	$theme = $modx->config['manager_theme'];
	$path = $modx->config['site_url'].'assets/modules/zakazy/';
	$mod_path = MODX_BASE_PATH.'assets/modules/zakazy/';
	
	function pre($data) {
echo '<pre>';
print_r ($data);
echo '</pre>';
}	
	
echo '
<!DOCTYPE html> 
	<html>
	<head>
	<link rel="stylesheet" type="text/css" href="'.$path.'css/manager.css">
	<link rel="stylesheet" type="text/css" href="media/style/'.$theme.'/style.css">
	<script src="'.$path.'js/jquery-1.3.2.min.js"></script>
	<script src="'.$path.'js/jquery.colorbox.js"></script>
	<link rel="stylesheet" href="'.$path.'css/colorbox.css" type="text/css" />
	<script>
			$(document).ready(function(){
				$(".iframe").colorbox({
				iframe:true, 
				width:"470px", 
				height:"535px",
				onCleanup: function(){
					location.reload();
				}
				});
				/*
				$(document).ready(function(){
				$(".iframe").colorbox({
				padding : 0,
				margin : 0,	
				width : 470,
				autoSize : true,
				onCleanup: function(){
					location.reload();
				}
				});
				*/
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$(\'#click\').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
				  });
	</script>
	<script type="text/javascript">
function checkAll(obj) {
  \'use strict\';
  // Получаем NodeList дочерних элементов input формы:
  var items = obj.form.getElementsByTagName("input"),
      len, i;
  // Здесь, увы цикл по элементам формы:
  for (i = 0, len = items.length; i < len; i += 1) {
    // Если текущий элемент является чекбоксом...
    if (items.item(i).type && items.item(i).type === "checkbox") {
      // Дальше логика простая: если checkbox "Выбрать всё" - отмечен           
      if (obj.checked) {
        // Отмечаем все чекбоксы...
        items.item(i).checked = true;
      } else {
        // Иначе снимаем отметки со всех чекбоксов:
        items.item(i).checked = false;
      }      
    }
  }
}
</script>
	<head>
	<body>';
	print_r($rd);
		if ($modx->db->getRecordCount($modx->db->query("SHOW TABLES FROM  $dbname LIKE '$mod_table'")) ==0) {
		
		// Если таблица не существует выводим окно установки
		echo '<div class="intro">
			<img src="'.$path.'images/logo.png" style="display:block; margin:0 auto">
			<div class="text-intro"><b>MODX™ FAQ</b> - это бесплатный модуль &laquo;Вопрос-ответ&raquo;для CMS | CMF MODx Evolution.
			<br>
				<ul class="actionButtons">
					<li>
						<a href="'.$_SERVER['REQUEST_URI'].'&action=install"><img src="media/style/MODxCarbon/images/icons/table.gif"> Установить модуль</a>
					</li>
				</ul>
				<small>Вопросы, помощь в доработке принимаются по адресу <a target="_blank" href="mailto:nikitadm@tut.by">nikitadm@tut.by</a><br>А так же на сайте автора <a target="_blank" href="http://p-style.by/">p-style.by</a></small>

			</div>
		</div>';
		if ($_GET['action']=="install") {
				$sql = "
		CREATE TABLE IF NOT EXISTS `modx_faq` (
			`id` int(10) NOT NULL AUTO_INCREMENT,
			`date` text NOT NULL,
		`name` text NOT NULL,
		`email` text NOT NULL,
		`message` text NOT NULL,
		`answer` text NOT NULL,
		`readen` int(1) NOT NULL DEFAULT '0',
		`answered` int(1) NOT NULL DEFAULT '0',
		`published` int(1) NOT NULL DEFAULT '0',
		`send` int(1) NOT NULL DEFAULT '0',
		PRIMARY KEY (`id`)
		)
		";
		$modx->db->query($sql);
		}
	}
	else {
		$zakazy = $modx->getFullTableName( 'zakazy' ); //таблица с вопросами
	$get_zakazy_table = $modx->db->select("*", $zakazy, "", "date DESC", "");
if (empty($_GET['zakaz'])) {
	echo '
	<div class="copy">
	<div class="left">
	<h1>Заказы</h1>
	<div class="addBlock">
</div>
	</div>
	<div class="right">
	<a class="info" href="'.$_SERVER['REQUEST_URI'].'&zakaz=help"><img src="media/style/'.$theme.'/images/icons/information.png">Справка</a>
	</div>
	<div class="clear"></div>
	</div>
<div class="mainList">
<form method="POST">
	<table class="faqList" cellspacing="0" >
	<thead>
		<tr>
			<td class="chk"></td>
			<td class="id"><b>№</b></td>
			<td class="status"><b>Статус</b></td>
			<td class="service"><b>Услуга</b></td>
			<td class="name"><b>Имя</b></td>
			<td class="tel"><b>Телефон</b></td>
			<td class="email"><b>E-mail</b></td>
			<td class="date"><b>Дата</b></td>
			<td class="ip"><b>IP</b></td>
		</tr>
	</thead>
	<tbody>';

while( $qw = $modx->db->getRow( $get_zakazy_table ) ) { 
$ip = ($qw['ip'] == "212.98.186.136" || $qw['ip'] == "79.98.53.5") ? '<span class="red">'.$qw['ip'].' (наш)</span>' : '<span class="green">'.$qw['ip'].'</span>';
	echo '
	 	<tr>
			<td class="chk"><input type="checkbox" name="chk[]" value="'.$qw['id'].'"></td>
			<td class="id">'.$qw['id'].'</td>';
	 if ($qw['status']==0){
		 echo '<td class="status unread iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'"><span>Новый</span></td>';
	 }
	 if ($qw['status']==1){
		 echo '<td class="status read iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'"><span>Просмотрен</span></td>';
	 }
	 
			echo '	
			<td class="service iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'">'.$qw['service'].'</td>
			<td class="name iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'"><span>'.$qw['name'].'</span></td>
			<td class="tel iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'">'.$qw['tel'].'</td>
			<td class="email iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'">'.$qw['email'].'</td>
			<td class="date iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'">'.date("d.m.Y в H:i", $qw['date']).'</td>
			<td class="ip iframe" href="'.$_SERVER['REQUEST_URI'].'&zakaz=open&zakaz_id='.$qw['id'].'">'.$ip.'</td>
		</tr>
		';
 }

echo '
	</tbody>
</table>
<label class="chk_all">Все<input style="display:none" type="checkbox" name="one" value="all" onclick="checkAll(this)" /></label>
<div class="actions">
<span>С отмечеными:</span><br>
<select name="actionForChecked">
				<option value="public">Выбрать...</option>
				<option value="delete">Удалить</option>
</select>
<input type="submit" value="Выполнить">
</div>
</form>
</div>
';
if (!empty($_POST['actionForChecked'])) {
if ($_POST['actionForChecked'] == "delete") {
	foreach ($_POST['chk'] as $key => $value) {
			$modx->db->delete($zakazy, "id ='".$value."'");
		}
}
unset($_POST);
$rewr = str_replace("&amp;","&",$_SERVER["HTTP_REFERER"]);
header('Location:'.$rewr.'');
}
}





if ($_GET['zakaz']=="open") {


	$fields = array(
	"status" => 1
     );
	$query = $modx->db->update($fields, $zakazy, "id='".$_GET['zakaz_id']."'");
	$qw = $modx->db->getRow( $modx->db->select("*", $zakazy, "id='".$_GET['zakaz_id']."'", "", "") );
echo '<div class="main">	
	<form method="POST">
	<div class="head">
	<div class="left">
	<p class="title">Заказ #'.$_GET['zakaz_id'].'</p>
	<p class="date">'.date("d.m.Y в H:i", $qw['date']).'<br>ip: '.$qw['ip'].'</p>
	</div>
	<div class="right">
	'.$qw['service'].'
	</div>
	<div class="clear"></div>
	</div>
	
	<div class="info">';
	if (!empty($qw['email'])) {
	echo '
	<p class="email"><b>E-mail:</b> '.$qw['email'].'</p>
	';
	}
	if (!empty($qw['name'])) {
	echo '
	<p class="name"><b>Имя:</b> '.$qw['name'].'</p>';
	}
	if (!empty($qw['tel'])) {
	echo '
	<p class="tel"><b>Телефон:</b> '.$qw['tel'].'';
		if (!empty($qw['region'])) {
		echo '<br>'.$qw['region'].'';
		}
	echo '</p>';
	}
	
	if (!empty($qw['site'])) {
	$site = str_replace("http://", "", $qw['site']);
	echo '
	<p class="site"><b>Сайт:</b> <a href="http://'.$site.'" target="_blank">'.$site.'</a></p>';
	}
	
	if (!empty($qw['timecall'])) {
	echo '
	<p class="timecall"><b>Время звонка:</b> '.$qw['timecall'].'</p>';
	}
	echo '
	</div>
	<div class="areas">';
	if (!empty($qw['message'])) {
	echo '
	<p class="question">
	<b>Пожелания:</b><br>
	<p class="message">
'.$qw['message'].'
	</p>
	</p>
	';
	}
	echo '
	<p class="answer">
	<b>Комментарий:</b><br>
<textarea name="comment">
'.$qw['comment'].'
</textarea><br>
	</p>
	</div>
	<!--Скрытые поля-->
	<input type="hidden" name="name" value="'.$qw['name'].'">
	<input type="hidden" name="email" value="'.$qw['email'].'">
	<input type="hidden" name="date" value="'.date("d.m.Y", $qw['date']).'">
	
		<div class="buttons">
	<button type="submit" name="action" value="save" class="save"><span>Сохранить</span></button>
	<button type="reset" value="" onclick="parent.$.fn.colorbox.close()"  class="back"><span>Закрыть</span></button>
	</div>
	<div class="clear"></div>
	</form>
</div>';
	if ($_POST['action'] == 'save') {
	
		
		
		
	$fields = array(
    "comment" => $_POST['comment']
	);
	$query = $modx->db->update($fields, $zakazy, "id = ".$_GET['zakaz_id'].""); 

	
	 echo ' <script>

parent.$.fn.colorbox.close()

</script>';
	}
}
			
	

if ($_GET['zakaz']=="help") {
echo '
	<div class="copy">
	<div class="left">
	<h1>Модуль &laquo;заказов&raquo;</h1>
	</div>
	<div class="right">
	<a class="info" href="'.$_SERVER['REQUEST_URI'].'&zakaz=help"><img src="media/style/'.$theme.'/images/icons/information.png">Справка</a>
	</div>
	<div class="clear"></div>
	</div>
<div class="mainHelp">
<h2>Справка по модулю &laquo;Заказов&raquo;</h2>

<div>';
}
}
echo '
	</body>
	</html>
	';
	



?>