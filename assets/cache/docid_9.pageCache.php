<?php die('Unauthorized access.'); ?>a:42:{s:2:"id";s:1:"9";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:87:"Контекстная реклама – ставка на эффективность!";s:9:"longtitle";s:84:"Контекстная реклама в Яндекс Директ и Google AdWords";s:11:"description";s:241:"Размещение контекстных рекламных объявлений в поисковых системах Яндекс и Google для увеличения продаж и привлечения новых клиентов.";s:5:"alias";s:20:"kontekstnaya-reklama";s:15:"link_attributes";s:8:"kontekst";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"3";s:8:"isfolder";s:1:"1";s:9:"introtext";s:234:"Мощный, эффективный и быстрый инструмент для связи с потенциальными клиентами, которые ищут в интернете ваши товары или услуги.";s:7:"content";s:4104:"<p>Вашему бизнесу нужна реклама, которая принесет быстрый и качественный результат?&nbsp; Вы стремитесь к максимальной отдаче при незначительных вложениях? Контекстная реклама &ndash; то что вам нужно! Она привлекает именно тех пользователей, которые не просто ищут информацию, а готовы совершить покупку вашего товара или заказать услугу прямо сейчас. <br />За быстроту исполнения рекламной кампании отвечают Яндекс.Директ и Google AdWords, а за качество &ndash; сертифицированные специалисты агентства.</p>
<p>&nbsp;</p>
<hr />
<h2>Преимущества контекстной рекламы</h2>
<ul>
<li>Быстрый старт (запуск рекламной кампании происходит в течение дня)</li>
<li>Нет комиссии для Yandex (100% вашей оплаты зачисляется на счет будущей рекламной кампании)</li>
<li>Максимальный охват целевой аудитории из поисковых систем Yandex и Google &ndash; тех посетителей, кто готов купить ваш товар</li>
<li>Гибкая настройка рекламной кампании (в любой момент можно внести необходимые корректировки)</li>
</ul>
<p>&nbsp;</p>
{{order_button_in_kontekst}}
<p></p>
<hr />
<h2>Как это работает</h2>
<p>{{howitworks}}</p>
<p>&nbsp;</p>
<hr />
<h2>За что вы платите</h2>
<p>Вы платите только за переходы потенциальных клиентов.</p>
<p>Стоимость перехода в контекстной рекламе зависит от количества запросов, уровня конкуренции и площадок размещения рекламного объявления.</p>
<p><i>Минимальная стоимость заказа&nbsp;&ndash; 10 000 руб.</i></p>
<p>&nbsp;</p>
<hr />
<h2>Преимущества сотрудничества с нами:</h2>
<ul>
<li>Мы являемся сертифицированным партнером Google Adwords</li>
<li>Все сотрудники отдела имеют сертификаты Yandex.Direct</li>
<li>Абонентская плата для Яндекса &ndash; 0%</li>
<li>Комплексный подход к размещению контекстной рекламы: ретаргетинг, иллюстрированные объявления, медийно-графические баннеры, видеообъявления</li>
<li>Настройка таргетинга на сайты, темы, демографические данные пользователей</li>
<li>Использование в объявлениях расширений: адресов, интерактивных номеров телефонов для мобильных устройств, дополнительных ссылок</li>
<li>Достижение высокого CTR (показатель эффективности) кампании. Чем выше CTR, тем ниже стоимость клика для рекламодателя</li>
<li>Постоянный мониторинг, анализ, оперативное внесение корректировок</li>
<li>Персональный менеджер</li>
<li>Юридические гарантии</li>
</ul>
<p style="text-align: center;">&nbsp;</p>
<hr />
<h2>Нам доверяют</h2>
<p>{{clients_inside}}</p>
<p></p>
<hr />
<p></p>
<h2>Как начать работу</h2>
<p>{{how_starting_kontekst}}</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"34";s:9:"menuindex";s:1:"1";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1383554277";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1403705445";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1383554277";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:37:"Контекстная реклама";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:14:"usluga_img_std";a:5:{i:0;s:14:"usluga_img_std";i:1;s:28:"images/services/kontekst.png";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:14:"usluga_img_hov";a:5:{i:0;s:14:"usluga_img_hov";i:1;s:34:"images/services/kontekst_hover.png";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:10:"howitworks";a:5:{i:0;s:10:"howitworks";i:1;s:2723:"{"fieldValue":[{"title":"Подготовка рекламной кампании","image_unhov":"/images/hwork_kontekst/prep_unhov.png","image_hov":"/images/hwork_kontekst/prep_hov.png","text":"На данном этапе формируется список ключевых запросов, составляется медиаплан (прогнозируются бюджет и позиции, на которых будут показываться объявления по выбранным ключевым словам), подбираются целевые страницы, на которые пользователь перейдет с контекстного объявления. Специалисты пишут привлекательные и цепляющие тексты объявлений."},{"title":"Настройка и запуск рекламной кампании","image_unhov":"/images/hwork_kontekst/setting_unhov.png","image_hov":"/images/hwork_kontekst/setting_hov.png","text":"Этап предполагает регистрацию в системе, выбор стратегии, настройку временного и географического таргетинга, размещение объявлений, выставление цены за клик. "},{"title":"Управление ставками","image_unhov":"/images/hwork_kontekst/bet_unhov.png","image_hov":"/images/hwork_kontekst/bet_hov.png","text":"Ставки выставляются согласно выбранной стратегии таким образом, чтобы объявления показывались на лучших позициях, получали больше кликов и при этом не требовалось дополнительных финансовых вложений. "},{"title":"Анализ и корректировки","image_unhov":"/images/hwork_kontekst/analysis_unhov.png","image_hov":"/images/hwork_kontekst/analysis_hov.png","text":"На основе анализа текущей статистики в целях повышения эффективности кампании, вносятся корректировки в объявления, запросы, ставки, время показа и в другие настройки рекламной кампании. "},{"title":"Отчетность для клиента","image_unhov":"/images/hwork_kontekst/report_unhov.png","image_hov":"/images/hwork_kontekst/report_hov.png","text":"Еженедельное предоставление подробных отчетов о ходе рекламной кампании."}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Контекстная реклама в Яндекс Директ и Google AdWords</title>
		<meta name="description" content="Размещение контекстных рекламных объявлений в поисковых системах Яндекс и Google для увеличения продаж и привлечения новых клиентов."/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<script src="/js/tabulous.js" type="text/javascript"></script>
		<link rel="stylesheet" href="/css/tabulous.css" />
		<script>
			$(document).ready(function($) {
				$('#how_work').tabulous({
					effect: 'scale'
				});
			});
			jQuery(document).ready(function(){
				jQuery('.answer').hide()
				jQuery('.question').click(function(){
					jQuery(this).toggleClass("closed").toggleClass("open").next().toggle(250)
					
				})
			})
		</script>
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li class="active"><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content services kontekst">
			
			<div class="left">
				<h1>Контекстная реклама – ставка на эффективность!</h1>
				<div class="text"><p>Вашему бизнесу нужна реклама, которая принесет быстрый и качественный результат?&nbsp; Вы стремитесь к максимальной отдаче при незначительных вложениях? Контекстная реклама &ndash; то что вам нужно! Она привлекает именно тех пользователей, которые не просто ищут информацию, а готовы совершить покупку вашего товара или заказать услугу прямо сейчас. <br />За быстроту исполнения рекламной кампании отвечают Яндекс.Директ и Google AdWords, а за качество &ndash; сертифицированные специалисты агентства.</p>
<p>&nbsp;</p>
<hr />
<h2>Преимущества контекстной рекламы</h2>
<ul>
<li>Быстрый старт (запуск рекламной кампании происходит в течение дня)</li>
<li>Нет комиссии для Yandex (100% вашей оплаты зачисляется на счет будущей рекламной кампании)</li>
<li>Максимальный охват целевой аудитории из поисковых систем Yandex и Google &ndash; тех посетителей, кто готов купить ваш товар</li>
<li>Гибкая настройка рекламной кампании (в любой момент можно внести необходимые корректировки)</li>
</ul>
<p>&nbsp;</p>
				<div class="order_button">
					<a href="[~88~]?service=Контекстная реклама" class="poplight open_iframe" data-fancybox-type="iframe">Заказать услугу</a>
				</div>
<p></p>
<hr />
<h2>Как это работает</h2>
<p>[!multiTV? 
	&tvName=`howitworks` 
	&display=`all` 
	&outerTpl=`@CODE:
		<div id="how_work">
			[!multiTV? 
				&tvName=`howitworks` 
				&display=`all` 
				&outerTpl=`@CODE:<ul>((wrapper))</ul>`
				&rowTpl=`@CODE:<li>
					<a class="tab" href="#tabs-((iteration))" title="">
						<img src="((image_unhov))" class="un_hov" alt="((title))">
						<img src="((image_hov))" class="hov" alt="((title))">
						<span>((title))</span>
					</a>
				</li>`
			&outputSeparator=`<li class="separator"><img src="/images/hwork_seo/sepr.png" alt=""></li>`
			!]
			<div id="tabs_container">((wrapper))</div></div>`
	&rowTpl=`@CODE:<div id="tabs-((iteration))">((text))</div>`!]</p>
<p>&nbsp;</p>
<hr />
<h2>За что вы платите</h2>
<p>Вы платите только за переходы потенциальных клиентов.</p>
<p>Стоимость перехода в контекстной рекламе зависит от количества запросов, уровня конкуренции и площадок размещения рекламного объявления.</p>
<p><i>Минимальная стоимость заказа&nbsp;&ndash; 10 000 руб.</i></p>
<p>&nbsp;</p>
<hr />
<h2>Преимущества сотрудничества с нами:</h2>
<ul>
<li>Мы являемся сертифицированным партнером Google Adwords</li>
<li>Все сотрудники отдела имеют сертификаты Yandex.Direct</li>
<li>Абонентская плата для Яндекса &ndash; 0%</li>
<li>Комплексный подход к размещению контекстной рекламы: ретаргетинг, иллюстрированные объявления, медийно-графические баннеры, видеообъявления</li>
<li>Настройка таргетинга на сайты, темы, демографические данные пользователей</li>
<li>Использование в объявлениях расширений: адресов, интерактивных номеров телефонов для мобильных устройств, дополнительных ссылок</li>
<li>Достижение высокого CTR (показатель эффективности) кампании. Чем выше CTR, тем ниже стоимость клика для рекламодателя</li>
<li>Постоянный мониторинг, анализ, оперативное внесение корректировок</li>
<li>Персональный менеджер</li>
<li>Юридические гарантии</li>
</ul>
<p style="text-align: center;">&nbsp;</p>
<hr />
<h2>Нам доверяют</h2>
<p><div class="brands_carousel_inside_page">
				<ul class="brands" id="brands_carousel_inside_page">
			[!Ditto? &tpl=`clientTpl` &parents=`4` &depth=`5` &hideFolders=`1` &randomize=`1` &showInMenuOnly=`1`!]
			</ul>
				
				</div></p>
<p></p>
<hr />
<p></p>
<h2>Как начать работу</h2>
<p><div class="order_button">
	<span>Нажмите на кнопку</span>
				<a href="[~88~]?service=Контекстная реклама" class="poplight open_iframe" data-fancybox-type="iframe">Заказать услугу</a>
	<span>или позвоните:</span>
	<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				</div></p></div>
				
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	<li ><span id="seo">SEO-продвижение</span>

<ul>
	<li><a href="[~140~]" title="SEO-продвижение – двигатель Вашего бизнеса!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`140` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="active" ><span id="kontekst">Контекстная реклама</span>

<ul>
	<li class="active"><span title="Контекстная реклама – ставка на эффективность!">Описание услуги</span></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`9` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="audit">Аудит сайта</span>

<ul>
	<li><a href="[~8~]" title="Аудит сайта">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`8` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="last" ><span id="copy">Копирайтинг</span>

<ul>
	<li><a href="[~10~]" title="Копирайтинг">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`10` &orderBy=`menuindex ASC`!]
</ul>

</li>






</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]
			</div>
			<div class="clear"></div>
			</div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
		<!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'top3seo';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/counter.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
	</body>
</html>