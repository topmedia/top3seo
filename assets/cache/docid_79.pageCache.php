<?php die('Unauthorized access.'); ?>a:42:{s:2:"id";s:2:"79";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:23:"Пакеты услуг";s:9:"longtitle";s:66:"Пакеты услуг по контекстной рекламе";s:11:"description";s:103:"Готовые пакеты для заказа услуг по контекстной рекламе. ";s:5:"alias";s:25:"stoimost-reklamy-kontekst";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"9";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:1078:"<p>Преимущество контекстной рекламы заключается в том, что ее можно настроить с высокой точностью, сориентировав на целевую аудиторию. С помощью сервисов статистики можно отслеживать эффективность рекламной кампании и в зависимости от ситуации оперативно вносить изменения в объявления, снижать стоимость клика и настраивать кампанию таким образом, чтобы получить от нее максимальный результат.</p>
<p>Мы разработали специальные пакеты услуг, каждый из которых на практике показал свою эффективность. Рекламные кампании гарантированно приведут вам новых клиентов и увеличат продажи.</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"21";s:9:"menuindex";s:1:"0";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1388150571";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1401187062";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1388150571";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:5:"price";a:5:{i:0;s:5:"price";i:1;s:182:"{"fieldValue":[{"bonus":"Ежемесячный бюджет (руб.)","optima":"от 10 000","business":"от 15 000","premium":"от 30 000"}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:9:"tarifswrd";a:5:{i:0;s:9:"tarifswrd";i:1;s:2:"[]";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:10:"tarifstext";a:5:{i:0;s:10:"tarifstext";i:1;s:6680:"{"fieldValue":[{"descr":"Формирование списка ключевых слов - запросов, после тщательного изучения и анализа бизнеса и сайта, по которым будут переходить пользователи на сайт, а также подбор минус-слов, по запросам с которыми рекламное объявление показываться не будет","bonus":"Подбор и уточнение ключевых фраз","micro":"+","optima":"+","business":"+","premium":"+"},{"descr":"Написание контекстных объявлений – текстовых отклткательных объявлений, составленных на основе подобранных ключевых слов (запросов), соответствующих требованиям рекламных площадок","bonus":"Написание текстов объявлений","micro":"до 10","optima":"до 20","business":"до 50","premium":"∞"},{"descr":"Настройка рекламной кампании с помощью различных функций и инструментов контекстных систем Яндекс.Директ и Google AdWords: «нацеливание» рекламной кампании на конкретную заинтересованную аудиторию, настройка запланированного бюджета, цены за клик, ссылок на сайт, внесение информации о местоположении, времени работы и контактных данных компании","bonus":"Настройка рекламной кампании ","micro":"+","optima":"+","business":"+","premium":"+"},{"descr":"Ежедневный мониторинг работы объявлений, изменений цены за клик, позиций показа, расхода установленного бюджета","bonus":"Ежедневный мониторинг","micro":"+","optima":"+","business":"+","premium":"+"},{"descr":"Повышение эффективности объявлений с помощью расширений GoogleAdWords (выбор подходящего типа расширения в соответствии с поставленными целями, создание расширений - адреса и номера телефонов, интерактивные номера телефонов для мобильных устройств, дополнительные ссылки). Создание медийных объявлений для показов в контекстно-медийной сети Google","bonus":"Расширения Google AdWords","micro":"","optima":"","business":"+","premium":"+"},{"descr":"Повышение эффективности объявлений с помощью расширений Яндекс.Директ (дополнительные ссылки, изображения, функция ретаргетинга)","bonus":"Расширения Яндекс.Директ","micro":"","optima":"","business":"+","premium":"+"},{"descr":"Создание связи с Google Analytics для глубокой оценки эффективности рекламной кампании. Формирование отчетов","bonus":"Работа с Google Analytics","micro":"","optima":"","business":"","premium":"+"},{"descr":"Создание связи с Яндекс.Метрикой для глубокой оценки эффективности рекламной кампании. Формирование отчетов","bonus":"Работа с Яндекс Метрикой","micro":"","optima":"","business":"","premium":"+"},{"descr":"Внесение предложений по улучшению рекламной кампании (пересмотр бюджетов, используемых инструментов, стратегии ведения, ключевых слов, объявлений). Отслеживание эффективности ключевых слов, площадок, объявлений (на предмет конверсий, цены цели).\nПланирование использования дополнительных услуг Яндекс и Google. Внедрение после согласования","bonus":"Оптимизация кампании","micro":"","optima":"","business":"+","premium":"+"},{"descr":"В ходе сотрудничества с нами у вас могут возникнуть различные вопросы. Наши специалисты с удовольствием ответят на все ваши вопросы, связанные с работой, ведением рекламной кампании и внесению необходимых корректировок ","bonus":"Консультация специалиста","micro":"","optima":"1 час","business":"3 часа","premium":"∞"},{"descr":"Отчет \"Статистика по дням\" - срез по статистике за определенный период, в котором представлены следующие данные: число показов; число кликов;\nCTR; расход; средняя цена клика","bonus":"Стандартный отчет по запросу","micro":"1 в месяц","optima":"1 в месяц","business":"3 в месяц","premium":"∞"},{"descr":"Расширенный отчет, в котором представлены такие статистические данные, как: статистика по дням, общая статистика, фразы по дням, статистика по регионам, статистика по площадкам за определенный период времени","bonus":"Нестандартный расширенный отчет","micro":"","optima":"","business":"","premium":"1 в месяц"},{"descr":"Аудит включает:\n• анализ рекламных площадок и их структуры;\n• анализ ключевых запросов, текстов объявлений;\n• оценку распределения бюджета;\n• составление рекомендаций","bonus":"Профессиональный аудит","micro":"","optima":"","business":"","premium":"1 в 6 мес."}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Пакеты услуг по контекстной рекламе</title>
		<meta name="description" content="Готовые пакеты для заказа услуг по контекстной рекламе. "/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<link href="/css/features-table.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li class="active"><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content services ">
			
			<div class="left">
				<h1>Пакеты услуг</h1>
				<div class="text"><p>Преимущество контекстной рекламы заключается в том, что ее можно настроить с высокой точностью, сориентировав на целевую аудиторию. С помощью сервисов статистики можно отслеживать эффективность рекламной кампании и в зависимости от ситуации оперативно вносить изменения в объявления, снижать стоимость клика и настраивать кампанию таким образом, чтобы получить от нее максимальный результат.</p>
<p>Мы разработали специальные пакеты услуг, каждый из которых на практике показал свою эффективность. Рекламные кампании гарантированно приведут вам новых клиентов и увеличат продажи.</p></div>
				
				<div class="text">
					<div class="price">
						<table id="tar" class="features-table">
							<thead>
								<tr>
									<td style="text-align: center;"></td>
									<td style="text-align: center;"><span data-mce-mark="1">Optima</span></td>
									<td style="text-align: center;"><span data-mce-mark="1">Business</span></td>
									<td style="text-align: center;"><span data-mce-mark="1">Premium</span></td>
								</tr>
							</thead>
							<tbody>
								[!multiTV? tvName=`tarifswrd` display=`all` rowTpl=`bonusTpl`!]
								[!multiTV? tvName=`tarifstext` display=`all` rowTpl=`bonusTpl`!]
							</tbody>
							<tfoot>
								[!multiTV? tvName=`price`!]
							</tfoot>
						</table>
					</div>
					<script language="javascript"> function togle_par_about(par_id){ if($("#"+par_id).is(":visible")){ $("#"+par_id).attr("style","display: none"); } else{ $(".par-q-link").attr("style","visibility: hidden"); $("#"+par_id).parent().find(".par-q-link").attr("style","visibility: visible"); $(".par-about-q").attr("style","display: none"); $("#"+par_id).attr("style","display: "); } }  $(".pdinfohead").live("hover",function(){ $(this).find(".par-q-link").attr("style","visibility: visible"); });  $(".pdinfohead").live("mouseleave",function(){ if(!($(this).find(".par-about-q").is(":visible"))){ $(this).find(".par-q-link").attr("style","visibility: hidden"); } });  $("body").click(function(e) { if($(e.target).attr("class") != "par-link" && $(e.target).attr("class") != "par-q-link" && $(e.target).attr("class") != "par-about-head" && $(e.target).attr("class") != "par-about-inner"){ $(".par-about-q:visible").parent().find(".par-q-link").attr("style","visibility: hidden"); $(".par-about-q").attr("style","display: none"); } }); </script>
				</div>
				<div class="order_button">
				<a href="[~88~]?service=Пакеты услуг" class="poplight open_iframe" data-fancybox-type="iframe">Заказать рекламу</a>
				</div>
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	<li ><span id="seo">SEO-продвижение</span>

<ul>
	<li><a href="[~140~]" title="SEO-продвижение – двигатель Вашего бизнеса!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`140` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="active" ><span id="kontekst">Контекстная реклама</span>

<ul>
	<li><a href="[~9~]" title="Контекстная реклама – ставка на эффективность!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`9` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="audit">Аудит сайта</span>

<ul>
	<li><a href="[~8~]" title="Аудит сайта">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`8` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="last" ><span id="copy">Копирайтинг</span>

<ul>
	<li><a href="[~10~]" title="Копирайтинг">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`10` &orderBy=`menuindex ASC`!]
</ul>

</li>






</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]
			</div>
			<div class="clear"></div>
			</div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
		<!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'top3seo';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/counter.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
	</body>
</html>