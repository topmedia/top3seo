<?php die('Unauthorized access.'); ?>a:40:{s:2:"id";s:3:"130";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:82:"Немного запоздалое поздравление с 23 февраля!";s:9:"longtitle";s:107:"Поздравление с "Днем защитника Отечества" от нашей команды";s:11:"description";s:0:"";s:5:"alias";s:26:"pozdravlenie-s-23-fevralya";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:2:"58";s:8:"isfolder";s:1:"0";s:9:"introtext";s:619:"Пока все отмечали 23 февраля, мы плодотворно трудились и лишь недавно вспомнили, что так и не отметили День защитника Отечества. Женский коллектив Top3Seo поздравляет мужчин-коллег с праздником и желает им дальнейших профессиональных достижений. Наша дружная команда специалистов поможет вашим проектам занять достойное место под солнцем!";s:7:"content";s:1169:"<p>Мы настолько увлеклись работой, что упустили очень важное событие &ndash; 23 февраля. Пусть и с запозданием, но мы поздравляем нашу дружную мужскую команду с прошедшим&nbsp;&laquo;Днем защитника Отечества&raquo;.&nbsp;Ведь для нас главное&nbsp;&ndash; защитить и укрепить позиции наших клиентов в поисковых системах.</p>
<p>В феврале работа кипит еще более усиленно, а времени еще меньше. Как мы не старались, но у нас не получилось отметить 23 февраля. Именно поэтому мы решили отпраздновать данное событие уже весной вместе с нашей прекрасной половиной коллектива.</p>
<p>Команда Top3SEO продолжает свою успешную работу и поздравляет всех с наступающей весной!&nbsp;</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"26";s:9:"menuindex";s:1:"1";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1393334648";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1394199428";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1393334648";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:11:"article_img";a:5:{i:0;s:11:"article_img";i:1;s:62:"assets/images/small/23-fevralya-den-zaschitnika-otechestva.jpg";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Поздравление с "Днем защитника Отечества" от нашей команды</title>
		<meta name="description" content=""/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

				<meta property="og:type" content="article">
		<meta property="og:url" content="http://www.top3seo.ru/[~130~]">
		<meta property="og:title" content="Немного запоздалое поздравление с 23 февраля!">
		<meta property="og:description" content="Пока все отмечали 23 февраля, мы плодотворно трудились и лишь недавно вспомнили, что так и не отметили День защитника Отечества. Женский коллектив Top3Seo поздравляет мужчин-коллег с праздником и желает им дальнейших профессиональных достижений. Наша дружная команда специалистов поможет вашим проектам занять достойное место под солнцем!">
		<meta property="og:image" content="http://www.top3seo.ru/assets/images/medium/23-fevralya-den-zaschitnika-otechestva.jpg">
		<script src="/js/social-likes.min.js"></script>
		<link rel="stylesheet" href="/css/social-likes_classic.css">
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li class="active"><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content news">
			
			<div class="left">
				
				<div class="text">
					<h1>Немного запоздалое поздравление с 23 февраля!</h1>
						<p>Мы настолько увлеклись работой, что упустили очень важное событие &ndash; 23 февраля. Пусть и с запозданием, но мы поздравляем нашу дружную мужскую команду с прошедшим&nbsp;&laquo;Днем защитника Отечества&raquo;.&nbsp;Ведь для нас главное&nbsp;&ndash; защитить и укрепить позиции наших клиентов в поисковых системах.</p>
<p>В феврале работа кипит еще более усиленно, а времени еще меньше. Как мы не старались, но у нас не получилось отметить 23 февраля. Именно поэтому мы решили отпраздновать данное событие уже весной вместе с нашей прекрасной половиной коллектива.</p>
<p>Команда Top3SEO продолжает свою успешную работу и поздравляет всех с наступающей весной!&nbsp;</p>
				</div>
				<div class="social">
<div class="social-likes social-likes_visible social-likes_ready" data-url="http://www.top3seo.ru/[~130~]" data-title="Немного запоздалое поздравление с 23 февраля!">
	<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
	<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
	<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
	<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
	<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
	<div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
</div>
					</div>
					
				<div class="clear"></div>
				
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	<li id="about"><a href="/o-nas/" title="О нас" id="about">О нас</a></li>
	
	
	
		<li id="news"><a href="/o-nas/novosti-i-akczii/" title="Новости и акции" id="news">Новости и акции</a></li>
	







</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				
			</div>
			<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
	</body>
</html>