<?php die('Unauthorized access.'); ?>a:40:{s:2:"id";s:3:"133";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:96:"Google Scraper Report будет следить за уникальностью контента";s:9:"longtitle";s:96:"Google Scraper Report сервис для борьбы с воровством контента ";s:11:"description";s:166:"Google предлагает вебмастерам отправлять жалобы на конкурентов, копирующих авторские тексты";s:5:"alias";s:63:"google-scraper-report-protiv-kopirovaniya-kontenta-konkurentami";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:2:"25";s:8:"isfolder";s:1:"0";s:9:"introtext";s:277:"По словам главного инженера Google Мэтта Каттса, пользователям крупнейшей поисковой сети стала доступна новая форма обращения под названием Google Scraper Report.";s:7:"content";s:1285:"<p>По словам главного инженера Google Мэтта Каттса, пользователям крупнейшей поисковой сети стала доступна новая форма обращения под названием <a href="https://docs.google.com/forms/d/1Pw1KVOVRyr4a7ezj_6SHghnX1Y6bp1SOVmy60QjkF0Y/viewform" target="_blank">Google Scraper Report</a>. С ее помощью вебмастера смогут информировать представителей поиска о том, что информация с их Интернет-ресурса была скопирована конкурентами.&nbsp;&nbsp;</p>
<p><img src="http://www.top3seo.ru/assets/images/blog/google-scraper-report.jpg" alt="Google Scraper Report" title="Google Scraper Report" style="vertical-align: middle;" height="198" width="480" /></p>
<p>Составляя фидбэк, представители поиска полагались на &laquo;Закон об авторском праве в цифровую эпоху&raquo;. Благодаря ему правообладатели могут требовать от Google удаления контента, который нарушает их права.&nbsp;&nbsp;&nbsp;&nbsp;</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"11";s:9:"menuindex";s:1:"4";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1393600508";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1393600508";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1393600508";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:11:"article_img";a:5:{i:0;s:11:"article_img";i:1;s:50:"assets/images/blog/small/google-scraper-report.jpg";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Google Scraper Report сервис для борьбы с воровством контента </title>
		<meta name="description" content="Google предлагает вебмастерам отправлять жалобы на конкурентов, копирующих авторские тексты"/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<meta property="og:type" content="article">
		<meta property="og:url" content="http://www.top3seo.ru/[~133~]">
		<meta property="og:title" content="Google Scraper Report будет следить за уникальностью контента">
		<meta property="og:description" content="По словам главного инженера Google Мэтта Каттса, пользователям крупнейшей поисковой сети стала доступна новая форма обращения под названием Google Scraper Report.">
		<meta property="og:image" content="http://www.top3seo.ru/assets/images/blog/medium/google-scraper-report.jpg">
		<script src="/js/social-likes.min.js"></script>
		<link rel="stylesheet" href="/css/social-likes_classic.css">
		<script type="text/javascript" src="//yandex.st/share/share.js"
charset="utf-8"></script>
						<script>
	new Ya.share({
        element: 'ya_share_top',
		theme: 'counter',
		link: 'http://www.top3seo.ru/[~133~]',
        title: 'Google Scraper Report будет следить за уникальностью контента',
		description: 'По словам главного инженера Google Мэтта Каттса, пользователям крупнейшей поисковой сети стала доступна новая форма обращения под названием Google Scraper Report.',
		image: 'http://www.top3seo.ru/assets/images/blog/medium/google-scraper-report.jpg',
        elementStyle: {
        	'border': true,
            'quickServices': ['yaru','vkontakte','facebook','twitter','odnoklassniki','moimir','gplus'],
			'type': 'small'
        }
	});
	
						</script>
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li class="active"><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content blog">
			
			<div class="left">
				
				<div class="text">
					<h1>Google Scraper Report будет следить за уникальностью контента</h1>
						<div class="post_date">
							<span class="pub_date">28</span>
							<span class="pub_month">февраля</span>
							<span class="pub_year">2014</span>
						</div>
						<div class="post_info">
							<span class="author">Опубликовал: <strong>Top3SEO</strong></span>
							<span class="category">В категории: <strong>SEO</strong></span>
							<span class="socshare" id="ya_share_top"></span>
						
						<div class="clear"></div>
					</div>
						<p>По словам главного инженера Google Мэтта Каттса, пользователям крупнейшей поисковой сети стала доступна новая форма обращения под названием <a href="https://docs.google.com/forms/d/1Pw1KVOVRyr4a7ezj_6SHghnX1Y6bp1SOVmy60QjkF0Y/viewform" target="_blank">Google Scraper Report</a>. С ее помощью вебмастера смогут информировать представителей поиска о том, что информация с их Интернет-ресурса была скопирована конкурентами.&nbsp;&nbsp;</p>
<p><img src="http://www.top3seo.ru/assets/images/blog/google-scraper-report.jpg" alt="Google Scraper Report" title="Google Scraper Report" style="vertical-align: middle;" height="198" width="480" /></p>
<p>Составляя фидбэк, представители поиска полагались на &laquo;Закон об авторском праве в цифровую эпоху&raquo;. Благодаря ему правообладатели могут требовать от Google удаления контента, который нарушает их права.&nbsp;&nbsp;&nbsp;&nbsp;</p>
					<div class="social">
<div class="social-likes social-likes_visible social-likes_ready" data-url="http://www.top3seo.ru/[~133~]" data-title="Google Scraper Report будет следить за уникальностью контента">
	<div class="facebook" title="Поделиться ссылкой на Фейсбуке">Facebook</div>
	<div class="twitter" title="Поделиться ссылкой в Твиттере">Twitter</div>
	<div class="mailru" title="Поделиться ссылкой в Моём мире">Мой мир</div>
	<div class="vkontakte" title="Поделиться ссылкой во Вконтакте">Вконтакте</div>
	<div class="odnoklassniki" title="Поделиться ссылкой в Одноклассниках">Одноклассники</div>
	<div class="plusone" title="Поделиться ссылкой в Гугл-плюсе">Google+</div>
</div>
					</div>
					<a href="[~25~]">Вернуться в категорию</a>
				</div>
				
				<div class="clear"></div>
				<div class="comments"><!-- KAMENT -->
<div id="kament_comments"></div>
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'top3seo';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function() {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/embed.js';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(node);
	})();
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT --></div>
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	
	
		<li ><a href="/blog/raznoe/" title="Разное" >Разное</a></li>
	







	
		<li ><a href="/blog/seo/" title="SEO" >SEO</a></li>
	







	
		<li ><a href="/blog/reklama/" title="Реклама" >Реклама</a></li>
	







	
		<li ><a href="/blog/soc-seti/" title="Соц сети" >Соц сети</a></li>
	







</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				
			</div>
			<div class="clear"></div>
			</div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
	</body>
</html>