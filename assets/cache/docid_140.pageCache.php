<?php die('Unauthorized access.'); ?>a:43:{s:2:"id";s:3:"140";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:78:"SEO-продвижение – двигатель Вашего бизнеса!";s:9:"longtitle";s:82:"Поисковое продвижение сайтов в Яндексе и Google";s:11:"description";s:261:"SEO продвижение является наиболее универсальным методом привлечения клиентов на сайт. Работы по продвижению сайта делятся на несколько этапов";s:5:"alias";s:16:"seo-prodvizhenie";s:15:"link_attributes";s:3:"seo";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"3";s:8:"isfolder";s:1:"1";s:9:"introtext";s:367:"Ищете свой сайт в Яндексе или Google и не можете найти? Сайт не приносит достаточное количество клиентов? Конкуренты заполонили просторы Интернета? Данная услуга поможет вам исправить это недоразумение.";s:7:"content";s:4730:"<p>У Вас бизнес, который нужно развить и улучшить? Вы хотите увеличить число клиентов, но не знаете, как? Вы слышали о том, что SEO &ndash; это эффективный инструмент для привлечения клиентов, но боитесь потратить время и деньги впустую?</p>
<p>Пришло время во всем разобраться.&nbsp;</p>
<h2>Что Вы получите от поискового продвижения</h2>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/hits.png" height="22" width="20" /></td>
<td style="height: 40px;" align="left" valign="middle">Увеличение количества обращений от потенциальных клиентов</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/result.png" height="22" width="22" /></td>
<td style="height: 40px;" align="left" valign="middle">Долговременный результат</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/trust.png" /></td>
<td style="height: 40px;" align="left" valign="middle">Доверие аудитории за счет первых позиций в поиске</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/cpv.png" /></td>
<td style="height: 40px;" align="left" valign="middle">Уменьшение стоимости привлечения клиентов</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/brand.png" height="22" width="22" /></td>
<td style="height: 40px;" align="left" valign="middle">Повышение узнаваемости бренда</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
{{order_button_in_seo}}
<h2>Как это работает</h2>
<p>{{howitworks}}</p>
<h2>Из чего складывается стоимость SEO</h2>
<p>Для каждого проекта стоимость определяется индивидуально и зависит от ряда факторов:</p>
<ul>
<li><span style="line-height: 1.5;">Уровень конкуренции (количество сайтов и активность конкурентов в интернете)<br /></span></li>
<li><span style="line-height: 1.5;">Сезонность тематики (уровень спроса на различные сезонные товары)<br /></span></li>
<li><span style="line-height: 1.5;">Количество ключевых слов</span></li>
<li><span style="line-height: 1.5;"></span><span style="line-height: 1.5;">География продвижения (города охвата рекламной кампанией)<br /></span></li>
<li><span style="line-height: 1.5;"><span style="line-height: 1.5;">Тип ресурса (корпоративный сайт, портал, интернет-магазин, визитка)<br /></span></span></li>
<li><span style="line-height: 1.5;">И<span style="line-height: 1.5;"> другие факторы...</span></span></li>
</ul>
<p><i>Минимальная стоимость заказа&nbsp;&ndash; 15 000 руб.<br /></i></p>
<h2>Распространённые заблуждения</h2>
<p>{{faq_block_seo}}</p>
<h2>Работа с Top3SEO &ndash; это:</h2>
<ul>
<li><span style="line-height: 1.5;">Отслеживание позиций в режиме онлайн в личном кабинете</span></li>
<li><span style="line-height: 1.5;">Юридические гарантии</span></li>
<li><span style="line-height: 1.5;">Прозрачная схема оплаты за результат</span></li>
<li><span style="line-height: 1.5;">Безопасные методы продвижения</span></li>
<li><span style="line-height: 1.5;">Персональный менеджер<br /></span></li>
<li><span style="line-height: 1.5;">Сотрудничество в развитии проекта</span></li>
</ul>
<p>Мы делаем все необходимое, чтобы Ваш сайт продавал.</p>
<h2>Нам доверяют</h2>
<p>{{clients_inside}}</p>
<h2>Как начать работу</h2>
<p>{{how_starting_seo}}</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"31";s:9:"menuindex";s:1:"0";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1397480546";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1401439837";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1397569676";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:26:"SEO-продвижение";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:14:"usluga_img_std";a:5:{i:0;s:14:"usluga_img_std";i:1;s:23:"images/services/seo.png";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:14:"usluga_img_hov";a:5:{i:0;s:14:"usluga_img_hov";i:1;s:29:"images/services/seo_hover.png";i:2;s:0:"";i:3;s:0:"";i:4;s:5:"image";}s:10:"howitworks";a:5:{i:0;s:10:"howitworks";i:1;s:3689:"{"fieldValue":[{"title":"Предварительные действия","image_unhov":"/images/hwork_seo/prel_unhov.png","image_hov":"/images/hwork_seo/prel_hov.png","text":"Приступая к работе с сайтом, важно провести глубокий анализ: определить целевую аудиторию конкретного сегмента рынка, изучить конкуренцию, оценить состояние сайта и на основе полученной информации подготовить стратегию продвижения. Все это поможет наиболее точно подобрать ключевые запросы и составить семантическое ядро, от чего напрямую зависит увеличение количества обращений."},{"title":"Внутренняя оптимизация","image_unhov":"/images/hwork_seo/inside_unhov.png","image_hov":"/images/hwork_seo/inside_hov.png","text":"<strong>Удобный сайт</strong> – это половина успеха. Чтобы сайт стал более посещаемым, следует провести оптимизацию структуры и устранить технические неполадки. Все настройки, в том числе и технические, мы производим в соответствии с <a href=\"http://help.yandex.ru/webmaster/yandex-indexing/quality-site.xml\" target=\"_blank\">рекомендациями Яндекса о качественных сайтах</a>. "},{"title":"Подготовка текстов","image_unhov":"/images/hwork_seo/text_unhov.png","image_hov":"/images/hwork_seo/text_hov.png","text":"<strong>Задача текста</strong> – не просто привлекать внимание, но и продавать. Поисковые системы, как и люди оценивают содержание текста, полноту информации и аккуратный вид. Исправление и создание текстового контента включено в стоимость продвижения. Вам нет нужды беспокоиться, мы всё сделаем сами."},{"title":"Внешняя оптимизация","image_unhov":"/images/hwork_seo/out_unhov.png","image_hov":"/images/hwork_seo/out_hov.png","text":"<strong>Внешняя оптимизация</strong> решает вопрос увеличения авторитетности вашего ресурса. Для этого на надежных сайтах с качественным контентом размещается ссылка, ведущая на Ваш сайт. Данное решение позволяет, помимо позиций в поиске, привлекать дополнительную аудиторию."},{"title":"Поддержка результата","image_unhov":"/images/hwork_seo/result_unhov.png","image_hov":"/images/hwork_seo/result_hov.png","text":"Важно не только <strong>продвинуть сайт в ТОП</strong>, но и удержать лидирующие позиции. Мы ежедневно отслеживаем изменения, в том числе и динамику позиций конкурентов. Такой метод позволяет оперативно разрабатывать контрмеры для поддержки положительного результата продвижения. "}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:7:"faq_seo";a:5:{i:0;s:7:"faq_seo";i:1;s:2740:"{"fieldValue":[{"ques":"Наша компания уже давно на рынке и нас отлично знают без интернета.","answ":"Безусловно, это хорошо, но границ между онлайн и офлайн бизнесом сегодня не существует, и если вас нет в интернете, Вы теряете своих клиентов. Каждый день. Разве Вам не хочется зарабатывать больше?"},{"ques":"Мы считаем, что нашему бизнесу достаточно контекстной рекламы.","answ":"Целевые посетители из поиска обходятся дешевле, чем из контекстной рекламы, даже несмотря на тематику. Разница может быть в два-три, а иногда и в десять раз. Да, конверсия с контекста высокая, но итоговый ROI (окупаемость инвестиций) у SEO все равно выше. Это не значит, что стоит полностью отказываться от контекста в пользу SEO, оптимальный вариант – совмещать."},{"ques":"Мы уже продвигали сайт, но клиентов не прибавилось.","answ":"Скорее всего Вам просто не повезло с подрядчиком. В случае профессионального SEO клиентов всегда становится больше."},{"ques":"Мы не готовы платить за SEO.","answ":"Выделить бюджет на продвижение сайта – непростой вопрос для многих. И Вы, наверное, задаётесь вопросом: «А что, если деньги будут потрачены впустую?» <br>\nТакое действительно возможно, но лишь в том случае, если Вашим проектом занимаются дилетанты. Выбрав добросовестного подрядчика Вы закрепите место в ТОПе и Ваш сайт будет работать на привлечение посетителей в долгосрочной перспективе.<br>\nЕсли уделять сайту должное внимание, клиенты Вас обязательно найдут. По статистике, многие компании получают 90% заказов именно из интернета. И это достижимо.\n"}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Поисковое продвижение сайтов в Яндексе и Google</title>
		<meta name="description" content="SEO продвижение является наиболее универсальным методом привлечения клиентов на сайт. Работы по продвижению сайта делятся на несколько этапов"/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<script src="/js/tabulous.js" type="text/javascript"></script>
		<link rel="stylesheet" href="/css/tabulous.css" />
		<script>
			$(document).ready(function($) {
				$('#how_work').tabulous({
					effect: 'scale'
				});
			});
			jQuery(document).ready(function(){
				jQuery('.answer').hide()
				jQuery('.question').click(function(){
					jQuery(this).toggleClass("closed").toggleClass("open").next().toggle(250)
					
				})
			})
		</script>
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li class="active"><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content services seo">
			
			<div class="left">
				<h1>SEO-продвижение – двигатель Вашего бизнеса!</h1>
				<div class="text"><p>У Вас бизнес, который нужно развить и улучшить? Вы хотите увеличить число клиентов, но не знаете, как? Вы слышали о том, что SEO &ndash; это эффективный инструмент для привлечения клиентов, но боитесь потратить время и деньги впустую?</p>
<p>Пришло время во всем разобраться.&nbsp;</p>
<h2>Что Вы получите от поискового продвижения</h2>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/hits.png" height="22" width="20" /></td>
<td style="height: 40px;" align="left" valign="middle">Увеличение количества обращений от потенциальных клиентов</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/result.png" height="22" width="22" /></td>
<td style="height: 40px;" align="left" valign="middle">Долговременный результат</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/trust.png" /></td>
<td style="height: 40px;" align="left" valign="middle">Доверие аудитории за счет первых позиций в поиске</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/cpv.png" /></td>
<td style="height: 40px;" align="left" valign="middle">Уменьшение стоимости привлечения клиентов</td>
</tr>
<tr>
<td style="height: 40px; width: 30px;" align="left" valign="middle"><img src="http://www.top3seo.ru/images/ico/obtaining/brand.png" height="22" width="22" /></td>
<td style="height: 40px;" align="left" valign="middle">Повышение узнаваемости бренда</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
				<div class="order_button">
					<a href="[~86~]?service=SEO Продвижение" class="poplight open_iframe" data-fancybox-type="iframe">Заказать продвижение</a>
				</div>
<h2>Как это работает</h2>
<p>[!multiTV? 
	&tvName=`howitworks` 
	&display=`all` 
	&outerTpl=`@CODE:
		<div id="how_work">
			[!multiTV? 
				&tvName=`howitworks` 
				&display=`all` 
				&outerTpl=`@CODE:<ul>((wrapper))</ul>`
				&rowTpl=`@CODE:<li>
					<a class="tab" href="#tabs-((iteration))" title="">
						<img src="((image_unhov))" class="un_hov" alt="((title))">
						<img src="((image_hov))" class="hov" alt="((title))">
						<span>((title))</span>
					</a>
				</li>`
			&outputSeparator=`<li class="separator"><img src="/images/hwork_seo/sepr.png" alt=""></li>`
			!]
			<div id="tabs_container">((wrapper))</div></div>`
	&rowTpl=`@CODE:<div id="tabs-((iteration))">((text))</div>`!]</p>
<h2>Из чего складывается стоимость SEO</h2>
<p>Для каждого проекта стоимость определяется индивидуально и зависит от ряда факторов:</p>
<ul>
<li><span style="line-height: 1.5;">Уровень конкуренции (количество сайтов и активность конкурентов в интернете)<br /></span></li>
<li><span style="line-height: 1.5;">Сезонность тематики (уровень спроса на различные сезонные товары)<br /></span></li>
<li><span style="line-height: 1.5;">Количество ключевых слов</span></li>
<li><span style="line-height: 1.5;"></span><span style="line-height: 1.5;">География продвижения (города охвата рекламной кампанией)<br /></span></li>
<li><span style="line-height: 1.5;"><span style="line-height: 1.5;">Тип ресурса (корпоративный сайт, портал, интернет-магазин, визитка)<br /></span></span></li>
<li><span style="line-height: 1.5;">И<span style="line-height: 1.5;"> другие факторы...</span></span></li>
</ul>
<p><i>Минимальная стоимость заказа&nbsp;&ndash; 15 000 руб.<br /></i></p>
<h2>Распространённые заблуждения</h2>
<p>[!multiTV? 
	&tvName=`faq_seo` 
	&display=`all` 
!]</p>
<h2>Работа с Top3SEO &ndash; это:</h2>
<ul>
<li><span style="line-height: 1.5;">Отслеживание позиций в режиме онлайн в личном кабинете</span></li>
<li><span style="line-height: 1.5;">Юридические гарантии</span></li>
<li><span style="line-height: 1.5;">Прозрачная схема оплаты за результат</span></li>
<li><span style="line-height: 1.5;">Безопасные методы продвижения</span></li>
<li><span style="line-height: 1.5;">Персональный менеджер<br /></span></li>
<li><span style="line-height: 1.5;">Сотрудничество в развитии проекта</span></li>
</ul>
<p>Мы делаем все необходимое, чтобы Ваш сайт продавал.</p>
<h2>Нам доверяют</h2>
<p><div class="brands_carousel_inside_page">
				<ul class="brands" id="brands_carousel_inside_page">
			[!Ditto? &tpl=`clientTpl` &parents=`4` &depth=`5` &hideFolders=`1` &randomize=`1` &showInMenuOnly=`1`!]
			</ul>
				
				</div></p>
<h2>Как начать работу</h2>
<p><div class="order_button">
	<span>Нажмите на кнопку</span>
				<a href="[~86~]?service=SEO Продвижение" class="poplight open_iframe" data-fancybox-type="iframe">Заказать продвижение</a>
	<span>или позвоните:</span>
	<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				</div></p></div>
				
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	<li class="active" ><span id="seo">SEO-продвижение</span>

<ul>
	<li class="active"><span title="SEO-продвижение – двигатель Вашего бизнеса!">Описание услуги</span></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`140` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="kontekst">Контекстная реклама</span>

<ul>
	<li><a href="[~9~]" title="Контекстная реклама – ставка на эффективность!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`9` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="audit">Аудит сайта</span>

<ul>
	<li><a href="[~8~]" title="Аудит сайта">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`8` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="last" ><span id="copy">Копирайтинг</span>

<ul>
	<li><a href="[~10~]" title="Копирайтинг">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`10` &orderBy=`menuindex ASC`!]
</ul>

</li>






</ul>
				</div>
				
				<div class="metrika">
					<div class="descr">
						<span class="big">Результат нашей работы</span>
						<span class="small">За последние 30 дней на сайты наших клиентов перешло:</span>
					</div>
					[!Metrika? &tpl=`metrika_sidebar` &delimiter=`1`!]
					<div class="clear"></div>
				</div>
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]
			</div>
			<div class="clear"></div>
			</div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
		<!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'top3seo';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/counter.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
	</body>
</html>