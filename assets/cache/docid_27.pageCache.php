<?php die('Unauthorized access.'); ?>a:43:{s:2:"id";s:2:"27";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:57:"Продвижение по ключевым словам";s:9:"longtitle";s:157:"Продвижение сайта по ключевым словам. Улучшение видимости сайта в поисковых системах";s:11:"description";s:259:"Услуги по продвижению сайтов в ТОП поисковых систем по ключевым словам. Продвижение по позициям с оплатой по факту или по фиксированной цене.";s:5:"alias";s:33:"prodvizhenie-po-klyuchevym-slovam";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:3:"140";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:8970:"<p><strong style="line-height: 1.5;">Представьте:</strong>&nbsp;ваш потенциальный клиент открывает страницу поиска, вводит название товара, который вы продаете, и видит ТОП-10 сайтов. Каждый из этих сайтов предлагает именно то, что посетитель хочет купить.</p>
<p>Но вас в этой десятке нет!</p>
<p>А где вы?</p>
<p>Вы за пределами видимости потенциального клиента.</p>
<p>Решить эту проблему поможет продвижение&nbsp;по позициям в ТОП поисковой выдачи Яндекса и Google.</p>
<p><img alt="Продвижение вашего сайта с оплатой по позициям" src="http://top3seo.ru/assets/images/uslugi/seo/seo-po-poziciyam.png" title="Продвижение по позициям" style="vertical-align: middle;" height="175" width="604" /></p>
<h2>Для чего необходимо продвижение на высокие позиции?</h2>
<p>Продвижение по позициям&nbsp;позволит вашему сайту попасть под пристальный взгляд потенциального клиента. Чем выше позиции ваших страниц в результатах поиска, тем больше шансов на то, что пользователь перейдёт к вам на сайт.</p>
<p>По статистике, за пределы ТОП-10 результатов поиска не переходят 90% пользователей. Сайты из ТОП-3 забирают себе в среднем 60% посетителей. Находясь в ТОП-5, сайт может рассчитывать на 10% переходов. Ресурсы на 6-10 позициях получают порядка 5% трафика.</p>
<p>Перейдя на страничку вашего сайта и изучив предложение, посетитель захочет приобрести вашу продукцию, потому что он её искал.</p>
<p>Конечно, он может сравнить ваш товар или услугу с аналогичными предложениями в сети. Но в данном случае серьёзно повлиять на выбор клиента в вашу пользу поможет <a href="http://www.top3seo.ru/[~39~]" title="Маркетинговый аудит">проведение маркетингового аудита</a>, который направлен на удержание пользователя в рамках вашего сайта.</p>
<h2>Без чего не обойтись?</h2>
<p>Качественный контент &ndash;&nbsp;это одна из самых важных составляющих сайта, на которую стоит обратить пристальное внимание. <strong>В стоимость SEO-продвижения входит <a href="http://www.top3seo.ru/[~10~]" title="Написание текстов">подготовка текстов</a></strong>, удовлетворяющих запросы посетителя.</p>
<h2>Стоимость ежемесячного продвижения</h2>
<p><span style="color: #333399;"><strong>Вариант №1.&nbsp;Традиционная схема работы &ndash;&nbsp;с фиксированной ценой.</strong></span></p>
<p><em>Ежемесячная стоимость продвижения сайта фиксируется в договоре на основании согласованного списка ключевых слов и остаётся неизменной до момента внесения корректировок. Регулируется с помощью дополнительного соглашения.</em></p>
<p>Стоимость услуги от <strong>15000 рублей в месяц</strong></p>
<p><strong>Расчет стоимости услуг осуществляется в зависимости от:</strong></p>
<ul>
<li>Количества ключевых запросов и их популярности;</li>
<li>Уровня конкуренции в продвигаемой тематике;</li>
<li>Текущего состояния сайта;</li>
<li>Охвата регионов;</li>
<li>Других параметров продвижения.</li>
</ul>
<p>Минимальный срок заключения контракта &ndash; <strong>6 месяцев</strong>.</p>
<p><strong>Дополнительные услуги и бонусы:</strong></p>
<ul>
<li>&nbsp; &nbsp;При стоимости продвижения в месяц от <strong>30000 руб.</strong> ежедневный отчет на E-mail.</li>
<li>* При стоимости продвижения&nbsp;в месяц от <strong>50000 руб.</strong> ежедневный отчёт на E-mail, поисковый аудит бесплатно в первый месяц работ.</li>
<li>* При стоимости продвижения&nbsp;в месяц от <strong>100000 руб.</strong> ежедневный отчёт на E-mail, поисковый и маркетинговый аудит бонусом каждые 6 месяцев.</li>
</ul>
<p>*&nbsp;<em>Бесплатное создание страниц в социальных сетях.</em></p>
<p><span style="color: #333399;"><strong>Вариант №2. Современная схема работы &ndash;&nbsp;с оплатой за результат.</strong></span></p>
<p><em>Счёт выставляется по факту оказанных услуг на основании зафиксированных позиций ключевых запросов с применением системы коэффициентов. Наибольший коэффициент стоимости = 2, при условии, что запрос находился в ТОП1.&nbsp;<em>Первые три месяца оплата производится по фиксированной ставке с коэффициентом 1.</em></em></p>
<p style="text-align: center;"><em><em><em><img src="http://www.top3seo.ru/assets/images/koeff.jpg" height="342" width="600" /></em></em></em></p>
<p>В зависимости от ежедневных позиций каждого запроса в течение месяца его цена может меняться. Таким образом, если запрос попадает в какие-то дни в ТОП 1, то он будет стоить дороже, если он занимает более низкую позицию &ndash; его стоимость соответственно ниже, а если запроса <strong>нет в ТОП 20</strong> &ndash; просто <strong>не платите ничего</strong>.</p>
<p>Наша команда работает по прозрачной схеме, в которой самую главную роль играет мотивация и достижение целей Клиента. Мы стремимся сделать так, чтобы по каждому запросу ваш сайт занимал только передовые позиции. Благодаря этому ваш ресурс становится еще более популярным, а мы получаем дополнительные бонусы.</p>
<div>* <i>В услугу входит ежедневный отчёт на E-mail, а также ежемесячный срез с отображением динамики за прошедший месяц.</i></div>
<div><i>&nbsp;</i></div>
<div><i></i>Минимальный срок заключения контракта &ndash;&nbsp;<strong style="line-height: 1.5;">6 месяцев</strong>.</div>
<p><strong>Расчет стоимости услуг осуществляется в зависимости от:</strong></p>
<ul>
<li>Количества ключевых запросов и их популярности;</li>
<li>Уровня конкуренции в продвигаемой тематике;</li>
<li>Текущего состояния сайта;</li>
<li>Охвата регионов;</li>
<li>Других параметров продвижения.</li>
</ul>
<p style="text-align: center;"><strong><a href="http://www.top3seo.ru/[~7~]" title="Этапы продвижения сайта">Ознакомьтесь с этапами работ над вашим сайтом</a></strong></p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"14";s:9:"menuindex";s:1:"0";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1383835116";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1397569740";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1383835116";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:34:"По ключевым словам";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:9:"dopl_cont";a:5:{i:0;s:9:"dopl_cont";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:8:"richtext";}s:5:"price";a:5:{i:0;s:5:"price";i:1;s:182:"{"fieldValue":[{"bonus":"Бюджет (руб.)","micro":"от 15 000","optima":"от 30 000","business":"от 50 000","premium":"от 100 000"}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:9:"tarifswrd";a:5:{i:0;s:9:"tarifswrd";i:1;s:187:"{"fieldValue":[{"bonus":"Продвижение ключевых фраз","micro":"50-100","optima":"100-200","business":"200-500","premium":"500+"}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:10:"tarifstext";a:5:{i:0;s:10:"tarifstext";i:1;s:6130:"{"fieldValue":[{"descr":"В данную услугу входит настройка всех внутренних элементов продвигаемых страниц. Заполнение мета-тегов, создание и настройка технических файлов (robots.txt, sitemap.xml, .htaccess), оптимизация HTML кода, текстов и изображений. А так же рекомендации по оптимизации других страниц сайта.","bonus":"Оптимизация сайта","micro":"+","optima":"+","business":"+","premium":"+"},{"descr":"\nВ стоимость продвижения входит подготовка текстов для посадочных страниц (Landing Page).","bonus":"Подготовка текстов","micro":"+","optima":"+","business":"+","premium":"+"},{"descr":"\nЭкспресс-аудит выявит основные препятствия для продвижения сайта по ключевым словам. Сформирует видимость ключевых параметров продвигаемого сайта в целом.\n","bonus":"Экспресс аудит","micro":"+","optima":"+","business":"","premium":""},{"descr":"Поисковый аудит включает в себя глубокий анализ структуры, контента, сезонности и множества других важных показателей тематики и сайта. Подробнее ","bonus":"Поисковый аудит","micro":"","optima":"","business":"+","premium":"+"},{"descr":"\nМаркетинговый аудит нацелен на увеличение эффективности бизнеса. В него входит: анализ контента, целевой аудитории, рыночного сегмента, а так же сопоставление текущих целей с опытом зарубежных коллег. Подробнее\n","bonus":"Маркетинговый аудит","micro":"","optima":"","business":"","premium":"+"},{"descr":"\nПо желанию, мы можем настроить E-mail рассылку на ваш почтовый ящик и каждый раз, когда будет собираться статистика, она будет приходить к вам на E-mail.","bonus":"Ежедневный отчёт на e-mail","micro":"","optima":"+","business":"+","premium":"+"},{"descr":"\nВ ходе продвижения мы отслеживаем запросы, по которым отсутствуют переходы и предлагаем исключить их из списка продвижения, тем самым экономя ваш бюджет.\n","bonus":"Поиск неэффективных фраз","micro":"","optima":"+","business":"+","premium":"+"},{"descr":"\nВнедрение микроразметки в HTML код сайта. При обработке информации, поисковая система подхватывает размеченные данные и выводит в результатах поиска. Данная услуга делает сниппет в поисковой выдаче наиболее привлекательным, что увеличивает количество переходов.\n","bonus":"Создание расширенных сниппетов","micro":"","optima":"","business":"+","premium":"+"},{"descr":"В ходе сотрудничества с нами у вас могут возникнуть различные вопросы. Наши специалисты с удовольствием ответят на все ваши вопросы, связанные с технической или стратегической частью.","bonus":"Консультация специалиста","micro":"+","optima":"1 час","business":"2 часа","premium":"4 часа"},{"descr":"Систематическое появление на сайте нового контента показывает, как пользователям, так и поисковым системам, что сайт \"жив\", а информация, размещённая на нём актуальна. Это один из важных факторов, которые учитываются при ранжировании сайта в поисковой выдаче. Необходимо поддерживать актуальность информации и давать посетителям свежую информацию.","bonus":"Написание статей","micro":"","optima":"1","business":"2","premium":"4"},{"descr":"Мы создаём страницы в социальных сетях для вашей компании опционально: ( Google, YouTube, Vkontakte, FaceBook, Odnoklassniki). Это позволит ускорить и повысить качество индексации вашего сайта.\nОднако, данные страницы рекомендуется не оставлять на произвол судьбы, а регулярно наполнять их контентом. Данная услуга плотно связана с услугой SMM.","bonus":"Создание страниц в соц. сетях","micro":"","optima":"","business":"G+Y+F+V","premium":"G+Y+F+V+O"},{"descr":"В первые три <a href=\"/\">месяца</a> мы берём фиксированную оплату за продвижение, поскольку, для достижения высоких результатов необходимо время. Для каждого сайта, время достижения цели может быть разное, поэтому мы его усреднили. ","bonus":"Стоимость в первые 3 месяца","micro":"100%","optima":"75%","business":"50%","premium":"25%"}],"fieldSettings":{"autoincrement":1}}";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Продвижение сайта по ключевым словам. Улучшение видимости сайта в поисковых системах</title>
		<meta name="description" content="Услуги по продвижению сайтов в ТОП поисковых систем по ключевым словам. Продвижение по позициям с оплатой по факту или по фиксированной цене."/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<link href="/css/features-table.css" rel="stylesheet" type="text/css">
	
		
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li class="active"><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content services ">
			
			<div class="left">
				<h1>Продвижение по ключевым словам</h1>
				<div class="text"><p><strong style="line-height: 1.5;">Представьте:</strong>&nbsp;ваш потенциальный клиент открывает страницу поиска, вводит название товара, который вы продаете, и видит ТОП-10 сайтов. Каждый из этих сайтов предлагает именно то, что посетитель хочет купить.</p>
<p>Но вас в этой десятке нет!</p>
<p>А где вы?</p>
<p>Вы за пределами видимости потенциального клиента.</p>
<p>Решить эту проблему поможет продвижение&nbsp;по позициям в ТОП поисковой выдачи Яндекса и Google.</p>
<p><img alt="Продвижение вашего сайта с оплатой по позициям" src="http://top3seo.ru/assets/images/uslugi/seo/seo-po-poziciyam.png" title="Продвижение по позициям" style="vertical-align: middle;" height="175" width="604" /></p>
<h2>Для чего необходимо продвижение на высокие позиции?</h2>
<p>Продвижение по позициям&nbsp;позволит вашему сайту попасть под пристальный взгляд потенциального клиента. Чем выше позиции ваших страниц в результатах поиска, тем больше шансов на то, что пользователь перейдёт к вам на сайт.</p>
<p>По статистике, за пределы ТОП-10 результатов поиска не переходят 90% пользователей. Сайты из ТОП-3 забирают себе в среднем 60% посетителей. Находясь в ТОП-5, сайт может рассчитывать на 10% переходов. Ресурсы на 6-10 позициях получают порядка 5% трафика.</p>
<p>Перейдя на страничку вашего сайта и изучив предложение, посетитель захочет приобрести вашу продукцию, потому что он её искал.</p>
<p>Конечно, он может сравнить ваш товар или услугу с аналогичными предложениями в сети. Но в данном случае серьёзно повлиять на выбор клиента в вашу пользу поможет <a href="http://www.top3seo.ru/[~39~]" title="Маркетинговый аудит">проведение маркетингового аудита</a>, который направлен на удержание пользователя в рамках вашего сайта.</p>
<h2>Без чего не обойтись?</h2>
<p>Качественный контент &ndash;&nbsp;это одна из самых важных составляющих сайта, на которую стоит обратить пристальное внимание. <strong>В стоимость SEO-продвижения входит <a href="http://www.top3seo.ru/[~10~]" title="Написание текстов">подготовка текстов</a></strong>, удовлетворяющих запросы посетителя.</p>
<h2>Стоимость ежемесячного продвижения</h2>
<p><span style="color: #333399;"><strong>Вариант №1.&nbsp;Традиционная схема работы &ndash;&nbsp;с фиксированной ценой.</strong></span></p>
<p><em>Ежемесячная стоимость продвижения сайта фиксируется в договоре на основании согласованного списка ключевых слов и остаётся неизменной до момента внесения корректировок. Регулируется с помощью дополнительного соглашения.</em></p>
<p>Стоимость услуги от <strong>15000 рублей в месяц</strong></p>
<p><strong>Расчет стоимости услуг осуществляется в зависимости от:</strong></p>
<ul>
<li>Количества ключевых запросов и их популярности;</li>
<li>Уровня конкуренции в продвигаемой тематике;</li>
<li>Текущего состояния сайта;</li>
<li>Охвата регионов;</li>
<li>Других параметров продвижения.</li>
</ul>
<p>Минимальный срок заключения контракта &ndash; <strong>6 месяцев</strong>.</p>
<p><strong>Дополнительные услуги и бонусы:</strong></p>
<ul>
<li>&nbsp; &nbsp;При стоимости продвижения в месяц от <strong>30000 руб.</strong> ежедневный отчет на E-mail.</li>
<li>* При стоимости продвижения&nbsp;в месяц от <strong>50000 руб.</strong> ежедневный отчёт на E-mail, поисковый аудит бесплатно в первый месяц работ.</li>
<li>* При стоимости продвижения&nbsp;в месяц от <strong>100000 руб.</strong> ежедневный отчёт на E-mail, поисковый и маркетинговый аудит бонусом каждые 6 месяцев.</li>
</ul>
<p>*&nbsp;<em>Бесплатное создание страниц в социальных сетях.</em></p>
<p><span style="color: #333399;"><strong>Вариант №2. Современная схема работы &ndash;&nbsp;с оплатой за результат.</strong></span></p>
<p><em>Счёт выставляется по факту оказанных услуг на основании зафиксированных позиций ключевых запросов с применением системы коэффициентов. Наибольший коэффициент стоимости = 2, при условии, что запрос находился в ТОП1.&nbsp;<em>Первые три месяца оплата производится по фиксированной ставке с коэффициентом 1.</em></em></p>
<p style="text-align: center;"><em><em><em><img src="http://www.top3seo.ru/assets/images/koeff.jpg" height="342" width="600" /></em></em></em></p>
<p>В зависимости от ежедневных позиций каждого запроса в течение месяца его цена может меняться. Таким образом, если запрос попадает в какие-то дни в ТОП 1, то он будет стоить дороже, если он занимает более низкую позицию &ndash; его стоимость соответственно ниже, а если запроса <strong>нет в ТОП 20</strong> &ndash; просто <strong>не платите ничего</strong>.</p>
<p>Наша команда работает по прозрачной схеме, в которой самую главную роль играет мотивация и достижение целей Клиента. Мы стремимся сделать так, чтобы по каждому запросу ваш сайт занимал только передовые позиции. Благодаря этому ваш ресурс становится еще более популярным, а мы получаем дополнительные бонусы.</p>
<div>* <i>В услугу входит ежедневный отчёт на E-mail, а также ежемесячный срез с отображением динамики за прошедший месяц.</i></div>
<div><i>&nbsp;</i></div>
<div><i></i>Минимальный срок заключения контракта &ndash;&nbsp;<strong style="line-height: 1.5;">6 месяцев</strong>.</div>
<p><strong>Расчет стоимости услуг осуществляется в зависимости от:</strong></p>
<ul>
<li>Количества ключевых запросов и их популярности;</li>
<li>Уровня конкуренции в продвигаемой тематике;</li>
<li>Текущего состояния сайта;</li>
<li>Охвата регионов;</li>
<li>Других параметров продвижения.</li>
</ul>
<p style="text-align: center;"><strong><a href="http://www.top3seo.ru/[~7~]" title="Этапы продвижения сайта">Ознакомьтесь с этапами работ над вашим сайтом</a></strong></p></div>
				
				<div class="text">
					
<script language="javascript"> function togle_par_about(par_id){ if($("#"+par_id).is(":visible")){ $("#"+par_id).attr("style","display: none"); } else{ $(".par-q-link").attr("style","visibility: hidden"); $("#"+par_id).parent().find(".par-q-link").attr("style","visibility: visible"); $(".par-about-q").attr("style","display: none"); $("#"+par_id).attr("style","display: "); } }  $(".pdinfohead").live("hover",function(){ $(this).find(".par-q-link").attr("style","visibility: visible"); });  $(".pdinfohead").live("mouseleave",function(){ if(!($(this).find(".par-about-q").is(":visible"))){ $(this).find(".par-q-link").attr("style","visibility: hidden"); } });  $("body").click(function(e) { if($(e.target).attr("class") != "par-link" && $(e.target).attr("class") != "par-q-link" && $(e.target).attr("class") != "par-about-head" && $(e.target).attr("class") != "par-about-inner"){ $(".par-about-q:visible").parent().find(".par-q-link").attr("style","visibility: hidden"); $(".par-about-q").attr("style","display: none"); } }); </script>
				</div>
				<div class="order_button">
				<a href="[~86~]?service=Продвижение по ключевым словам" class="poplight open_iframe" data-fancybox-type="iframe">Заказать продвижение</a>
				</div>
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	<li class="active" ><span id="seo">SEO-продвижение</span>

<ul>
	<li><a href="[~140~]" title="SEO-продвижение – двигатель Вашего бизнеса!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`140` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="kontekst">Контекстная реклама</span>

<ul>
	<li><a href="[~9~]" title="Контекстная реклама – ставка на эффективность!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`9` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="audit">Аудит сайта</span>

<ul>
	<li><a href="[~8~]" title="Аудит сайта">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`8` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="last" ><span id="copy">Копирайтинг</span>

<ul>
	<li><a href="[~10~]" title="Копирайтинг">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`10` &orderBy=`menuindex ASC`!]
</ul>

</li>






</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]
			</div>
			<div class="clear"></div>
			</div>
			
				<div class="clear"></div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
	</body>
</html>