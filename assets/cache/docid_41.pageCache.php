<?php die('Unauthorized access.'); ?>a:42:{s:2:"id";s:2:"41";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:46:"Аудит рекламной кампании";s:9:"longtitle";s:113:"Проведение аудита рекламных кампаний Яндекс.Директ и Google AdWords";s:11:"description";s:243:"Чтобы найти ошибки в настройках кампании Яндекс.Директ или Google Adwords закажите у наших специалистов услугу "Аудит рекламной кампании". ";s:5:"alias";s:24:"audit-reklamnoj-kampanii";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"9";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:1640:"<p>Вы запустили рекламную кампанию, но ее результаты не соответствуют ожиданиям? Проведите аудит, который поможет выявить ошибки и неточности в настройках кампании Яндекс.Директ и Google Adwords, а также позволит определить, как добиться максимальной отдачи от рекламы.</p>
<h2>Аудит включает:</h2>
<p>&bull; анализ рекламных площадок и их структуры;</p>
<p>&bull; анализ ключевых запросов, текстов объявлений;</p>
<p>&bull; оценку распределения бюджета;</p>
<p>&bull; составление рекомендаций.</p>
<p>&nbsp;</p>
<p>При настройке рекламной кампании не всегда мелким нюансам уделяется достаточно времени. Накапливаясь, они могут привести к лавинному эффекту и снижению эффективности кампании. Грамотно проведенный аудит рекламной кампании позволит своевременно внести изменения в стратегию, исключить показ объявлений нецелевой аудитории, рационально распланировать рекламный бюджет и увеличить число переходов по объявлениям.</p>";s:8:"richtext";s:1:"1";s:8:"template";s:2:"29";s:9:"menuindex";s:1:"2";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1383835350";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1393426675";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1383835350";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:0:"";s:7:"donthit";s:1:"0";s:11:"haskeywords";s:1:"0";s:11:"hasmetatags";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:5:"price";a:5:{i:0;s:5:"price";i:1;s:2:"[]";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:9:"tarifswrd";a:5:{i:0;s:9:"tarifswrd";i:1;s:2:"[]";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:10:"tarifstext";a:5:{i:0;s:10:"tarifstext";i:1;s:2:"[]";i:2;s:0:"";i:3;s:0:"";i:4;s:9:"custom_tv";}s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!doctype html>
<html>
    <head>
		        <base href="http://www.top3seo.ru/" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


  		<title>Проведение аудита рекламных кампаний Яндекс.Директ и Google AdWords</title>
		<meta name="description" content="Чтобы найти ошибки в настройках кампании Яндекс.Директ или Google Adwords закажите у наших специалистов услугу "Аудит рекламной кампании". "/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-48171662-1', 'top3seo.ru');
			ga('send', 'pageview');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$('input[placeholder], textarea[placeholder]').placeholder();});</script>

		<link href="/css/features-table.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N3XKVC');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>
		
		<div id="header">
						<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					<ul class="nav"><li><a href="http://www.top3seo.ru/" title="Главная" class="">Главная</a></li>
<li><a href="/o-nas/" title="О нас" class="about">О нас</a></li>
<li class="active"><a href="/uslugi/" title="Услуги" class="">Услуги</a></li>
<li><a href="/klientyi/" title="Клиенты" class="">Клиенты</a></li>
<li><a href="/blog/" title="Блог" class="">Блог</a></li>
<li class="last"><a href="/contacts" title="Контакты" class="">Контакты</a></li>
</ul>
			</div>
<div class="clear"></div>

		</div>
								
		<div class="breadcrumbs">
			[!Breadcrumbs &crumbSeparator=` <span class="separator">\</span> `!]
		</div>
									
		<div id="main">
			<div class="content services ">
			
			<div class="left">
				<h1>Аудит рекламной кампании</h1>
				<div class="text"><p>Вы запустили рекламную кампанию, но ее результаты не соответствуют ожиданиям? Проведите аудит, который поможет выявить ошибки и неточности в настройках кампании Яндекс.Директ и Google Adwords, а также позволит определить, как добиться максимальной отдачи от рекламы.</p>
<h2>Аудит включает:</h2>
<p>&bull; анализ рекламных площадок и их структуры;</p>
<p>&bull; анализ ключевых запросов, текстов объявлений;</p>
<p>&bull; оценку распределения бюджета;</p>
<p>&bull; составление рекомендаций.</p>
<p>&nbsp;</p>
<p>При настройке рекламной кампании не всегда мелким нюансам уделяется достаточно времени. Накапливаясь, они могут привести к лавинному эффекту и снижению эффективности кампании. Грамотно проведенный аудит рекламной кампании позволит своевременно внести изменения в стратегию, исключить показ объявлений нецелевой аудитории, рационально распланировать рекламный бюджет и увеличить число переходов по объявлениям.</p></div>
				
				
				<div class="order_button">
				<a href="[~108~]?service=Аудит рекламной кампании" class="poplight open_iframe" data-fancybox-type="iframe">Заказать аудит</a>
				</div>
			</div>
							<div class="right">

				<div class="right_menu">
				<ul class="accordion">
	
	
	<li ><span id="seo">SEO-продвижение</span>

<ul>
	<li><a href="[~140~]" title="SEO-продвижение – двигатель Вашего бизнеса!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`140` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="active" ><span id="kontekst">Контекстная реклама</span>

<ul>
	<li><a href="[~9~]" title="Контекстная реклама – ставка на эффективность!">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`9` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li ><span id="audit">Аудит сайта</span>

<ul>
	<li><a href="[~8~]" title="Аудит сайта">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`8` &orderBy=`menuindex ASC`!]
</ul>

</li>





<li class="last" ><span id="copy">Копирайтинг</span>

<ul>
	<li><a href="[~10~]" title="Копирайтинг">Описание услуги</a></li>
[!Ditto? tpl=`innerRowTpl_dt` &parents=`10` &orderBy=`menuindex ASC`!]
</ul>

</li>






</ul>
				</div>
				
				
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]
			</div>
			<div class="clear"></div>
			</div>
		</div>
									<!-- ПОДВАЛ -->
		<div id="footer">
					<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число посетителей за"+
" сегодня' "+
"border='0' width='88' height='15'><\/a>")
//--></script><!--/LiveInternet-->
</div>


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>

		</div>
		<!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = 'top3seo';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true;
		node.src = 'http://' + kament_subdomain + '.svkament.ru/js/counter.js';
		(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->
	</body>
</html>