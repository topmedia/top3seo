<?php
$c=&$this->config;
$c['manager_theme'] = "MODxRE";
$c['settings_version'] = "1.0.12";
$c['show_meta'] = "0";
$c['server_offset_time'] = "0";
$c['server_protocol'] = "http";
$c['manager_language'] = "russian-UTF8";
$c['modx_charset'] = "UTF-8";
$c['site_name'] = "Top3Seo";
$c['site_start'] = "1";
$c['error_page'] = "73";
$c['unauthorized_page'] = "74";
$c['site_status'] = "1";
$c['site_unavailable_message'] = "The site is currently unavailable";
$c['track_visitors'] = "0";
$c['top_howmany'] = "10";
$c['auto_template_logic'] = "sibling";
$c['default_template'] = "3";
$c['old_template'] = "3";
$c['publish_default'] = "1";
$c['cache_default'] = "1";
$c['search_default'] = "1";
$c['friendly_urls'] = "1";
$c['friendly_url_prefix'] = "";
$c['friendly_url_suffix'] = "";
$c['friendly_alias_urls'] = "1";
$c['use_alias_path'] = "1";
$c['use_udperms'] = "1";
$c['udperms_allowroot'] = "0";
$c['failed_login_attempts'] = "5";
$c['blocked_minutes'] = "20";
$c['use_captcha'] = "0";
$c['captcha_words'] = "MODX,Access,Better,BitCode,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Tattoo,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";
$c['emailsender'] = "info@top3seo.ru";
$c['email_method'] = "mail";
$c['smtp_auth'] = "0";
$c['smtp_host'] = "";
$c['smtp_port'] = "25";
$c['smtp_username'] = "";
$c['emailsubject'] = "Подтверждение регистрации";
$c['number_of_logs'] = "100";
$c['number_of_messages'] = "30";
$c['number_of_results'] = "20";
$c['use_editor'] = "1";
$c['use_browser'] = "1";
$c['rb_base_dir'] = "/home/web/www/clients/client12/web38/web/assets/";
$c['rb_base_url'] = "assets/";
$c['which_editor'] = "TinyMCE";
$c['fe_editor_lang'] = "russian-UTF8";
$c['fck_editor_toolbar'] = "standard";
$c['fck_editor_autolang'] = "0";
$c['editor_css_path'] = "";
$c['editor_css_selectors'] = "";
$c['strip_image_paths'] = "0";
$c['upload_images'] = "bmp,ico,gif,jpeg,jpg,png,psd,tif,tiff";
$c['upload_media'] = "au,avi,mp3,mp4,mpeg,mpg,wav,wmv";
$c['upload_flash'] = "fla,flv,swf";
$c['upload_files'] = "aac,au,avi,css,cache,doc,docx,gz,gzip,htaccess,htm,html,js,mp3,mp4,mpeg,mpg,ods,odp,odt,pdf,ppt,pptx,rar,tar,tgz,txt,wav,wmv,xls,xlsx,xml,z,zip";
$c['upload_maxsize'] = "1048576";
$c['new_file_permissions'] = "0644";
$c['new_folder_permissions'] = "0755";
$c['filemanager_path'] = "/home/web/www/clients/client12/web38/web/";
$c['theme_refresher'] = "";
$c['manager_layout'] = "4";
$c['custom_contenttype'] = "application/rss+xml,application/pdf,application/vnd.ms-word,application/vnd.ms-excel,text/html,text/css,text/xml,text/javascript,text/plain,application/json";
$c['auto_menuindex'] = "1";
$c['session.cookie.lifetime'] = "604800";
$c['mail_check_timeperiod'] = "60";
$c['manager_direction'] = "ltr";
$c['tinymce_editor_theme'] = "custom";
$c['tinymce_custom_plugins'] = "style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media, umispoiler, youtubeIframe, table";
$c['tinymce_custom_buttons1'] = "undo,redo,|,bold,forecolor,backcolor,strikethrough,formatselect,fontsizeselect,code, umispoiler";
$c['tinymce_custom_buttons2'] = "image,media,youtubeIframe,link,unlink,anchor,|,bullist,numlist,|,blockquote,outdent,indent,|,justifyleft,justifycenter,justifyright,|,hr,|,template,visualblocks,styleprops,removeformat,|,pastetext,pasteword ";
$c['tree_show_protected'] = "0";
$c['rss_url_news'] = "http://feeds.feedburner.com/modx-announce";
$c['rss_url_security'] = "http://feeds.feedburner.com/modxsecurity";
$c['validate_referer'] = "1";
$c['datepicker_offset'] = "-10";
$c['xhtml_urls'] = "1";
$c['allow_duplicate_alias'] = "1";
$c['automatic_alias'] = "1";
$c['datetime_format'] = "dd-mm-YYYY";
$c['warning_visibility'] = "1";
$c['remember_last_tab'] = "0";
$c['enable_bindings'] = "1";
$c['seostrict'] = "0";
$c['cache_type'] = "1";
$c['maxImageWidth'] = "1600";
$c['maxImageHeight'] = "1200";
$c['thumbWidth'] = "150";
$c['thumbHeight'] = "150";
$c['thumbsDir'] = ".thumbs";
$c['jpegQuality'] = "90";
$c['denyZipDownload'] = "0";
$c['denyExtensionRename'] = "0";
$c['showHiddenFiles'] = "0";
$c['docid_incrmnt_method'] = "0";
$c['make_folders'] = "1";
$c['site_id'] = "52775031dd879";
$c['site_unavailable_page'] = "75";
$c['reload_site_unavailable'] = "";
$c['siteunavailable_message_default'] = "В настоящее время сайт недоступен.";
$c['check_files_onlogin'] = "index.php\r\n.htaccess\r\nmanager/index.php\r\nmanager/includes/config.inc.php";
$c['error_reporting'] = "1";
$c['send_errormail'] = "3";
$c['pwd_hash_algo'] = "UNCRYPT";
$c['reload_captcha_words'] = "";
$c['captcha_words_default'] = "MODX,Access,Better,BitCode,Chunk,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Oscope,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Tattoo,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote";
$c['reload_emailsubject'] = "";
$c['emailsubject_default'] = "Данные для авторизации";
$c['reload_signupemail_message'] = "";
$c['signupemail_message'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['system_email_signup_default'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации в системе управления сайтом [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации в системе управления сайтом ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['reload_websignupemail_message'] = "";
$c['websignupemail_message'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['system_email_websignup_default'] = "Здравствуйте, [+uid+]!\r\n\r\nВаши данные для авторизации на [+sname+]:\r\n\r\nИмя пользователя: [+uid+]\r\nПароль: [+pwd+]\r\n\r\nПосле успешной авторизации на [+sname+] ([+surl+]), вы сможете изменить свой пароль.\r\n\r\nС уважением, Администрация";
$c['reload_system_email_webreminder_message'] = "";
$c['webpwdreminder_message'] = "Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";
$c['system_email_webreminder_default'] = "Здравствуйте, [+uid+]!\r\n\r\nЧтобы активировать ваш новый пароль, перейдите по следующей ссылке:\r\n\r\n[+surl+]\r\n\r\nПозже вы сможете использовать следующий пароль для авторизации: [+pwd+]\r\n\r\nЕсли это письмо пришло к вам по ошибке, пожалуйста, проигнорируйте его.\r\n\r\nС уважением, Администрация";
$c['tree_page_click'] = "27";
$c['resource_tree_node_name'] = "menutitle";
$c['rb_webuser'] = "0";
$c['clean_uploaded_filename'] = "1";
$c['mce_editor_skin'] = "default";
$c['mce_template_docs'] = "";
$c['mce_template_chunks'] = "";
$c['mce_entermode'] = "p";
$c['mce_element_format'] = "xhtml";
$c['mce_schema'] = "html5";
$c['tinymce_custom_buttons3'] = "table,|,row_props,cell_props,|,row_before,row_after,delete_row,|,col_before,col_after,delete_col,|,split_cells,merge_cells,|,fullscreen,help";
$c['tinymce_custom_buttons4'] = "";
$c['tinymce_css_selectors'] = "left=justifyleft;right=justifyright;Лайтбокс=fancy";
$c['sys_files_checksum'] = "a:4:{s:50:\"/home/web/www/clients/client12/web38/web/index.php\";s:32:\"cd008df3faa7d7ecbce5b05480fa8c34\";s:50:\"/home/web/www/clients/client12/web38/web/.htaccess\";s:32:\"e9d0797d8c9823fe59e92844f7d7c8d4\";s:58:\"/home/web/www/clients/client12/web38/web/manager/index.php\";s:32:\"ef228d1a7e707d376ab4e2ec314c7f6c\";s:72:\"/home/web/www/clients/client12/web38/web/manager/includes/config.inc.php\";s:32:\"e41b79cec47dea20709f7cc9c169eb9d\";}";
$this->aliasListing = array();
$a = &$this->aliasListing;
$d = &$this->documentListing;
$m = &$this->documentMap;
$d['index'] = 1;
$a[1] = array('id' => 1, 'alias' => 'index', 'path' => '', 'parent' => 0, 'isfolder' => 0);
$m[] = array('0' => '1');
$d['o-nas'] = 2;
$a[2] = array('id' => 2, 'alias' => 'o-nas', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '2');
$d['uslugi'] = 3;
$a[3] = array('id' => 3, 'alias' => 'uslugi', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '3');
$d['klientyi'] = 4;
$a[4] = array('id' => 4, 'alias' => 'klientyi', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '4');
$d['blog'] = 5;
$a[5] = array('id' => 5, 'alias' => 'blog', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '5');
$d['contacts'] = 6;
$a[6] = array('id' => 6, 'alias' => 'contacts', 'path' => '', 'parent' => 0, 'isfolder' => 0);
$m[] = array('0' => '6');
$d['dopolnitelnye-elementy'] = 62;
$a[62] = array('id' => 62, 'alias' => 'dopolnitelnye-elementy', 'path' => '', 'parent' => 0, 'isfolder' => 1);
$m[] = array('0' => '62');
$d['o-nas/stuff'] = 11;
$a[11] = array('id' => 11, 'alias' => 'stuff', 'path' => 'o-nas', 'parent' => 2, 'isfolder' => 1);
$m[] = array('2' => '11');
$d['o-nas/novosti-i-akczii'] = 58;
$a[58] = array('id' => 58, 'alias' => 'novosti-i-akczii', 'path' => 'o-nas', 'parent' => 2, 'isfolder' => 1);
$m[] = array('2' => '58');
$d['o-nas/sertifikaty'] = 78;
$a[78] = array('id' => 78, 'alias' => 'sertifikaty', 'path' => 'o-nas', 'parent' => 2, 'isfolder' => 0);
$m[] = array('2' => '78');
$d['o-nas/otzyvy'] = 143;
$a[143] = array('id' => 143, 'alias' => 'otzyvy', 'path' => 'o-nas', 'parent' => 2, 'isfolder' => 1);
$m[] = array('2' => '143');
$d['uslugi/seo-prodvizhenie'] = 140;
$a[140] = array('id' => 140, 'alias' => 'seo-prodvizhenie', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '140');
$d['uslugi/kontekstnaya-reklama'] = 9;
$a[9] = array('id' => 9, 'alias' => 'kontekstnaya-reklama', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '9');
$d['uslugi/audit-sajta'] = 8;
$a[8] = array('id' => 8, 'alias' => 'audit-sajta', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '8');
$d['uslugi/kopirajting'] = 10;
$a[10] = array('id' => 10, 'alias' => 'kopirajting', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '10');
$d['uslugi/dopolnitelnye-uslugi'] = 90;
$a[90] = array('id' => 90, 'alias' => 'dopolnitelnye-uslugi', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '90');
$d['uslugi/seo-prodvizhenie-old'] = 7;
$a[7] = array('id' => 7, 'alias' => 'seo-prodvizhenie-old', 'path' => 'uslugi', 'parent' => 3, 'isfolder' => 1);
$m[] = array('3' => '7');
$d['klientyi/korporativnye'] = 43;
$a[43] = array('id' => 43, 'alias' => 'korporativnye', 'path' => 'klientyi', 'parent' => 4, 'isfolder' => 1);
$m[] = array('4' => '43');
$d['klientyi/vneshnie'] = 44;
$a[44] = array('id' => 44, 'alias' => 'vneshnie', 'path' => 'klientyi', 'parent' => 4, 'isfolder' => 1);
$m[] = array('4' => '44');
$d['blog/seo'] = 25;
$a[25] = array('id' => 25, 'alias' => 'seo', 'path' => 'blog', 'parent' => 5, 'isfolder' => 1);
$m[] = array('5' => '25');
$d['blog/raznoe'] = 23;
$a[23] = array('id' => 23, 'alias' => 'raznoe', 'path' => 'blog', 'parent' => 5, 'isfolder' => 1);
$m[] = array('5' => '23');
$d['blog/reklama'] = 92;
$a[92] = array('id' => 92, 'alias' => 'reklama', 'path' => 'blog', 'parent' => 5, 'isfolder' => 1);
$m[] = array('5' => '92');
$d['blog/soc-seti'] = 128;
$a[128] = array('id' => 128, 'alias' => 'soc-seti', 'path' => 'blog', 'parent' => 5, 'isfolder' => 1);
$m[] = array('5' => '128');
$d['uslugi/seo-prodvizhenie-old/prodvizhenie-po-trafiku'] = 28;
$a[28] = array('id' => 28, 'alias' => 'prodvizhenie-po-trafiku', 'path' => 'uslugi/seo-prodvizhenie-old', 'parent' => 7, 'isfolder' => 0);
$m[] = array('7' => '28');
$d['uslugi/seo-prodvizhenie-old/prodvizhenie-po-klikam'] = 29;
$a[29] = array('id' => 29, 'alias' => 'prodvizhenie-po-klikam', 'path' => 'uslugi/seo-prodvizhenie-old', 'parent' => 7, 'isfolder' => 0);
$m[] = array('7' => '29');
$d['uslugi/seo-prodvizhenie-old/prodvizhenie-po-lidam'] = 30;
$a[30] = array('id' => 30, 'alias' => 'prodvizhenie-po-lidam', 'path' => 'uslugi/seo-prodvizhenie-old', 'parent' => 7, 'isfolder' => 0);
$m[] = array('7' => '30');
$d['uslugi/seo-prodvizhenie-old/prodvizhenie-molodogo-sajta'] = 31;
$a[31] = array('id' => 31, 'alias' => 'prodvizhenie-molodogo-sajta', 'path' => 'uslugi/seo-prodvizhenie-old', 'parent' => 7, 'isfolder' => 0);
$m[] = array('7' => '31');
$d['uslugi/audit-sajta/poiskovyij-audit-sajta'] = 40;
$a[40] = array('id' => 40, 'alias' => 'poiskovyij-audit-sajta', 'path' => 'uslugi/audit-sajta', 'parent' => 8, 'isfolder' => 0);
$m[] = array('8' => '40');
$d['uslugi/audit-sajta/marketingovyij-audit'] = 39;
$a[39] = array('id' => 39, 'alias' => 'marketingovyij-audit', 'path' => 'uslugi/audit-sajta', 'parent' => 8, 'isfolder' => 0);
$m[] = array('8' => '39');
$d['uslugi/kontekstnaya-reklama/stoimost-reklamy-kontekst'] = 79;
$a[79] = array('id' => 79, 'alias' => 'stoimost-reklamy-kontekst', 'path' => 'uslugi/kontekstnaya-reklama', 'parent' => 9, 'isfolder' => 0);
$m[] = array('9' => '79');
$d['uslugi/kontekstnaya-reklama/audit-reklamnoj-kampanii'] = 41;
$a[41] = array('id' => 41, 'alias' => 'audit-reklamnoj-kampanii', 'path' => 'uslugi/kontekstnaya-reklama', 'parent' => 9, 'isfolder' => 0);
$m[] = array('9' => '41');
$d['uslugi/kopirajting/seo-tekstyi'] = 32;
$a[32] = array('id' => 32, 'alias' => 'seo-tekstyi', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '32');
$d['uslugi/kopirajting/reklamnyie-tekstyi'] = 33;
$a[33] = array('id' => 33, 'alias' => 'reklamnyie-tekstyi', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '33');
$d['uslugi/kopirajting/press-relizyi'] = 34;
$a[34] = array('id' => 34, 'alias' => 'press-relizyi', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '34');
$d['uslugi/kopirajting/informaczionnyie-stati'] = 35;
$a[35] = array('id' => 35, 'alias' => 'informaczionnyie-stati', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '35');
$d['uslugi/kopirajting/pozitivnyie-otzyivyi-o-brende'] = 36;
$a[36] = array('id' => 36, 'alias' => 'pozitivnyie-otzyivyi-o-brende', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '36');
$d['uslugi/kopirajting/novostnyie-stati'] = 37;
$a[37] = array('id' => 37, 'alias' => 'novostnyie-stati', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '37');
$d['uslugi/kopirajting/e-mail-rassyilka'] = 38;
$a[38] = array('id' => 38, 'alias' => 'e-mail-rassyilka', 'path' => 'uslugi/kopirajting', 'parent' => 10, 'isfolder' => 0);
$m[] = array('10' => '38');
$d['o-nas/stuff/anna-skinderskaya'] = 77;
$a[77] = array('id' => 77, 'alias' => 'anna-skinderskaya', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '77');
$d['o-nas/stuff/nikita-scherba'] = 17;
$a[17] = array('id' => 17, 'alias' => 'nikita-scherba', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '17');
$d['o-nas/stuff/elena-maslova'] = 15;
$a[15] = array('id' => 15, 'alias' => 'elena-maslova', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '15');
$d['o-nas/stuff/nikita-dmitrievich'] = 13;
$a[13] = array('id' => 13, 'alias' => 'nikita-dmitrievich', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '13');
$d['o-nas/stuff/irina-korenevskaya'] = 16;
$a[16] = array('id' => 16, 'alias' => 'irina-korenevskaya', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '16');
$d['o-nas/stuff/seosquirrel'] = 12;
$a[12] = array('id' => 12, 'alias' => 'seosquirrel', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '12');
$d['o-nas/stuff/yuliya-belaya'] = 76;
$a[76] = array('id' => 76, 'alias' => 'yuliya-belaya', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '76');
$d['o-nas/stuff/mariya-romanova'] = 18;
$a[18] = array('id' => 18, 'alias' => 'mariya-romanova', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '18');
$d['o-nas/stuff/vladislav'] = 134;
$a[134] = array('id' => 134, 'alias' => 'vladislav', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '134');
$d['o-nas/stuff/yuliya-karpovich'] = 19;
$a[19] = array('id' => 19, 'alias' => 'yuliya-karpovich', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '19');
$d['o-nas/stuff/natalya-sheremet'] = 107;
$a[107] = array('id' => 107, 'alias' => 'natalya-sheremet', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '107');
$d['o-nas/stuff/andrej-matrashilo'] = 106;
$a[106] = array('id' => 106, 'alias' => 'andrej-matrashilo', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '106');
$d['o-nas/stuff/dmitrij-kutuzov'] = 105;
$a[105] = array('id' => 105, 'alias' => 'dmitrij-kutuzov', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '105');
$d['o-nas/stuff/nikolaj-makerov'] = 104;
$a[104] = array('id' => 104, 'alias' => 'nikolaj-makerov', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '104');
$d['o-nas/stuff/mariya-bosak'] = 103;
$a[103] = array('id' => 103, 'alias' => 'mariya-bosak', 'path' => 'o-nas/stuff', 'parent' => 11, 'isfolder' => 0);
$m[] = array('11' => '103');
$d['blog/seo/yandeks-obyavil-vojnu-nekachestvennoj-reklame'] = 111;
$a[111] = array('id' => 111, 'alias' => 'yandeks-obyavil-vojnu-nekachestvennoj-reklame', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '111');
$d['blog/seo/google-nachal-otobrazhat-eskizy-video-na-pervom-meste-v-serp'] = 112;
$a[112] = array('id' => 112, 'alias' => 'google-nachal-otobrazhat-eskizy-video-na-pervom-meste-v-serp', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '112');
$d['blog/seo/google-uzhestochaet-borbu-s-sajtami-kotorye-polzuyutsya-uslugami-ssylochnyh-birzh'] = 113;
$a[113] = array('id' => 113, 'alias' => 'google-uzhestochaet-borbu-s-sajtami-kotorye-polzuyutsya-uslugami-ssylochnyh-birzh', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '113');
$d['blog/seo/novyj-instrument-diagnostiki-stranic-ot-yandeks'] = 114;
$a[114] = array('id' => 114, 'alias' => 'novyj-instrument-diagnostiki-stranic-ot-yandeks', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '114');
$d['blog/seo/google-scraper-report-protiv-kopirovaniya-kontenta-konkurentami'] = 133;
$a[133] = array('id' => 133, 'alias' => 'google-scraper-report-protiv-kopirovaniya-kontenta-konkurentami', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '133');
$d['blog/seo/google-novyi-serp-interface'] = 135;
$a[135] = array('id' => 135, 'alias' => 'google-novyi-serp-interface', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '135');
$d['blog/seo/prodvizhenie-saitov-po-kluchevym-slovam-info'] = 139;
$a[139] = array('id' => 139, 'alias' => 'prodvizhenie-saitov-po-kluchevym-slovam-info', 'path' => 'blog/seo', 'parent' => 25, 'isfolder' => 0);
$m[] = array('25' => '139');
$d['klientyi/jaguar'] = 50;
$a[50] = array('id' => 50, 'alias' => 'jaguar', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '50');
$d['klientyi/volkswagen'] = 55;
$a[55] = array('id' => 55, 'alias' => 'volkswagen', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '55');
$d['klientyi/cadillac'] = 45;
$a[45] = array('id' => 45, 'alias' => 'cadillac', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '45');
$d['klientyi/chevrolet'] = 46;
$a[46] = array('id' => 46, 'alias' => 'chevrolet', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '46');
$d['klientyi/lada'] = 42;
$a[42] = array('id' => 42, 'alias' => 'lada', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '42');
$d['klientyi/citroen'] = 47;
$a[47] = array('id' => 47, 'alias' => 'citroen', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '47');
$d['klientyi/honda'] = 48;
$a[48] = array('id' => 48, 'alias' => 'honda', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '48');
$d['klientyi/hyundai'] = 49;
$a[49] = array('id' => 49, 'alias' => 'hyundai', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '49');
$d['klientyi/land-rover'] = 51;
$a[51] = array('id' => 51, 'alias' => 'land-rover', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '51');
$d['klientyi/opel'] = 52;
$a[52] = array('id' => 52, 'alias' => 'opel', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '52');
$d['klientyi/peugeot'] = 53;
$a[53] = array('id' => 53, 'alias' => 'peugeot', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '53');
$d['klientyi/skoda'] = 54;
$a[54] = array('id' => 54, 'alias' => 'skoda', 'path' => 'klientyi', 'parent' => 43, 'isfolder' => 0);
$m[] = array('43' => '54');
$d['klientyi/vneshnie/keramika-opttorg'] = 115;
$a[115] = array('id' => 115, 'alias' => 'keramika-opttorg', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '115');
$d['klientyi/vneshnie/krasnodarneftepererabotka'] = 123;
$a[123] = array('id' => 123, 'alias' => 'krasnodarneftepererabotka', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '123');
$d['klientyi/vneshnie/slavyanskij-kirpich'] = 117;
$a[117] = array('id' => 117, 'alias' => 'slavyanskij-kirpich', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '117');
$d['klientyi/vneshnie/baza-otdyha-viktoriya'] = 116;
$a[116] = array('id' => 116, 'alias' => 'baza-otdyha-viktoriya', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '116');
$d['klientyi/vneshnie/alfa-polimer'] = 118;
$a[118] = array('id' => 118, 'alias' => 'alfa-polimer', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '118');
$d['klientyi/vneshnie/kompaniya-eko-point'] = 119;
$a[119] = array('id' => 119, 'alias' => 'kompaniya-eko-point', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '119');
$d['klientyi/vneshnie/kliniki-vale-dental'] = 120;
$a[120] = array('id' => 120, 'alias' => 'kliniki-vale-dental', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '120');
$d['klientyi/vneshnie/kubanskij-ekologicheskij-centr'] = 121;
$a[121] = array('id' => 121, 'alias' => 'kubanskij-ekologicheskij-centr', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '121');
$d['klientyi/vneshnie/fabrika-gram'] = 122;
$a[122] = array('id' => 122, 'alias' => 'fabrika-gram', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '122');
$d['klientyi/vneshnie/florist-yuliya-kravcova'] = 124;
$a[124] = array('id' => 124, 'alias' => 'florist-yuliya-kravcova', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '124');
$d['klientyi/vneshnie/specmash-m'] = 125;
$a[125] = array('id' => 125, 'alias' => 'specmash-m', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '125');
$d['klientyi/vneshnie/akvapark-zolotaya-buhta'] = 126;
$a[126] = array('id' => 126, 'alias' => 'akvapark-zolotaya-buhta', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '126');
$d['klientyi/vneshnie/pansionat-tizdar'] = 127;
$a[127] = array('id' => 127, 'alias' => 'pansionat-tizdar', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '127');
$d['klientyi/vneshnie/ooo-optovaya-torgovaya-kompaniya'] = 141;
$a[141] = array('id' => 141, 'alias' => 'ooo-optovaya-torgovaya-kompaniya', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '141');
$d['klientyi/vneshnie/kadrovoe-agentstvo-bystryj-poisk-personala'] = 142;
$a[142] = array('id' => 142, 'alias' => 'kadrovoe-agentstvo-bystryj-poisk-personala', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '142');
$d['klientyi/vneshnie/akademiya-strategicheskogo-upravleniya'] = 146;
$a[146] = array('id' => 146, 'alias' => 'akademiya-strategicheskogo-upravleniya', 'path' => 'klientyi/vneshnie', 'parent' => 44, 'isfolder' => 0);
$m[] = array('44' => '146');
$d['order-consultation'] = 57;
$a[57] = array('id' => 57, 'alias' => 'order-consultation', 'path' => '', 'parent' => 56, 'isfolder' => 0);
$m[] = array('56' => '57');
$d['zakazat-audit'] = 60;
$a[60] = array('id' => 60, 'alias' => 'zakazat-audit', 'path' => '', 'parent' => 56, 'isfolder' => 0);
$m[] = array('56' => '60');
$d['zakazat-seo'] = 61;
$a[61] = array('id' => 61, 'alias' => 'zakazat-seo', 'path' => '', 'parent' => 56, 'isfolder' => 0);
$m[] = array('56' => '61');
$d['formy-uslugi'] = 85;
$a[85] = array('id' => 85, 'alias' => 'formy-uslugi', 'path' => '', 'parent' => 56, 'isfolder' => 1);
$m[] = array('56' => '85');
$d['call-order'] = 91;
$a[91] = array('id' => 91, 'alias' => 'call-order', 'path' => '', 'parent' => 56, 'isfolder' => 0);
$m[] = array('56' => '91');
$d['o-nas/novosti-i-akczii/v-novyj-god-s-novym-sajtom'] = 80;
$a[80] = array('id' => 80, 'alias' => 'v-novyj-god-s-novym-sajtom', 'path' => 'o-nas/novosti-i-akczii', 'parent' => 58, 'isfolder' => 0);
$m[] = array('58' => '80');
$d['o-nas/novosti-i-akczii/pozdravlenie-s-23-fevralya'] = 130;
$a[130] = array('id' => 130, 'alias' => 'pozdravlenie-s-23-fevralya', 'path' => 'o-nas/novosti-i-akczii', 'parent' => 58, 'isfolder' => 0);
$m[] = array('58' => '130');
$d['o-nas/novosti-i-akczii/pozdravlenie-8-marta'] = 136;
$a[136] = array('id' => 136, 'alias' => 'pozdravlenie-8-marta', 'path' => 'o-nas/novosti-i-akczii', 'parent' => 58, 'isfolder' => 0);
$m[] = array('58' => '136');
$d['o-nas/novosti-i-akczii/ves-mart-2014-top3seo-darit-podarki'] = 137;
$a[137] = array('id' => 137, 'alias' => 'ves-mart-2014-top3seo-darit-podarki', 'path' => 'o-nas/novosti-i-akczii', 'parent' => 58, 'isfolder' => 0);
$m[] = array('58' => '137');
$d['medals'] = 63;
$a[63] = array('id' => 63, 'alias' => 'medals', 'path' => '', 'parent' => 62, 'isfolder' => 1);
$m[] = array('62' => '63');
$d['sistemnye-stranicy'] = 72;
$a[72] = array('id' => 72, 'alias' => 'sistemnye-stranicy', 'path' => '', 'parent' => 62, 'isfolder' => 1);
$m[] = array('62' => '72');
$d['formyi'] = 56;
$a[56] = array('id' => 56, 'alias' => 'formyi', 'path' => '', 'parent' => 62, 'isfolder' => 1);
$m[] = array('62' => '56');
$d['medals/direkt'] = 64;
$a[64] = array('id' => 64, 'alias' => 'direkt', 'path' => 'medals', 'parent' => 63, 'isfolder' => 0);
$m[] = array('63' => '64');
$d['medals/metrika'] = 65;
$a[65] = array('id' => 65, 'alias' => 'metrika', 'path' => 'medals', 'parent' => 63, 'isfolder' => 0);
$m[] = array('63' => '65');
$d['medals/advords'] = 66;
$a[66] = array('id' => 66, 'alias' => 'advords', 'path' => 'medals', 'parent' => 63, 'isfolder' => 0);
$m[] = array('63' => '66');
$d['medals/analitika'] = 67;
$a[67] = array('id' => 67, 'alias' => 'analitika', 'path' => 'medals', 'parent' => 63, 'isfolder' => 0);
$m[] = array('63' => '67');
$d['404'] = 73;
$a[73] = array('id' => 73, 'alias' => '404', 'path' => '', 'parent' => 72, 'isfolder' => 0);
$m[] = array('72' => '73');
$d['403'] = 74;
$a[74] = array('id' => 74, 'alias' => '403', 'path' => '', 'parent' => 72, 'isfolder' => 0);
$m[] = array('72' => '74');
$d['sajt-nedostupen'] = 75;
$a[75] = array('id' => 75, 'alias' => 'sajt-nedostupen', 'path' => '', 'parent' => 72, 'isfolder' => 0);
$m[] = array('72' => '75');
$d['sitemap.xml'] = 83;
$a[83] = array('id' => 83, 'alias' => 'sitemap.xml', 'path' => '', 'parent' => 72, 'isfolder' => 0);
$m[] = array('72' => '83');
$d['test'] = 109;
$a[109] = array('id' => 109, 'alias' => 'test', 'path' => '', 'parent' => 72, 'isfolder' => 0);
$m[] = array('72' => '109');
$d['order-seo'] = 86;
$a[86] = array('id' => 86, 'alias' => 'order-seo', 'path' => '', 'parent' => 85, 'isfolder' => 0);
$m[] = array('85' => '86');
$d['order-audit'] = 87;
$a[87] = array('id' => 87, 'alias' => 'order-audit', 'path' => '', 'parent' => 85, 'isfolder' => 0);
$m[] = array('85' => '87');
$d['order-kontekst'] = 88;
$a[88] = array('id' => 88, 'alias' => 'order-kontekst', 'path' => '', 'parent' => 85, 'isfolder' => 0);
$m[] = array('85' => '88');
$d['kontekst-audit-rk'] = 108;
$a[108] = array('id' => 108, 'alias' => 'kontekst-audit-rk', 'path' => '', 'parent' => 85, 'isfolder' => 0);
$m[] = array('85' => '108');
$d['order-text'] = 89;
$a[89] = array('id' => 89, 'alias' => 'order-text', 'path' => '', 'parent' => 85, 'isfolder' => 0);
$m[] = array('85' => '89');
$d['uslugi/dopolnitelnye-uslugi/rassyilka-press-relizov'] = 59;
$a[59] = array('id' => 59, 'alias' => 'rassyilka-press-relizov', 'path' => 'uslugi/dopolnitelnye-uslugi', 'parent' => 90, 'isfolder' => 0);
$m[] = array('90' => '59');
$d['blog/reklama/google-protiv-sklikivaniy-reklamy'] = 131;
$a[131] = array('id' => 131, 'alias' => 'google-protiv-sklikivaniy-reklamy', 'path' => 'blog/reklama', 'parent' => 92, 'isfolder' => 0);
$m[] = array('92' => '131');
$d['blog/reklama/torgovye-kampanii-api-google-adwords'] = 138;
$a[138] = array('id' => 138, 'alias' => 'torgovye-kampanii-api-google-adwords', 'path' => 'blog/reklama', 'parent' => 92, 'isfolder' => 0);
$m[] = array('92' => '138');
$d['blog/soc-seti/facebook-budet-optimizirovat-instrumenty-socialnogo-poiska-dlya-mobilnyh-ustrojstv'] = 129;
$a[129] = array('id' => 129, 'alias' => 'facebook-budet-optimizirovat-instrumenty-socialnogo-poiska-dlya-mobilnyh-ustrojstv', 'path' => 'blog/soc-seti', 'parent' => 128, 'isfolder' => 0);
$m[] = array('128' => '129');
$d['blog/soc-seti/neozhidannaya-volna-popularnosti-telegram'] = 132;
$a[132] = array('id' => 132, 'alias' => 'neozhidannaya-volna-popularnosti-telegram', 'path' => 'blog/soc-seti', 'parent' => 128, 'isfolder' => 0);
$m[] = array('128' => '132');
$d['uslugi/seo-prodvizhenie/prodvizhenie-po-klyuchevym-slovam'] = 27;
$a[27] = array('id' => 27, 'alias' => 'prodvizhenie-po-klyuchevym-slovam', 'path' => 'uslugi/seo-prodvizhenie', 'parent' => 140, 'isfolder' => 0);
$m[] = array('140' => '27');
$d['o-nas/otzyvy/novyj-resurs'] = 144;
$a[144] = array('id' => 144, 'alias' => 'novyj-resurs', 'path' => 'o-nas/otzyvy', 'parent' => 143, 'isfolder' => 0);
$m[] = array('143' => '144');
$d['o-nas/otzyvy/knp'] = 145;
$a[145] = array('id' => 145, 'alias' => 'knp', 'path' => 'o-nas/otzyvy', 'parent' => 143, 'isfolder' => 0);
$m[] = array('143' => '145');
$d['o-nas/otzyvy/akademiya'] = 147;
$a[147] = array('id' => 147, 'alias' => 'akademiya', 'path' => 'o-nas/otzyvy', 'parent' => 143, 'isfolder' => 0);
$m[] = array('143' => '147');
$c = &$this->contentTypes;
$c[83] = 'text/xml';
$c = &$this->chunkCache;
$c['mm_rules'] = '// more example rules are in assets/plugins/managermanager/example_mm_rules.inc.php

// example of how PHP is allowed - check that a TV named documentTags exists before creating rule
if($modx->db->getValue("SELECT COUNT(id) FROM " . $modx->getFullTableName(\'site_tmplvars\') . " WHERE name=\'documentTags\'")) {
    mm_widget_tags(\'documentTags\',\' \'); // Give blog tag editing capabilities to the \'documentTags (3)\' TV
}
mm_widget_showimagetvs(); // Always give a preview of Image TVs
';
$c['WebLoginSideBar'] = '<!-- #declare:separator <hr> -->
<!-- login form section-->
<form method="post" name="loginfrm" action="[+action+]">
    <input type="hidden" value="[+rememberme+]" name="rememberme" />
    <fieldset>
        <h3>Your Login Details</h3>
        <label for="username">User: <input type="text" name="username" id="username" tabindex="1" onkeypress="return webLoginEnter(document.loginfrm.password);" value="[+username+]" /></label>
    	<label for="password">Password: <input type="password" name="password" id="password" tabindex="2" onkeypress="return webLoginEnter(document.loginfrm.cmdweblogin);" value="" /></label>
    	<input type="checkbox" id="checkbox_1" name="checkbox_1" tabindex="3" size="1" value="" [+checkbox+] onclick="webLoginCheckRemember()" /><label for="checkbox_1" class="checkbox">Remember me</label>
    	<input type="submit" value="[+logintext+]" name="cmdweblogin" class="button" />
	<a href="#" onclick="webLoginShowForm(2);return false;" id="forgotpsswd">Forget Your Password?</a>
	</fieldset>
</form>
<hr>
<!-- log out hyperlink section -->
<h4>You\'re already logged in</h4>
Do you wish to <a href="[+action+]" class="button">[+logouttext+]</a>?
<hr>
<!-- Password reminder form section -->
<form name="loginreminder" method="post" action="[+action+]">
    <fieldset>
        <h3>It happens to everyone...</h3>
        <input type="hidden" name="txtpwdrem" value="0" />
        <label for="txtwebemail">Enter the email address of your account to reset your password: <input type="text" name="txtwebemail" id="txtwebemail" size="24" /></label>
        <label>To return to the login form, press the cancel button.</label>
    	<input type="submit" value="Submit" name="cmdweblogin" class="button" /> <input type="reset" value="Cancel" name="cmdcancel" onclick="webLoginShowForm(1);" class="button" style="clear:none;display:inline" />
    </fieldset>
</form>

';
$c['head'] = '        <base href="[(site_url)]" />
		<meta http-equiv="Content-Type" content="text/html; charset=[(modx_charset)]">


  		<title>[+phx:if=`[*longtitle*]`:is=``:then=`[*pagetitle*]`:else=`[*longtitle*]`+]</title>
		<meta name="description" content="[*description*]"/>

		<link rel="SHORTCUT ICON" type="image/x-icon" href="/favicon.ico"> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon/apple-touch-icon-144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon/apple-touch-icon-114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon/apple-touch-icon-72.png" />
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon/apple-touch-icon-precomposed.png" />

		<link href="/css/style.css" rel="stylesheet" type="text/css">

		<link href="/css/skin.css" rel="stylesheet" type="text/css">
		<link href="/css/fancybox.css" rel="stylesheet" type="text/css"  media="screen" />
		<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&amp;subset=latin,cyrillic-ext\' rel=\'stylesheet\' type=\'text/css\'>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<!--[if IE]>
        <link href="/css/ie.css" rel="stylesheet" type="text/css">
		<![endif]-->
		<script>
			(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
			ga(\'create\', \'UA-48171662-1\', \'top3seo.ru\');
			ga(\'send\', \'pageview\');

		</script>
		<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter24035908 = new Ya.Metrika({id:24035908,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>

		<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
		<script type="text/javascript" src="/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="/js/smooth.pack.js"></script>
		<script type="text/javascript" src="/js/html5placeholder.js"></script> 

		<script type="text/javascript" src="/js/all_scripts.js"></script>
<script>$(function(){$(\'input[placeholder], textarea[placeholder]\').placeholder();});</script>
';
$c['header'] = '			<div class="logo">
				<a href="http://www.top3seo.ru/">
					<img src="/images/logo.png" alt=""/>
				</a>
			</div>
			
			<div class="top_menu">
					[[Wayfinder? &rowTpl=`top_menu_li` &startId=`0` &outerClass=`nav` &excludeDocs=`` &level=`1`]]
			</div>
<div class="clear"></div>
';
$c['stuffs_tpl'] = '[+phx:if=`[+foto_stuff+]`:is=``:then=``:else=`
<li>
	[+foto_stuff+]<br>
	<span class="name">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</span>
	<span class="post">[+post_stuff+]</span>
</li>
`+]';
$c['uslugi_tpl'] = '<li[+wf.id+][+wf.classes+]>
	<a href="[+wf.link+]" title="[+wf.title+]" class="[+wf.attributes+]">
		<img class="un_hov" src="/[+usluga_img_std+]" alt="[+pagetitle+]">
		<img class="hov" src="/[+usluga_img_hov+]" alt="[+pagetitle+]">
		<span>[+wf.linktext+]</span>
	</a>[+wf.wrapper+]
</li>';
$c['top_line'] = '<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N3XKVC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-N3XKVC\');</script>
<!-- End Google Tag Manager -->


			<div class="top_line"> <!-- верхняя полоска -->
				<div class="tel"><a class="big" href="tel:88005001732">8 (800) 500-17-90</a> <span class="small">Звонок бесплатный!</span>
				</div>
				<div class="marketing">
					<span class="how_seo">
						<a href="[~57~]" class="open_iframe" data-fancybox-type="iframe">Узнайте, можно ли продвинуть ваш сайт</a></span>
					
					
				</div>
			</div>
<div class="clearline"></div>';
$c['counters'] = '<div class="counters">
<noscript><div><img src="//mc.yandex.ru/watch/24035908" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100713874); }catch(e){}</script>

<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href=\'http://www.liveinternet.ru/click\' "+
"target=_blank><img src=\'//counter.yadro.ru/hit?t23.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"\' alt=\'\' title=\'LiveInternet: показано число посетителей за"+
" сегодня\' "+
"border=\'0\' width=\'88\' height=\'15\'><\\/a>")
//--></script><!--/LiveInternet-->
</div>';
$c['top_menu_li'] = '[+phx:if=`[*id*]`:is=`[[UltimateParent? &id=`[+wf.docid+]`]]`:then=`<li[+wf.id+][+wf.classes+]><span [+wf.attributes+]>[+wf.linktext+]</span>[+wf.wrapper+]</li>`:else=`<li[+wf.id+][+wf.classes+]><a href="[+wf.link+]" title="[+wf.title+]" class="[+wf.attributes+]">[+wf.linktext+]</a>[+wf.wrapper+]</li>`+]';
$c['reportServicesKontekstAudit'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['ServicesKontekstFormAudit'] = '<p class="title">Заказать <span>аудит рекламной компании</span></p>
<form id="ServicesKontekstForm" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="ServicesKontekstForm" />
			<input type="hidden" name="service" value="[!servicevar!]" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<input type="text" id="lastname" name="lastname" >
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
	
	<p class="row">
			<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;" >
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
			<input type="text" style="display:none;" id="region" value="" name="region" ></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" placeholder="Ваши пожелания..."></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['uslugi'] = '[[Wayfinder? &startId=`3` &limit=`4` &outerClass=`service` &rowTpl=`uslugi_tpl` &level=`1`]]			

			<div class="clearline"></div>';
$c['minfo'] = '<div id="minfo" class="block"> <!-- первый блок -->
			<h2>Хотите, чтобы Ваш сайт приносил больше прибыли?</h2>
			<div class="cont">
				
			<p>
			<span>Значит вы попали по адресу</span><br><br>
			Мы как раз решаем такие бизнес-задачи!<br>
			За последний год нашими специалистами было реализовано 54 успешных проекта в области продвижения бизнеса в сети Интернет.</p>
			<ul class="stats_data">
				<li class="count-1"><img src="/images/minfo/count1.png" alt="">Людей<br>пользуются поисковыми системами</li>
				<li class="count-2"><img src="/images/minfo/count2.png" alt="">Посетителей<br>приходит из поисковых систем</li>
				<li class="count-3"><img src="/images/minfo/count3.png" alt="">Увеличение<br>продаж благодаря SEO-оптимизации</li>
			</ul>
			<div class="clear"></div>
			</div>
	[!zakazWrite_iframe!]
	[!eForm? 
	&formid=`orderCall` 
	&tpl=`formCalltOrder` 
	&to=`ndmitrievich@upsales.ru, sales@top3seo.ru, osmolkina@upsales.ru, dfokin@upsales.ru, ailyuchenko@upsales.ru, abelyakov@upsales.ru, amezin@upsales.ru, vjanovskij@upsales.ru`
	&report=`reportOrderCall` 
	&eFormOnMailSent=`zakazWrite`
	&thankyou=`thankOrderCall` 
	&protectSubmit=`name`
	&subject=`Top3Seo: Заказ звонка`
	&eFormOnBeforeMailSent=`BeforeMailSent` 
	&runSnippet=`eFormPHxEvent`
	
!]
</div>
			<div class="clearline"></div>';
$c['formCalltOrder'] = '<form id ="orderCall" action="[~[*id*]~]" method="POST"> <!-- форма заказа звонка -->
			<p class="title">Мы вам сами позвоним</p>
			<input type="hidden" name="formid" value="orderCall">
			<input type="hidden" name="service" value="Звонок">
	<input type="hidden" name="ids" value="1" eform="Название::0::#EVAL return true;" />
	<input type="text" name="name" placeholder="Имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"/><br>
	<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;"/>
	<input type="text" id="customer_phone" name="telephone" placeholder="Телефон *" onclick="if (this.className==\'valid\') this.className=\'\';"/><br>
	<input type="text" name="time_call" placeholder="Время звонка" onclick="if (this.className==\'valid\') this.className=\'\';"/><br>
	<input type="text" style="display:none;" id="region" value="" name="region" />
			 <input type="text" id="lastname" name="lastname" />
	<p class="req">* - Поля, обязательные для заполнения</p>
			 <button id="contact-send" class="ordCall" type="submit">Заказать звонок</button>
			</form>
			
';
$c['hwork'] = '<div id="hwork" class="block">
				<h2>Всё просто!</h2>
				<ul class="sheme">
					<li>
						<img class="un_hov" src="/images/hwork/analiz.png" alt="Анализируем рынок">
						<img class="hov" src="/images/hwork/analiz_hov.png" alt="Анализируем рынок">
						<span class="analiz">Анализируем<br>рынок</span>
					</li>
					<li class="arrow"><img src="/images/hwork/arrow.png" alt="Затем"></li>
					<li>
						<img class="un_hov" src="/images/hwork/strateg.png" alt="Формируем стратегию">
						<img class="hov" src="/images/hwork/strateg_hov.png" alt="Формируем стратегию">
						<span class="strateg">Формируем<br>стратегию</span>
					</li>
					<li class="arrow"><img src="/images/hwork/arrow.png" alt="Затем"></li>
					<li>
						<img class="un_hov" src="/images/hwork/tehnology.png" alt="Внедряем технологии">
						<img class="hov" src="/images/hwork/tehnology_hov.png" alt="Внедряем технологии">
						<span class="tehnology">Внедряем<br>технологии</span>
					</li>
					<li class="arrow"><img src="/images/hwork/arrow.png" alt="Затем"></li>
					<li>
						<img class="un_hov" src="/images/hwork/result.png" alt="Поддерживаем результат">
						<img class="hov" src="/images/hwork/result_hov.png" alt="Поддерживаем результат">
						<span class="result">Поддерживаем<br>результат</span>
					</li>
				</ul>
				<div class="clear"></div>
				<div class="desc">
					<span class="title" >Мы учитываем все «подводные камни» процесса и делаем выгодное предложение:</span>
					<ul>
					<li class="one"><span class="numb">1</span><span class="gar_desc">Даём гарантии.<br>Вы платите только<br>за результат.</span></li>
					<li class="two"><span class="numb">2</span><span class="gar_desc">Используем методы,<br>рекомендованные<br>Яндексом и Google.</span></li>
					<li class="three"><span class="numb">3</span><span class="gar_desc">Проводим<br>бесплатную<br>экспресс-аналитику.</span></li>
						<li class="clear"></li>
					</ul>
					<div class="clear"></div>
			
					
					<!--<a href="[~60~]" class="open_iframe ordConsult " data-fancybox-type="iframe">Экспресс-аудит</a>-->
				</div>
				
			</div>
			<div class="clear"></div>';
$c['clients'] = '			<div id="clients" >
			<div id="wrap" class="block">
			<h2>C кем мы работаем</h2>
				<p>Наши клиенты – это компании, с которыми мы вместе держим курс на успех!</p>
				<p>Мы работаем с автомобильными брендами, медицинскими центрами, строительными и другими успешными компаниями, решившими направить свой потенциал в интернет-маркетинг. Взаимодействие с клиентом и создание комфортных условий сотрудничества помогают нам стать единой командой и добиться высоких результатов.
</p>
			<div class="brands_carousel">
				<ul class="brands" id="brands_carousel">
			[!Ditto? &tpl=`clientTpl` &parents=`4` &depth=`5` &hideFolders=`1` &randomize=`1` &showInMenuOnly=`0`!]
			</ul>
				
				</div>
			<div class="clear"></div>
			<a class="moreClients" href="[~4~]">Посмотреть всех</a>
			</div></div>';
$c['last_article'] = '<div class="last_article">
	<a class="hd_link" href="[~[+id+]~]">
		<img src="[+article_img+]" alt="[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]" />
		<span class="title">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</span>
	</a>
	<span class="intro">
		[[summary? &text=`[+content+]` &len=`120` &noparser=`0`]]</span><a class="read_more" href="[~[+id+]~]">Читать дальше</a>
		<div class="clear"></div>
</div>';
$c['sidebar_contacts'] = '			<div class="right">
				[!zakazWrite_iframe!]
[!eForm? 
	&formid=`feedback` 
	&tpl=`feedbackForm` 
	&to=`sales@top3seo.ru, osmolkina@upsales.ru, dfokin@upsales.ru, ailyuchenko@upsales.ru, abelyakov@upsales.ru, ndmitrievich@upsales.ru, amezin@upsales.ru, vjanovskij@upsales.ru`
	&report=`reportFeedbackForm` 
	&eFormOnMailSent=`zakazWrite`
	&thankyou=`thankFeedbackForm` 
	&protectSubmit=`name` 
	&subject=`Вопрос на сайте Top3Seo` 
	&eFormOnBeforeMailSent=`BeforeMailSent` 
	&runSnippet=`eFormPHxEvent`
	 
!]
			</div>';
$c['news_blog'] = '<div id="newsline" class="block">
	

	<div class="news_entry">
		<a href="[~58~]"><h2>Новости</h2></a>
		[!Ditto? &parents=`58` &display=`1` &tpl=`one_news` &extenders=`summary` &hideFolders=`1`!]
	</div>
	<div class="blog_list">
		<a href="[~5~]"><h2>Блог</h2></a>
		[!Ditto? &parents=`5` &display=`2` &tpl=`last_article` &extenders=`summary` &depth=`5` &hideFolders=`1`!]
	</div>
	<div class="clear"></div>
</div>


</div>';
$c['OrderSeoForm'] = '<p class="title">Заказать продвижение</p>
<form id="orderseo" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="how_seoform" />
			<input type="hidden" name="service" value="[!servicevar!]" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<p class="row">
			<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;" >
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
			<input type="text" style="display:none;" id="region" value="" name="region" ></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" placeholder="Ваши пожелания..." onclick="if (this.className==\'valid\') this.className=\'\';"></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['footer'] = '		<div class="cent">
			<div class="copyright">
				ООО &laquo;Электронная реклама&raquo;<br>
				&copy; 2010-2014 TOP3SEO.ru. All rights reserved
			</div>
			<div class="sicial">
				<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				<a class="email" href="mailto:sales@top3seo.ru">sales@top3seo.ru</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
{{counters}}


<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>
';
$c['sidebar'] = '			<div class="right">

				{{right_menu}}
				
				[+phx:if=`[*id*]`:is=`140`:then=`
				<div class="metrika">
					<div class="descr">
						<span class="big">Результат нашей работы</span>
						<span class="small">За последние 30 дней на сайты наших клиентов перешло:</span>
					</div>
					[!Metrika? &tpl=`metrika_sidebar` &delimiter=`1`!]
					<div class="clear"></div>
				</div>
				`:else=``+]
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[+phx:if=`[[UltimateParent?]]`:is=`2`:or:is=`5`:then=``:else=`[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]`+]
			</div>';
$c['noreplyHowSeo'] = '<p>Здравствуйте [+name+]. Ваша заявка на оценку возможности продвижения сайта [+url+] принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['blog_list'] = '<div class="anons">
	<a class="anons_title" href="[~[+id+]~]">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</a>
	<div class="anons_introtext">
		<img src="[+article_img:replace=`small,medium`+]" alt="[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]">
		<p>[+introtext+]</p>
		<a class="read_more" href="[~[+id+]~]">Читать дальше</a>
		<div class="clear"></div>
	</div>
	<div class="anons_info">
		<span class="pub_date">[[datarus? &MyDate=`[+createdon+]`]]</span>
	
		<a class="comments_count" data-kament-name=\'kament_page_name\' href=\'[(site_url)][~[+id+]~]#kament_comments\'></a>
	</div>
</div>';
$c['comments'] = '<!-- KAMENT -->
<div id="kament_comments"></div>
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = \'top3seo\';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function() {
		var node = document.createElement(\'script\'); node.type = \'text/javascript\'; node.async = true;
		node.src = \'http://\' + kament_subdomain + \'.svkament.ru/js/embed.js\';
		(document.getElementsByTagName(\'head\')[0] || document.getElementsByTagName(\'body\')[0]).appendChild(node);
	})();
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->';
$c['services_list'] = '<div class="block_one_service">
	 
	<a href="[~[+id+]~]">
		<img class="un_hov" src="/[+usluga_img_std+]" alt="[+pagetitle+]">
		<img class="hov" src="/[+usluga_img_hov+]" alt="[+pagetitle+]">
		<span class="service_introtext">
			<span class="service_title">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</span><br>
			<span class="service_desc">[+introtext+]</span>
		</span>
	</a>
	<div class="clear"></div>
</div>';
$c['comments_count'] = '<!-- KAMENT -->
<script type="text/javascript">
	/* * * НАСТРОЙКА * * */
	var kament_subdomain = \'top3seo\';

	/* * * НЕ МЕНЯЙТЕ НИЧЕГО НИЖЕ ЭТОЙ СТРОКИ * * */
	(function () {
		var node = document.createElement(\'script\'); node.type = \'text/javascript\'; node.async = true;
		node.src = \'http://\' + kament_subdomain + \'.svkament.ru/js/counter.js\';
		(document.getElementsByTagName(\'HEAD\')[0] || document.getElementsByTagName(\'BODY\')[0]).appendChild(node);
	}());
</script>
<noscript>Для отображения комментариев нужно включить Javascript</noscript>
<!-- /KAMENT -->';
$c['thankServicesKontekstAudit'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['tplReportPhx'] = '[+phx:if=`[+subject+]`:is=``:then=``:else=`<tr valign="top"><td>Тема сообщения: </td><td>[+subject:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+name+]`:is=``:then=``:else=`<tr valign="top"><td>Имя: </td><td>[+name:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+email+]`:is=``:then=``:else=`<tr valign="top"><td>Email: </td><td>[+email:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+url+]`:is=``:then=``:else=`<tr valign="top"><td>Адрес сайта: </td><td>[+url:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+telephone+]`:is=``:then=``:else=`<tr valign="top"><td>Телефон: </td><td>[+telephone:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+time_call+]`:is=``:then=``:else=`<tr valign="top"><td>Время звонка: </td><td>[+time_call:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+bonus+]`:is=``:then=``:else=`<tr valign="top"><td>Акция: </td><td>[+bonus:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+comment+]`:is=``:then=``:else=`<tr valign="top"><td>Сообщение: </td><td></td></tr>`+]
[+phx:if=`[+comment+]`:is=``:then=``:else=`<tr valign="top"><td colspan="2">[+comment:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+message+]`:is=``:then=``:else=`<tr valign="top"><td>Сообщение: </td><td></td></tr>`+]
[+phx:if=`[+message+]`:is=``:then=``:else=`<tr valign="top"><td colspan="2">[+message:strip:notags:esc+]</td></tr>`+]';
$c['noreplyOrderSeo'] = '<p>Здравствуйте [+name+]. Ваша заявка на продвижение сайта принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyOrderAudit'] = '<p>Здравствуйте [+name+]. Ваша заявка на экспресс-аудит сайта [+url+] принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['Duplicate of tplReportPhx'] = '[+phx:if=`[+ids+]`:is=``:then=``:else=`<tr><td>FormID</td><td>[+ids:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+subject+]`:is=``:then=``:else=`<tr><td>Subject</td><td>[+subject:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+service+]`:is=``:then=``:else=`<tr><td>FormTitle</td><td>[+service:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+name+]`:is=``:then=``:else=`<tr><td>Name</td><td>[+name:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+email+]`:is=``:then=``:else=`<tr><td>Email</td><td>[+email:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+url+]`:is=``:then=``:else=`<tr><td>Url</td><td>[+url:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+telephone+]`:is=``:then=``:else=`<tr><td>Telephone</td><td>[+telephone:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+time_call+]`:is=``:then=``:else=`<tr><td>TimeCall</td><td>[+time_call:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+comment+]`:is=``:then=``:else=`<tr><td>Message</td><td>[+comment:strip:notags:esc+]</td></tr>`+]
[+phx:if=`[+message+]`:is=``:then=``:else=`<tr><td>Message</td><td>[+message:strip:notags:esc+]</td></tr>`+]';
$c['clientTpl'] = '<li>
	
		<img src="[+brand_logo+]" alt="[+pagetitle+]" title="[+phx:if=`[+longtitle+]`:is=``:then=`[+pagetitle+]`:else=`[+longtitle+]`+]" />


</li>';
$c['Duplicate of clientTpl_inpage'] = '<li>
	<a href="[~[+id+]~]" class="open_iframe_client" data-fancybox-type="iframe">
		<img src="[+brand_logo+]" alt="[+pagetitle+]" title="[+phx:if=`[+longtitle+]`:is=``:then=`[+pagetitle+]`:else=`[+longtitle+]`+]" />
	</a>

</li>';
$c['rowTpl'] = '[+phx:if=`[[UltimateParent?]]`:is=`3`:then=`<li[+wf.id+][+wf.classes+] ><span [+phx:if=`[+wf.attributes+]`:is=``:then=``:else=`id="[+wf.attributes+]"`+]>[+wf.linktext+]</span>

<ul>
	[+phx:if=`[+wf.docid+]`:is=`[*id*]`:then=`<li class="active"><span title="[+wf.title+]">Описание услуги</span></li>`:else=`<li><a href="[~[+wf.docid+]~]" title="[+wf.title+]">Описание услуги</a></li>`+]
[!Ditto? tpl=`innerRowTpl_dt` &parents=`[+wf.docid+]` &orderBy=`menuindex ASC`!]
</ul>

</li>`:else=`
	[+phx:if=`[+wf.docid+]`:is=`[*id*]`:then=`
		<li class="active this"> <span class="item" title="[+wf.title+]" [+phx:if=`[+wf.attributes+]`:is=``:then=``:else=`id="[+wf.attributes+]"`+]>[+wf.linktext+]</span></li>
	`:else=`
		<li [+phx:if=`[+wf.attributes+]`:is=``:then=``:else=`id="[+wf.attributes+]"`+]><a href="[+wf.link+]" title="[+wf.title+]" [+phx:if=`[+wf.attributes+]`:is=``:then=``:else=`id="[+wf.attributes+]"`+]>[+wf.linktext+]</a></li>
	`+]
`+]




';
$c['right_menu'] = '<div class="right_menu">
				[[Wayfinder? 
					&startId=`[[UltimateParent?]]`
					&outerTpl=`outerTpl`
					&outerClass=`accordion`
					&rowTpl=`rowTpl`
					&innerTpl=`innerTpl`
					&innerRowTpl=`innerRowTpl`
					&ignoreHidden=`0`
				]]
				</div>';
$c['innerRowTpl'] = '[+phx:if=`[*id*]`:is=`[+wf.docid+]`:then=`<li[+wf.id+][+wf.classes+]><span title="[+wf.title+]" [+wf.attributes+]>[+wf.linktext+]</span>[+wf.wrapper+]</li>`:else=`<li[+wf.classes+] ><a href="[+wf.link+]" title="[+wf.title+]">[+wf.linktext+]</a>[+wf.wrapper+]</li>`+]';
$c['clientTpl_inpage'] = '<div class="brand">
		<span></span>
		<strong>
			<img src="[+brand_logo_2+]" alt="[+pagetitle+]" title="[+phx:if=`[+longtitle+]`:is=``:then=`[+pagetitle+]`:else=`[+longtitle+]`+]" />
		</strong>
	</div>';
$c['HowSeoTpl'] = '	<div id="how_seo_pop" class="iframe_form">	

		<p class="title">Можно ли продвинуть сайт:</p>

				<form id="how_seoform" action="[~[*id*]~]" onsubmit="clearcookie()" method="POST"> <!-- форма заказа звонка -->
					<input type="hidden" name="formid" value="how_seoform">
					<input type="hidden" name="service" value="Возможность продвижения" />
							<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
					<p class="row"><input id="name" type="text" name="name" placeholder="Ваше имя *" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
					<p class="row"><input id="email" type="text" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';" ><span class="gal"></span></p>
					<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта *" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
					<p class="row phone">
						<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;">
						<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
						
						<input type="text" style="display:none;" id="region" value="" name="region" >
					</p>
					
					
					<p class="req">* - Поля, обязательные для заполнения</p>					
					<div class="clear"></div>
					<button class="hs" type="submit">Узнать</button>
					
<input type="text" id="lastname" name="lastname" >
				
				</form>
</div>
	<div class="clear"></div>		';
$c['reportHowSeo'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['steps'] = '[!multiTV? tvName=`seoSteps` display=`all`!]';
$c['thankHowSeo'] = '<p style="text-align:center">Спасибо, что воспользовались формой обратной связи на нашем сайте.</p>';
$c['reportOrderSeo'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['medalTpl'] = '<img src="[+medal+]" alt="Сертификат [+pagetitle+]">';
$c['thankServicesSeo'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['thankServicesText'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['thankServicesKontekst'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['outerTpl'] = '<ul[+wf.classes+]>
	[+phx:if=`[[UltimateParent?]]`:is=`2`:then=`[[Wayfinder? &startId=`2` &includeDocs=`2` &startId=`0` &outerTpl=`outerTplempty` &rowTpl=`rowTpl` &ignoreHidden=`0`]]`:else=``+]
	
	[+wf.wrapper+]
</ul>';
$c['rowTplDitto'] = '[+phx:if=`[[UltimateParent?]]`:is=`3`:then=`<li[+wf.id+][+wf.classes+] id="[+wf.attributes+]"><span>[+wf.linktext+]</span>[+wf.wrapper+]</li>`:else=`<li[+wf.id+][+wf.classes+] id="[+wf.attributes+]"><a href="[+wf.link+]" title="[+wf.title+]" [+wf.attributes+]>[+wf.linktext+]</a></li>`+]
';
$c['outerTplempty'] = '	[+wf.wrapper+]';
$c['innerTpl'] = '<ul [+wf.classes+]>
	<li[+wf.classes+]><a href="[~[*id*]~]" title="[+wf.title+]">[+wf.docid+]</a></li>
	
	[+wf.wrapper+]
</ul>';
$c['innerRowTpl_dt'] = '[+phx:if=`[+id+]`:is=`[*id*]`:then=`
		<li class="active"><span>[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</span></li>
	`:else=`
		<li><a href="[~[+id+]~]" title="[+pagetitle+]">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</a></li>
	`+]';
$c['clientTpl_iframe'] = '	<span class="title">[*phx:if=`[*longtitle*]`:is=``:then=`[*pagetitle*]`:else=`[*longtitle*]`*]</span>
	<span class="service">[*client_service*]</span>
	<div class="desc"><img src="[*brand_logo*]" alt="[*pagetitle*]" /> [*content*]</div>
';
$c['noreplyOrderCall'] = '<p>Здравствуйте [+name+]. Ваша заявка на звонок принята. В указанное время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['thankOrderCall'] = '<form id ="orderCall" action="[~[*id*]~]" method="POST"> <!-- форма заказа звонка -->
			<span class="form_title">Заказать звонок специалиста</span><br><br>
	
			<input type="hidden" name="formid" value="Send">
			<input type="hidden" name="service" value="Звонок" />
			<input type="text" name="name" placeholder="Имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="hidden" id="phone_mask" checked eform="Название::0::#EVAL return true;">
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон *" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="text" name="time_call" placeholder="Время звонка" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			 <input type="text" style="display:none;" id="region" value="" name="region" >
			 <input type="text" id="lastname" name="lastname" />
			 <button id="contact-send" class="ordCall" type="submit">Заказать звонок</button>
			</form>
			
';
$c['OrderAudit'] = '	<div id="orderFreeAudit" class="iframe_form">	

		<p class="title">Заявка на бесплатный аудит сайта</p>
				<form id="how_seoform" action="[~[*id*]~]" onsubmit="clearcookie()" method="POST">
					<input type="hidden" name="formid" value="how_seoform">
					<input type="hidden" name="service" value="Бесплатный аудит" />	
					<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
					<p class="row"><input id="name" type="text" name="name" placeholder="Ваше имя *" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
					<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта *" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
					<p class="row"><input id="email" type="text" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';" ><span class="gal"></span></p>
					
					<p class="row phone">
						<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;">
						<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
						<input type="text" style="display:none;" id="region" value="" name="region" >
					</p>
					
					<p class="req">* - Поля, обязательные для заполнения</p>					
					
					<button class="hs" type="submit">Отправить</button>
					
<input type="text" id="lastname" name="lastname" >
				
				</form>
</div>
			';
$c['thankOrderSeo'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['thankServicesAudit'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['slogan'] = '<div class="slogan">
	<h1>Оторваться от толпы преследователей — это хорошо организовать работу с информацией. <em>© Билл Гейтс.</em></h1>
		</div>';
$c['menu404Tpl'] = '<div class="serv">
			<p>[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</p>
			[[Wayfinder? 
					&startId=`[+id+]` 
					
				]]
			</div>';
$c['thankOrderAudit'] = '<p style="text-align:center">Аудит скоро будет проведён</p>';
$c['Duplicate of top_menu_li'] = '[+phx:if=`[[UltimateParent?]]`:is=`[[UltimateParent? &id=`[+wf.docid+]`]]`:then=`<li[+wf.id+][+wf.classes+]><span title="[+wf.title+]" [+wf.attributes+]>[+wf.linktext+]</span>[+wf.wrapper+]</li>`:else=`<li[+wf.id+][+wf.classes+]><a href="[+wf.link+]" title="[+wf.title+]" [+wf.attributes+]>[+wf.linktext+]</a>[+wf.wrapper+]</li>`+]';
$c['Duplicate of clientTpl'] = '<li>
	<a href="[~[+id+]~]" class="open_iframe_client" data-fancybox-type="iframe">
		<img src="[+brand_logo+]" alt="[+pagetitle+]" title="[+phx:if=`[+longtitle+]`:is=``:then=`[+pagetitle+]`:else=`[+longtitle+]`+]" />
	</a>

</li>';
$c['reportOrderAudit'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['ServicesSeoForm'] = '<p class="title">Заказать <span>[!servicevar!]</span></p>
<form id="ServicesForm" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="ServicesForm" />
			<input type="hidden" name="service" value="[!servicevar!]" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" title="Пожалуйста, введите Ваше имя" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<input type="text" id="lastname" name="lastname" >
				<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта *" title="Нам необходимо изучить Ваш сайт на пригодность к продвижению" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" title="Оставьте Ваш адрес электронной почты, чтобы мы могли с Вами связаться."onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<p class="row">
			<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;" >
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон" title="Вы можете так же оставить и свой телефон - наши менеджеры свяжутся с Вами в ближайшее время."><br>
			<input type="text" style="display:none;" id="region" value="" name="region" ></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" title="Опишите цель продвижения, область деятельности, регион или просто задайте вопрос." placeholder="Ваши пожелания..."></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['reportServicesSeo'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['ServicesAuditForm'] = '<p class="title">Заказать <span>[!servicevar!]</span></p>
<form id="ServicesForm" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="ServicesForm" />
			<input type="hidden" name="service" value="[!servicevar!]" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<input type="text" id="lastname" name="lastname" >
				<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта *" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<p class="row">
			<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;" >
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
			<input type="text" style="display:none;" id="region" value="" name="region" ></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" placeholder="Ваши пожелания..."></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['ServicesKontekstForm'] = '<p class="title">Заказать <span>контекстную рекламу</span></p>
<form id="ServicesKontekstForm" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="ServicesKontekstForm" />
			<input type="hidden" name="service" value="Контекстная реклама" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<input type="text" id="lastname" name="lastname" >
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	<p class="row"><input id="url" type="text" name="url" placeholder="Адрес сайта" onclick="if (this.className==\'valid\') this.className=\'\';"><span class="gal"></span></p>
	
	<p class="row">
			<input type="hidden" id="phone_mask" checked="checked" eform="Название::0::#EVAL return true;" >
			<input type="text" id="customer_phone" name="telephone" placeholder="Телефон"><br>
			<input type="text" style="display:none;" id="region" value="" name="region" ></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" placeholder="Ваши пожелания..."></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['reportServicesAudit'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['reportServicesKontekst'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['ServicesTextForm'] = '<p class="title">Заказать <span>[!servicevar!]</span></p>
<form id="ServicesForm" action="" method="POST"> <!-- форма заказа продвижения -->
			<input type="hidden" name="formid" value="ServicesForm" />
			<input type="hidden" name="service" value="[!servicevar!]" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
				<p class="row">
					<input type="text" name="name" id="name" placeholder="Ваше имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
	
	<input type="text" id="lastname" name="lastname" >
				<p class="row">
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"></p>
				<p class="row tarea">
			<textarea value="" name="comment" id="message" placeholder="Ваши пожелания..."></textarea><br>
				</p>
			 <p class="req">* - Поля, обязательные для заполнения</p>
				<div class="clear"></div>
			 <button class="ordSeo" type="submit">Заказать</button>
		
			</form>
			<div class="clear"></div>';
$c['reportServicesText'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['tarifs'] = '<table id="tar" class="features-table">
							<thead>
								<tr>
									<td style="text-align: center;"></td>
									<td style="text-align: center;"><span data-mce-mark="1">Optima</span></td>
									<td style="text-align: center;"><span data-mce-mark="1">Business</span></td>
									<td style="text-align: center;"><span data-mce-mark="1">Premium</span></td>
								</tr>
							</thead>
							<tbody>
								[!multiTV? tvName=`tarifswrd` display=`all` rowTpl=`bonusTpl`!]
								[!multiTV? tvName=`tarifstext` display=`all` rowTpl=`bonusTpl`!]
							</tbody>
							<tfoot>
								[!multiTV? tvName=`price`!]
							</tfoot>
						</table>';
$c['order_button_in_kontekst'] = '				<div class="order_button">
					<a href="[~88~]?service=Контекстная реклама" class="poplight open_iframe" data-fancybox-type="iframe">Заказать услугу</a>
				</div>';
$c['news_list'] = '<div class="anons">
	<a class="anons_title" href="[~[+id+]~]">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</a>
	<div class="anons_introtext">
		<img src="[+article_img:replace=`small,medium`+]">
		<p>[+introtext+]</p>
		<a class="read_more" href="[~[+id+]~]">Читать дальше</a>
		<div class="clear"></div>
	</div>
	<div class="anons_info">
		<span class="pub_date">[[datarus? &MyDate=`[+createdon+]`]]</span>
	</div>
</div>';
$c['bonusTpl'] = '<tr>
	<td class="pdinfohead">
		<div class="par-name">
            <a class="par-link" href="#" onclick="togle_par_about(\'[+row.total+][+iteration+]\'); return false;">[+bonus+]</a>
            <a class="par-link  par-q-link-img" href="#" onclick="togle_par_about(\'[+row.total+][+iteration+]\'); return false;">
                <img class="par-q-link" style="visibility: hidden" src="/images/ico/question.png" alt="Описание" border="0">
             </a>                         
        </div>
		
		<div id="[+row.total+][+iteration+]" class="par-about-q" style="display: none">
            <div class="par-about-popup">
                <div class="par-about-img"><img src="/images/par-about-bg.png"></div>
                <div class="ie-shadow">
                    <div class="par-about-inner">
						
                         [+descr+]
                    </div>
                </div>
                <div class="par-about-bg">
                    <div class="par-about-inner">
						
						[+descr+]
					</div>
                </div>
            </div>
        </div>
	</td>
	
	<td>
		[+phx:if=`[+optima+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+phx:if=`[+optima+]`:is=`+`:then=`<img class="quest" src="/images/ico/check.png">`:else=`[+optima+]`+]`+]
	</td>
	<td>
		[+phx:if=`[+business+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+phx:if=`[+business+]`:is=`+`:then=`<img class="quest" src="/images/ico/check.png">`:else=`[+business+]`+]`+]
	</td>
	<td>
		[+phx:if=`[+premium+]`:is=``:then=`<img src="/images/ico/cross.png">`:else=`[+phx:if=`[+premium+]`:is=`+`:then=`<img class="quest" src="/images/ico/check.png">`:else=`[+premium+]`+]`+]
	</td>
</tr>';
$c['one_news'] = '<div class="el">
	<p class="title"><a href="[~[+id+]~]">[+pagetitle+]</a></p>
	<img src="[+article_img:replace=`small,medium`+]" alt="[+pagetitle+]"/>
	<div class="anons">[[summary? &text=`[+summary+]` &len=`3600`  &cut=`<!--cut-->` &dotted=`2`]]<br><a class="read_more" href="[~[+id+]~]">Читать дальше</a></div>
	<div class="clear"></div>
</div>';
$c['backup last_article'] = '<div class="last_article">
	<div class="head_last">
		<a href="[~[+id+]~]">
		<img src="[+article_img+]">
		<span class="title">[+phx:if=`[+menutitle+]`:is=``:then=`[+pagetitle+]`:else=`[+menutitle+]`+]</span>
		</a>
	</div>
		<span class="intro">
			
			[[summary? &text=`[+content+]` &len=`120` &noparser=`0`]]</span>... <a class="read_more" href="[~[+id+]~]">Читать дальше</a>
	<br>
		
		<div class="clear"></div>
</div>';
$c['feedbackForm'] = '<form id ="feedback" action="[~[*id*]~]" method="POST"> <!-- форма обратной связи -->
			<p class="title">Напишите нам</p>
			<input type="hidden" name="formid" value="feedback">
			<input type="hidden" name="service" value="Обратная связь" />
			<input type="hidden" name="ids" value="[*id*]" eform="Название::0::#EVAL return true;" >
			<input type="text" name="name" placeholder="Имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="text" name="subject" placeholder="Тема сообщения *" eform_options="Тема::1" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<textarea value="" name="message" id="message" placeholder="Ваше сообщение *"></textarea><br>
			 <input type="text" style="display:none;" id="region" value="" name="region" >
			 <input type="text" id="lastname" name="lastname" />
	
	<p class="req">* - Поля, обязательные для заполнения</p>
			 <button id="contact-send" class="ordCall" type="submit">Отправить</button>
			</form>
			
';
$c['reportFeedbackForm'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['thankFeedbackForm'] = '<form id ="feedback" action="[~[*id*]~]" method="POST"> <!-- форма обратной связи -->
			<p class="title">Обратная связь</p>
			<input type="hidden" name="formid" value="feedback">
			<input type="hidden" name="service" value="Обратная связь" />
			<input type="text" name="name" placeholder="Имя *" eform_options="Имя::1" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="text" id="email" name="email" placeholder="E-mail *" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<input type="text" name="theme" placeholder="Тема сообщения *" eform_options="Тема::1" onclick="if (this.className==\'valid\') this.className=\'\';"><br>
			<textarea value="" name="message" id="message" placeholder="Ваше сообщение..."></textarea><br>
			 <input type="text" style="display:none;" id="region" value="" name="region" >
			 <input type="text" id="lastname" name="lastname" />
	<p class="req">* - Поля, обязательные для заполнения</p>
			 <button id="contact-send" class="ordCall" type="submit">Отправить</button>
			</form>
			
';
$c['order_button_in_seo'] = '				<div class="order_button">
					<a href="[~86~]?service=SEO Продвижение" class="poplight open_iframe" data-fancybox-type="iframe">Заказать продвижение</a>
				</div>';
$c['noreplyFeedbackForm'] = '<p>Здравствуйте [+name+]. Ваше сообщение «[+subject+]» отправлено. В ближайшее время вам ответит наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyServicesSeo'] = '<p>Здравствуйте [+name+]. Ваша заявка на [+service+] сайта [+url+] принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyServicesAudit'] = '<p>Здравствуйте [+name+]. Ваша заявка на [+service+] сайта [+url+] принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyServicesKontekst'] = '<p>Здравствуйте [+name+]. Ваша заявка на контекстную рекламу принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyServicesKontekstAudit'] = '<p>Здравствуйте [+name+]. Ваша заявка на аудит рекламной компании принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['noreplyServicesText'] = '<p>Здравствуйте [+name+]. Ваша заявка на [+service+] принята. В ближайшее время с вами свяжется наш менеджер.</p>

<p>Указанная вами информация:</p>
<table>
	[+output+]
</table>
<p>Спасибо, что обратились к нам.</p>
<hr>
<p>С уважением, команда TOP3SEO.<br>
	<a href="http://www.top3seo.ru/">
		<img src="http://www.top3seo.ru/images/logo.png" alt="Продвижение сайтов - Top3Seo"/>
	</a>
<br>
<em>
8 (800) 500-17-90<br>
sales@top3seo.ru<br>
350059, г. Краснодар, ул. Новороссийская, д. 172, оф. 17.</em>
</p>';
$c['reportOrderCall'] = '<p>Посетитель по имени [+name+] оставил заявку на [+service+]:</p>
<table>
	[+output+]
</table>';
$c['thankOrderCallIframe'] = '<p style="text-align:center">Мы с вами свяжемся</p>';
$c['sidebar_test'] = '			<div class="right">

				{{right_menu}}
				
				<div class="metrika">
					[!Metrika? &tpl=`metrika_sidebar`!]
					<div class="clear"></div>
				</div>
				
				<div class="certified">
					[!Ditto? &parents=`63` &tpl=`medalTpl` &randomize=`1` &display=`1`!]
					<span><a class="moresert" href="[~78~]">Наши сертификаты</a></span>
					<div class="clear"></div>
				</div>
				[+phx:if=`[[UltimateParent?]]`:is=`2`:or:is=`5`:then=``:else=`[!Ditto? &tpl=`last_article` &parents=`5` &depth=`5` &display=`1` &hideFolders=`1` &extenders=`summary` &truncOffset=`10` &truncLen=`150` &truncText=`... Читать далее`!]`+]
			</div>';
$c['metrika_sidebar'] = '<div class="area">
	<span class="count">[+visits+]</span>
	<span class="name">Посетителей из поиска</span>
</div>';
$c['how_starting_kontekst'] = '<div class="order_button">
	<span>Нажмите на кнопку</span>
				<a href="[~88~]?service=Контекстная реклама" class="poplight open_iframe" data-fancybox-type="iframe">Заказать услугу</a>
	<span>или позвоните:</span>
	<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				</div>';
$c['howitworks'] = '[!multiTV? 
	&tvName=`howitworks` 
	&display=`all` 
	&outerTpl=`@CODE:
		<div id="how_work">
			[!multiTV? 
				&tvName=`howitworks` 
				&display=`all` 
				&outerTpl=`@CODE:<ul>((wrapper))</ul>`
				&rowTpl=`@CODE:<li>
					<a class="tab" href="#tabs-((iteration))" title="">
						<img src="((image_unhov))" class="un_hov" alt="((title))">
						<img src="((image_hov))" class="hov" alt="((title))">
						<span>((title))</span>
					</a>
				</li>`
			&outputSeparator=`<li class="separator"><img src="/images/hwork_seo/sepr.png" alt=""></li>`
			!]
			<div id="tabs_container">((wrapper))</div></div>`
	&rowTpl=`@CODE:<div id="tabs-((iteration))">((text))</div>`!]';
$c['faq_block_seo'] = '[!multiTV? 
	&tvName=`faq_seo` 
	&display=`all` 
!]';
$c['clients_inside'] = '<div class="brands_carousel_inside_page">
				<ul class="brands" id="brands_carousel_inside_page">
			[!Ditto? &tpl=`clientTpl` &parents=`4` &depth=`5` &hideFolders=`1` &randomize=`1` &showInMenuOnly=`1`!]
			</ul>
				
				</div>';
$c['how_starting_seo'] = '<div class="order_button">
	<span>Нажмите на кнопку</span>
				<a href="[~86~]?service=SEO Продвижение" class="poplight open_iframe" data-fancybox-type="iframe">Заказать продвижение</a>
	<span>или позвоните:</span>
	<a class="tel" href="tel:88005001732">8 (800) 500-17-90</a>
				</div>';
$c['metrika_tpl'] = '<div class="total">
	<div class="dates">
		<span class="start">[+start+]</span>
		<span class="def"> - </span>
		<span class="finish">[+finish+]</span>
	</div>
	<div class="big">
		<div class="cell">
			<span class="name">Посещений:</span>
			<span class="value">[+visits+]</span>
		</div>
		<div class="cell">
			<span class="name">Посетителей:</span>
			<span class="value">[+visitors+]</span>
		</div>
		<div class="cell">
			<span class="name">Новых:</span>
			<span class="value">[+newvisitors+]</span>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="small">
		<div class="cell">
			<span class="name">по ссылкам</span>
			<span class="value">[+links+]</span>
		</div>
		<div class="cell">
			<span class="name">из поиска</span>
			<span class="value">[+search+]</span>
		</div>
		<div class="cell">
			<span class="name">по рекламе</span>
			<span class="value">[+advertising+]</span>
		</div>
		<div class="cell last">
			<span class="name">из соц сетей</span>
			<span class="value">[+social+]</span>
		</div>
		<div class="clear"></div>
	</div>
</div>';
$c['reviews'] = '<div id="reviews" class="block">
	<h2>Отзывы наших клиентов</h2>
	<noindex>
		[[Ditto? &parents=`143` &display=`3` &tpl=`one_review` &extenders=`summary` &hideFolders=`1`]]
	</noindex>
</div>';
$c['one_review'] = '		<div class="review">
			<span class="title">[+pagetitle+]</span>
			<span class="client">
				[!GetField? &docid=`[+client_name+]` &field=`pagetitle`!]
			</span>
			<div class="logo">
				<div><img src="[!GetField? &docid=`[+client_name+]` &field=`brand_logo_2`!]" alt="[!GetField? &docid=`[+client_name+]` &field=`pagetitle`!]"></div>
			</div>
			<div class="text">
				[+content+]
			</div>
			<div class="clear"></div>
			<span class="site">Сайт: [!GetField? &docid=`[+client_name+]` &field=`client_url`!]</span>
		</div>';
$s = &$this->snippetCache;
$s['AjaxSearch'] = 'return require MODX_BASE_PATH.\'assets/snippets/ajaxSearch/snippet.ajaxSearch.php\';';
$s['Breadcrumbs'] = 'return require MODX_BASE_PATH.\'assets/snippets/breadcrumbs/snippet.breadcrumbs.php\';';
$s['Ditto'] = 'return require MODX_BASE_PATH.\'assets/snippets/ditto/snippet.ditto.php\';';
$s['eForm'] = 'return require MODX_BASE_PATH.\'assets/snippets/eform/snippet.eform.php\';';
$s['FirstChildRedirect'] = 'return require MODX_BASE_PATH.\'assets/snippets/firstchildredirect/snippet.firstchildredirect.php\';';
$s['if'] = 'return require MODX_BASE_PATH.\'assets/snippets/if/snippet.if.php\';';
$s['Jot'] = '/*####
#
# Author: Armand "bS" Pondman (apondman@zerobarrier.nl)
#
# Latest Version: http://modxcms.com/Jot-998.html
# Jot Demo Site: http://projects.zerobarrier.nl/modx/
# Documentation: http://wiki.modxcms.com/index.php/Jot (wiki)
#
####*/

$jotPath = $modx->config[\'base_path\'] . \'assets/snippets/jot/\';
include_once($jotPath.\'jot.class.inc.php\');

$Jot = new CJot;
$Jot->VersionCheck("1.1.4");
$Jot->Set("path",$jotPath);
$Jot->Set("action", $action);
$Jot->Set("postdelay", $postdelay);
$Jot->Set("docid", $docid);
$Jot->Set("tagid", $tagid);
$Jot->Set("subscribe", $subscribe);
$Jot->Set("moderated", $moderated);
$Jot->Set("captcha", $captcha);
$Jot->Set("badwords", $badwords);
$Jot->Set("bw", $bw);
$Jot->Set("sortby", $sortby);
$Jot->Set("numdir", $numdir);
$Jot->Set("customfields", $customfields);
$Jot->Set("guestname", $guestname);
$Jot->Set("canpost", $canpost);
$Jot->Set("canview", $canview);
$Jot->Set("canedit", $canedit);
$Jot->Set("canmoderate", $canmoderate);
$Jot->Set("trusted", $trusted);
$Jot->Set("pagination", $pagination);
$Jot->Set("placeholders", $placeholders);
$Jot->Set("subjectSubscribe", $subjectSubscribe);
$Jot->Set("subjectModerate", $subjectModerate);
$Jot->Set("subjectAuthor", $subjectAuthor);
$Jot->Set("notify", $notify);
$Jot->Set("notifyAuthor", $notifyAuthor);
$Jot->Set("validate", $validate);
$Jot->Set("title", $title);
$Jot->Set("authorid", $authorid);
$Jot->Set("css", $css);
$Jot->Set("cssFile", $cssFile);
$Jot->Set("cssRowAlt", $cssRowAlt);
$Jot->Set("cssRowMe", $cssRowMe);
$Jot->Set("cssRowAuthor", $cssRowAuthor);
$Jot->Set("tplForm", $tplForm);
$Jot->Set("tplComments", $tplComments);
$Jot->Set("tplModerate", $tplModerate);
$Jot->Set("tplNav", $tplNav);
$Jot->Set("tplNotify", $tplNotify);
$Jot->Set("tplNotifyModerator", $tplNotifyModerator);
$Jot->Set("tplNotifyAuthor", $tplNotifyAuthor);
$Jot->Set("tplSubscribe", $tplSubscribe);
$Jot->Set("debug", $debug);
$Jot->Set("output", $output);
return $Jot->Run();';
$s['ListIndexer'] = 'return require MODX_BASE_PATH.\'assets/snippets/listindexer/snippet.listindexer.php\';';
$s['MemberCheck'] = 'return require MODX_BASE_PATH.\'assets/snippets/membercheck/snippet.membercheck.php\';';
$s['Personalize'] = 'return require MODX_BASE_PATH.\'assets/snippets/personalize/snippet.personalize.php\';';
$s['phpthumb'] = 'return require MODX_BASE_PATH.\'assets/snippets/phpthumb/snippet.phpthumb.php\';
';
$s['Reflect'] = '/*
 * Author: 
 *      Mark Kaplan for MODx CMF
 * 
 * Note: 
 *      If Reflect is not retrieving its own documents, make sure that the
 *          Ditto call feeding it has all of the fields in it that you plan on
 *       calling in your Reflect template. Furthermore, Reflect will ONLY
 *          show what is currently in the Ditto result set.
 *       Thus, if pagination is on it will ONLY show that page\'s items.
*/
 

// ---------------------------------------------------
//  Includes
// ---------------------------------------------------

$reflect_base = isset($reflect_base) ? $modx->config[\'base_path\'].$reflect_base : $modx->config[\'base_path\']."assets/snippets/reflect/";
/*
    Param: ditto_base
    
    Purpose:
    Location of Ditto files

    Options:
    Any valid folder location containing the Ditto source code with a trailing slash

    Default:
    [(base_path)]assets/snippets/ditto/
*/

$config = (isset($config)) ? $config : "default";
/*
    Param: config

    Purpose:
    Load a custom configuration

    Options:
    "default" - default blank config file
    CONFIG_NAME - Other configs installed in the configs folder or in any folder within the MODx base path via @FILE

    Default:
    "default"
    
    Related:
    - <extenders>
*/

require($reflect_base."configs/default.config.php");
require($reflect_base."default.templates.php");
if ($config != "default") {
    require((substr($config, 0, 5) != "@FILE") ? $reflect_base."configs/$config.config.php" : $modx->config[\'base_path\'].trim(substr($config, 5)));
}

// ---------------------------------------------------
//  Parameters
// ---------------------------------------------------

$id = isset($id) ? $id."_" : false;
/*
    Param: id

    Purpose:
    Unique ID for this Ditto instance for connection with other scripts (like Reflect) and unique URL parameters

    Options:
    Any valid folder location containing the Ditto source code with a trailing slash

    Default:
    "" - blank
*/
$getDocuments = isset($getDocuments) ? $getDocuments : 0;
/*
    Param: getDocuments

    Purpose:
    Force Reflect to get documents

    Options:
    0 - off
    1 - on
    
    Default:
    0 - off
*/
$showItems = isset($showItems) ? $showItems : 1;
/*
    Param: showItems

    Purpose:
    Show individual items in the archive

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/
$groupByYears = isset($groupByYears)? $groupByYears : 1;
/*
    Param: groupByYears

    Purpose:
    Group the archive by years

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/
$targetID = isset($targetID) ? $targetID : $modx->documentObject[\'id\'];
/*
    Param: targetID

    Purpose:
    ID for archive links to point to

    Options:
    Any MODx document with a Ditto call setup with extenders=`dateFilter`
    
    Default:
    Current MODx Document
*/
$dateSource = isset($dateSource) ? $dateSource : "createdon";
/*
    Param: dateSource

    Purpose:
    Date source to display for archive items

    Options:
    # - Any UNIX timestamp from MODx fields or TVs such as createdon, pub_date, or editedon
    
    Default:
    "createdon"
    
    Related:
    - <dateFormat>
*/
$dateFormat = isset($dateFormat) ? $dateFormat : "%d-%b-%y %H:%M";  
/*
    Param: dateFormat

    Purpose:
    Format the [+date+] placeholder in human readable form

    Options:
    Any PHP valid strftime option

    Default:
    "%d-%b-%y %H:%M"
    
    Related:
    - <dateSource>
*/
$yearSortDir = isset($yearSortDir) ? $yearSortDir : "DESC";
/*
    Param: yearSortDir

    Purpose:
    Direction to sort documents

    Options:
    ASC - ascending
    DESC - descending

    Default:
    "DESC"
    
    Related:
    - <monthSortDir>
*/
$monthSortDir = isset($monthSortDir) ? $monthSortDir : "ASC";
/*
    Param: monthSortDir

    Purpose:
    Direction to sort the months

    Options:
    ASC - ascending
    DESC - descending

    Default:
    "ASC"
    
    Related:
    - <yearSortDir>
*/
$start = isset($start)? intval($start) : 0;
/*
    Param: start

    Purpose:
    Number of documents to skip in the results
    
    Options:
    Any number

    Default:
    0
*/  
$phx = (isset($phx))? $phx : 1;
/*
    Param: phx

    Purpose:
    Use PHx formatting

    Options:
    0 - off
    1 - on
    
    Default:
    1 - on
*/

// ---------------------------------------------------
//  Initialize Ditto
// ---------------------------------------------------
$placeholder = ($id != false && $getDocuments == 0) ? true : false;
if ($placeholder === false) {
    $rID = "reflect_".rand(1,1000);
    $itemTemplate = isset($tplItem) ? $tplItem: "@CODE:".$defaultTemplates[\'item\'];
    $dParams = array(
        "id" => "$rID",
        "save" => "3",  
        "summarize" => "all",
        "tpl" => $itemTemplate,
    );
    
    $source = $dittoSnippetName;
    $params = $dittoSnippetParameters;
        // TODO: Remove after 3.0
        
    if (isset($params)) {
        $givenParams = explode("|",$params);
        foreach ($givenParams as $parameter) {
            $p = explode(":",$parameter);
            $dParams[$p[0]] = $p[1];
        }
    }
    /*
        Param: params

        Purpose:
        Pass parameters to the Ditto instance used to retreive the documents

        Options:
        Any valid ditto parameters in the format name:value 
        with multiple parameters separated by a pipe (|)
        
        Note:
        This parameter is only needed for config, start, and phx as you can
        now simply use the parameter as if Reflect was Ditto

        Default:
        [NULL]
    */
    
    $reflectParameters = array(\'reflect_base\',\'config\',\'id\',\'getDocuments\',\'showItems\',\'groupByYears\',\'targetID\',\'yearSortDir\',\'monthSortDir\',\'start\',\'phx\',\'tplContainer\',\'tplYear\',\'tplMonth\',\'tplMonthInner\',\'tplItem\',\'save\');
    $params =& $modx->event->params;
    if(is_array($params)) {
        foreach ($params as $param=>$value) {
            if (!in_array($param,$reflectParameters) && substr($param,-3) != \'tpl\') {
                $dParams[$param] = $value;
            }
        }
    }

    $source = isset($source) ? $source : "Ditto";
    /*
        Param: source

        Purpose:
        Name of the Ditto snippet to use

        Options:
        Any valid snippet name

        Default:
        "Ditto"
    */
    $snippetOutput = $modx->runSnippet($source,$dParams);
    $ditto = $modx->getPlaceholder($rID."_ditto_object");
    $resource = $modx->getPlaceholder($rID."_ditto_resource");
} else {
    $ditto = $modx->getPlaceholder($id."ditto_object");
    $resource = $modx->getPlaceholder($id."ditto_resource");
}
if (!is_object($ditto) || !isset($ditto) || !isset($resource)) {
    return !empty($snippetOutput) ? $snippetOutput : "The Ditto object is invalid. Please check it.";
}

// ---------------------------------------------------
//  Templates
// ---------------------------------------------------

$templates[\'tpl\'] = isset($tplContainer) ? $ditto->template->fetch($tplContainer): $defaultTemplates[\'tpl\'];
/*
    Param: tplContainer

    Purpose:
    Container template for the archive

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'year\'] = isset($tplYear) ? $ditto->template->fetch($tplYear): $defaultTemplates[\'year\'];
/*
    Param: tplYear

    Purpose:
    Template for the year item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'year_inner\'] = isset($tplYearInner) ? $ditto->template->fetch($tplYearInner): $defaultTemplates[\'year_inner\'];
/*
    Param: tplYearInner

    Purpose:
    Template for the year item (the ul to hold the year template)

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'month\'] = isset($tplMonth) ? $ditto->template->fetch($tplMonth): $defaultTemplates[\'month\'];
/*
    Param: tplMonth

    Purpose:
    Template for the month item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'month_inner\'] = isset($tplMonthInner) ? $ditto->template->fetch($tplMonthInner): $defaultTemplates[\'month_inner\'];
/*
    Param: tplMonthInner

    Purpose:
    Template for the month item  (the ul to hold the month template)

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/
$templates[\'item\'] = isset($tplItem) ? $ditto->template->fetch($tplItem): $defaultTemplates[\'item\'];
/*
    Param: tplItem

    Purpose:
    Template for the individual item

    Options:
    - Any valid chunk name
    - Code via @CODE:
    - File via @FILE:

    Default:
    See default.tempates.php
*/

$ditto->addField("date","display","custom");
    // force add the date field if receiving data from a Ditto instance

// ---------------------------------------------------
//  Reflect
// ---------------------------------------------------

if (function_exists("reflect") === FALSE) {
function reflect($templatesDocumentID, $showItems, $groupByYears, $resource, $templatesDateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir) {
    global $modx;
    $cal = array();
    $output = \'\';
    $ph = array(\'year\'=>\'\',\'month\'=>\'\',\'item\'=>\'\',\'out\'=>\'\');
    $build = array();
    $stop = count($resource);

    // loop and fetch all the results
    for ($i = $start; $i < $stop; $i++) {
        $date = getdate($resource[$i][$templatesDateSource]);
        $year = $date["year"];
        $month = $date["mon"];
        $cal[$year][$month][] = $resource[$i];
    }
    if ($yearSortDir == "DESC") {
        krsort($cal);
    } else {
        ksort($cal);
    }
    foreach ($cal as $year=>$months) {
        if ($monthSortDir == "ASC") {
            ksort($months);
        } else {
            krsort($months);
        }
        $build[$year] = $months;
    }
    
    foreach ($build as $year=>$months) {
        $r_year = \'\';
        $r_month = \'\';
        $r_month_2 = \'\';
        $year_count = 0;
        $items = array();
        
        foreach ($months as $mon=>$month) {
            $month_text = strftime("%B", mktime(10, 10, 10, $mon, 10, $year));
            $month_url = $ditto->buildURL("month=".$mon."&year=".$year."&day=false&start=0",$templatesDocumentID,$id);
            $month_count = count($month);
            $year_count += $month_count;
            $r_month = $ditto->template->replace(array("year"=>$year,"month"=>$month_text,"url"=>$month_url,"count"=>$month_count),$templates[\'month\']);
            if ($showItems) {
                foreach ($month as $item) {
                    $items[$year][$mon][\'items\'][] = $ditto->render($item, $templates[\'item\'], false, $templatesDateSource, $dateFormat, array(),$phx);
                }
                $r_month_2 = $ditto->template->replace(array(\'wrapper\' => implode(\'\',$items[$year][$mon][\'items\'])),$templates[\'month_inner\']);
                $items[$year][$mon] = $ditto->template->replace(array(\'wrapper\' => $r_month_2),$r_month);
            } else {
                $items[$year][$mon] = $r_month;
            }
        }
        if ($groupByYears) {
            $year_url = $ditto->buildURL("year=".$year."&month=false&day=false&start=0",$templatesDocumentID,$id);
            $r_year =  $ditto->template->replace(array("year"=>$year,"url"=>$year_url,"count"=>$year_count),$templates[\'year\']);
            $var = $ditto->template->replace(array(\'wrapper\'=>implode(\'\',$items[$year])),$templates[\'year_inner\']);
            $output .= $ditto->template->replace(array(\'wrapper\'=>$var),$r_year);
        } else {
            $output .= implode(\'\',$items[$year]);
        }
    }

    $output = $ditto->template->replace(array(\'wrapper\'=>$output),$templates[\'tpl\']);
    $modx->setPlaceholder($id.\'reset\',$ditto->buildURL(\'year=false&month=false&day=false\',$templatesDocumentID,$id));

return $output;
    
}
}

return reflect($targetID, $showItems, $groupByYears, $resource, $dateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir);';
$s['UltimateParent'] = 'return require MODX_BASE_PATH.\'assets/snippets/ultimateparent/snippet.ultimateparent.php\';';
$s['Wayfinder'] = 'return require MODX_BASE_PATH.\'assets/snippets/wayfinder/snippet.wayfinder.php\';';
$s['WebChangePwd'] = '# Created By Raymond Irving April, 2005
#::::::::::::::::::::::::::::::::::::::::
# Params:	
#
#	&tpl			- (Optional)
#		Chunk name or document id to use as a template
#				  
#	Note: Templats design:
#			section 1: change pwd template
#			section 2: notification template 
#
# Examples:
#
#	[[WebChangePwd? &tpl=`ChangePwd`]] 

# Set Snippet Paths 
$snipPath  = (($modx->insideManager())? "../":"");
$snipPath .= "assets/snippets/";

# check if inside manager
if ($m = $modx->insideManager()) {
	return \'\'; # don\'t go any further when inside manager
}


# Snippet customize settings
$tpl		= isset($tpl)? $tpl:"";

# System settings
$isPostBack		= count($_POST) && isset($_POST[\'cmdwebchngpwd\']);

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once $snipPath."weblogin/webchangepwd.inc.php";

# Return
return $output;



';
$s['WebLogin'] = '# Created By Raymond Irving 2004
#::::::::::::::::::::::::::::::::::::::::
# Params:	
#
#	&loginhomeid 	- (Optional)
#		redirects the user to first authorized page in the list.
#		If no id was specified then the login home page id or 
#		the current document id will be used
#
#	&logouthomeid 	- (Optional)
#		document id to load when user logs out	
#
#	&pwdreqid 	- (Optional)
#		document id to load after the user has submited
#		a request for a new password
#
#	&pwdactid 	- (Optional)
#		document id to load when the after the user has activated
#		their new password
#
#	&logintext		- (Optional) 
#		Text to be displayed inside login button (for built-in form)
#
#	&logouttext 	- (Optional)
#		Text to be displayed inside logout link (for built-in form)
#	
#	&tpl			- (Optional)
#		Chunk name or document id to as a template
#				  
#	Note: Templats design:
#			section 1: login template
#			section 2: logout template 
#			section 3: password reminder template 
#
#			See weblogin.tpl for more information
#
# Examples:
#
#	[[WebLogin? &loginhomeid=`8` &logouthomeid=`1`]] 
#
#	[[WebLogin? &loginhomeid=`8,18,7,5` &tpl=`Login`]] 

# Set Snippet Paths 
$snipPath = $modx->config[\'base_path\'] . "assets/snippets/";

# check if inside manager
if ($m = $modx->insideManager()) {
	return \'\'; # don\'t go any further when inside manager
}

# deprecated params - only for backward compatibility
if(isset($loginid)) $loginhomeid=$loginid;
if(isset($logoutid)) $logouthomeid = $logoutid;
if(isset($template)) $tpl = $template;

# Snippet customize settings
$liHomeId	= isset($loginhomeid)? explode(",",$loginhomeid):array($modx->config[\'login_home\'],$modx->documentIdentifier);
$loHomeId	= isset($logouthomeid)? $logouthomeid:$modx->documentIdentifier;
$pwdReqId	= isset($pwdreqid)? $pwdreqid:0;
$pwdActId	= isset($pwdactid)? $pwdactid:0;
$loginText	= isset($logintext)? $logintext:\'Login\';
$logoutText	= isset($logouttext)? $logouttext:\'Logout\';
$tpl		= isset($tpl)? $tpl:"";

# System settings
$webLoginMode = isset($_REQUEST[\'webloginmode\'])? $_REQUEST[\'webloginmode\']: \'\';
$isLogOut		= $webLoginMode==\'lo\' ? 1:0;
$isPWDActivate	= $webLoginMode==\'actp\' ? 1:0;
$isPostBack		= count($_POST) && (isset($_POST[\'cmdweblogin\']) || isset($_POST[\'cmdweblogin_x\']));
$txtPwdRem 		= isset($_REQUEST[\'txtpwdrem\'])? $_REQUEST[\'txtpwdrem\']: 0;
$isPWDReminder	= $isPostBack && $txtPwdRem==\'1\' ? 1:0;

$site_id = isset($site_id)? $site_id: \'\';
$cookieKey = substr(md5($site_id."Web-User"),0,15);

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once ($modx->config[\'site_manager_path\'] . "includes/crypt.class.inc.php");

if ($isPWDActivate || $isPWDReminder || $isLogOut || $isPostBack) {
	# include the logger class
	include_once $modx->config[\'site_manager_path\'] . "includes/log.class.inc.php";
	include_once $snipPath."weblogin/weblogin.processor.inc.php";
}

include_once $snipPath."weblogin/weblogin.inc.php";

# Return
return $output;
';
$s['WebLoginProps'] = '&loginhomeid=Login Home Id;string; &logouthomeid=Logout Home Id;string; &logintext=Login Button Text;string; &logouttext=Logout Button Text;string; &tpl=Template;string; ';
$s['WebSignup'] = '# Created By Raymond Irving April, 2005
#::::::::::::::::::::::::::::::::::::::::
# Usage:     
#    Allows a web user to signup for a new web account from the website
#    This snippet provides a basic set of form fields for the signup form
#    You can customize this snippet to create your own signup form
#
# Params:    
#
#    &tpl        - (Optional) Chunk name or document id to use as a template
#    &groups     - Web users groups to be assigned to users
#    &useCaptcha - (Optional) Determine to use (1) or not to use (0) captcha
#                  on signup form - if not defined, will default to system
#                  setting. GD is required for this feature. If GD is not 
#                  available, useCaptcha will automatically be set to false;
#                  
#    Note: Templats design:
#        section 1: signup template
#        section 2: notification template 
#
# Examples:
#
#    [[WebSignup? &tpl=`SignupForm` &groups=`NewsReaders,WebUsers`]] 

# Set Snippet Paths 
$snipPath = $modx->config[\'base_path\'] . "assets/snippets/";

# check if inside manager
if ($m = $modx->insideManager()) {
    return \'\'; # don\'t go any further when inside manager
}


# Snippet customize settings
$tpl = isset($tpl)? $tpl:"";
$useCaptcha = isset($useCaptcha)? $useCaptcha : $modx->config[\'use_captcha\'] ;
// Override captcha if no GD
if ($useCaptcha && !gd_info()) $useCaptcha = 0;

# setup web groups
$groups = isset($groups) ? explode(\',\',$groups):array();
for($i=0;$i<count($groups);$i++) $groups[$i] = trim($groups[$i]);

# System settings
$isPostBack        = count($_POST) && isset($_POST[\'cmdwebsignup\']);

$output = \'\';

# Start processing
include_once $snipPath."weblogin/weblogin.common.inc.php";
include_once $snipPath."weblogin/websignup.inc.php";

# Return
return $output;';
$s['WebSignupProps'] = '&tpl=Template;string; ';
$s['ddYMap'] = '
/**
 * ddYMap.php
 * @version 1.1.1 (2013-10-02)
 *
 * @desc A snippet that allows Yandex.Maps to be rendered on a page in a simple way.
 * 
 * @uses The snippet ddGetDocumentField 2.4 (if position getting from another field is required, see $geoPos).
 * 
 * @note Attention! The jQuery library should be included on the page.
 * @note From the pair of field/getField parameters one is required.
 * 
 * @param $geoPos {comma separated string} - Comma separated longitude and latitude. @required
 * @param $getField {string} - A field name with position that is required to be got.
 * @param $getId {integer} - Document ID with a field value needed to be received. Default: current document.
 * @param $mapElementId {string} - Container ID which the map is required to be embed in. Default: \'map\'.
 * @param $icon {string} - An icon to use (relative address). Default: without (default icon).
 * @param $iconOffset {comma separated string} - An offset of the icon in pixels (x, y).Basic position: the icon is horizontally centered with respect to x and its bottom position is y. Default: \'0,0\'.
 * @param $scrollZoom {0; 1} - Allow zoom while scrolling. Default: 0.
 * 
 * @link http://code.divandesign.biz/modx/ddymap/1.1.1
 *
 * @copyright 2013, DivanDesign
 * http://www.DivanDesign.biz
 */

//Если задано имя поля, которое необходимо получить
if (isset($getField)){
	$geoPos = $modx->runSnippet(\'ddGetDocumentField\', array(
		\'id\' => $getId,
		\'field\' => $getField
	));
}

//Если координаты заданы и не пустые
if (!empty($geoPos)){
	$mapElementId = isset($mapElementId) ? $mapElementId : \'map\';
	
	//Подключаем библиотеку карт
	$modx->regClientStartupScript(\'http://api-maps.yandex.ru/2.0-stable/?load=package.standard&amp;lang=ru-RU\', array(\'name\' => \'api-maps.yandex.ru\', \'version\' => \'2.0-stable\'));
	//Подключаем $.ddYMap
	$modx->regClientStartupScript(\'assets/js/jquery.ddYMap-1.0.min.js\', array(\'name\' => \'$.ddYMap\', \'version\' => \'1.0\'));
	
	//Инлайн-скрипт инициализации
	$inlineScript = \'(function($){$(function(){$.ddYMap.init({elementId: "\'.$mapElementId.\'", latLng: new Array(\'.$geoPos.\')\';
	
	//Если иконка задана
	if (!empty($icon)){
		//путь иконки на сервере
		$icon = ltrim($icon, \'/\');
		
		//Пытаемся открыть файл
		$iconHandle = @fopen($icon, \'r\');
		
		if ($iconHandle){
			//Получим её размеры
			$iconSize = getimagesize($icon);
			
			//если смещение не задано сделаем над опорной точкой ценруя по ширине
			$resultIconOffset = array($iconSize[0] / -2, $iconSize[1] * -1);
			if (!empty($iconOffset)){
				$iconOffset = explode(\',\', $iconOffset);
				//если задано сделает относительно положения по умолчанию
				$resultIconOffset[0] += $iconOffset[0];
				$resultIconOffset[1] += $iconOffset[1];
			}
			//Позиционируем точку по центру иконки
			$inlineScript .= \', placemarkOptions: {
				iconImageHref: "\'.$icon.\'",
				iconImageSize: [\'.$iconSize[0].\', \'.$iconSize[1].\'],
				iconImageOffset: [\'.$resultIconOffset[0].\', \'.$resultIconOffset[1].\']
			}\';
			
			fclose($iconHandle);
		}
	}
	
	//Если нужен скролл колесом мыши, упомянем об этом
	if (isset($scrollZoom) && $scrollZoom == 1){$inlineScript .= \', scrollZoom: true\';}
	
	$inlineScript .= \'});});})(jQuery);\';
	
	//Подключаем инлайн-скрипт с инициализацией
	$modx->regClientStartupScript(\'<script type="text/javascript">\'.$inlineScript.\'</script>\', array(\'plaintext\' => true));
}
';
$s['datarus'] = '
$MyDate= (isset($MyDate)) ? $MyDate: $modx -> documentObject[\'MyDate\'];
$output= (isset($output)) ? $output: "all";
$type= (isset($type)) ? $type: $modx -> documentObject[\'type\'];
$monthes = array(\'\',\'января\',\'февраля\',\'марта\',\'апреля\',\'мая\',\'июня\',\'июля\',\'августа\',\'сентября\',\'октября\',\'ноября\',\'декабря\');
$day = date("j" ,$MyDate);
$month = $monthes[date("n",$MyDate)];
$year = date("Y",$MyDate);
$h = date("G",$MyDate);
$i = date("i",$MyDate);
$nday=date("j");
$nh=date("G");
$ni = date("i");
if(!$type){
if($nday==$day && $hh<10){
$hh=$nh-$h;$ii=$i-$ni;
if($hh==1){$ht=" час ";}
if($hh<=4 && $hh!=1){$ht=" часа ";}
if($hh>4){$ht=" часов ";}
$nowy=abs($hh).$ht.abs($ii)." мин."; echo "Опубликовано ".$nowy." назад";}
else{echo $day.\' \'.$month.\' \'.$year.\' года\'." (".$h.":".$i.")";}
}
else{
	if ($output == "all") {
		echo "$day $month $year ";
	}
	if ($output == "day") {
		echo $day;
	}
	if ($output == "month") {
		echo $month;
	}
	if ($output == "year") {
		echo $year;
	}
	if ($output == "month, year") {
		echo "$month $year";
	}
	

	
}
';
$s['GetField'] = '
/*
==================================================
	GetField
==================================================

Returns any document field or template variable from any document or any of its parents.

Author: Grzegorz Adamiak [grad]
Version: 1.3 beta @2006-11-08 14:40:04
License: LGPL
MODx: 0.9.2.1+

See GetField.txt for instructions and version history.
--------------------------------------------------
*/

/* Parameters
----------------------------------------------- */

# $docid [ int ]
# ID of the document for which to get a field content.
# Default: current document

$gfIntDocId = (isset($docid)) ? $docid : $modx->documentIdentifier;

# $field [ string ]
# Name of the field for which to get the content:
# - any of the document object fields (http://modxcms.com/the-document-object.html)
# - template variable
# Default: \'pagetitle\'

$gfStrDocField = (isset($field)) ? trim($field) : \'pagetitle\';

# $parent [ 0 | 1 ]
# If set to 1, the snippet will return value for the document parent.
# Default: 0

$gfBoolParent = (isset($parent)) ? $parent : 0;

# $parentLevel [ int ]
# Specifies how high in the document tree to search for the parent of the document:
# - $parentLevel = 0 - returns the ultimate parent (right under site root)
# - $parentLevel = 1 - returns the direct parent
# Default: 0

$gfIntParentLevel = (isset($parentLevel) && is_int((int) $parentLevel)) ? $parentLevel : 0;

# $topid [ int ]
# Id of the topmost document in the document tree under which to search for a parent. Used only with $parent set to 1.
# Default: 0

$gfIntTopDocId = (isset($topid) && is_int((int) $topid)) ? $topid : 0;

/* Do not edit the code below!
----------------------------------------------- */

# Include logic

/*
==================================================
	GetField
==================================================

Returns any document field or template variable from any document or any of its parents.

Author: Grzegorz Adamiak [grad]
Version: 1.3 beta @2006-11-08 14:40:04
License: LGPL
MODx: 0.9.2.1+

See GetField.txt for instructions and version history.
--------------------------------------------------
*/

unset($docid, $field, $parent, $parentLevel, $topid);

# GetField functions
# ---------------------------------------------

# gfGetFieldContent
# Returns the inherited value of any content field
if (!function_exists(gfGetFieldContent))
{
	function gfGetFieldContent($modx,$gfIntDocId,$gfStrDocField)
	{
	/* apparently in 0.9.2.1 the getTemplateVarOutput function doesn\'t work as expected and doesn\'t return INHERITED value; this is probably to be fixed for next release; see http://modxcms.com/bugs/task/464
		$gfArrTV = $modx->getTemplateVarOutput($gfStrDocField,$gfIntDocId);
		return $gfArrTV[$gfStrDocField];
	*/

		while ($gfArrParent = $modx->getDocument($gfIntDocId,\'parent\'))
		{
			$gfArrTV = $modx->getTemplateVar($gfStrDocField,\'*\',$gfIntDocId);
			if (($gfArrTV[\'value\'] && substr($gfArrTV[\'value\'],0,8) != \'@INHERIT\') or !$gfArrTV[\'value\']) // tv default value is overriden (including empty)
			{
				$output = $modx->getTemplateVarOutput($gfStrDocField,$gfIntDocId);
				$output = $output[$gfStrDocField];
				break;
			}
			else // there is no parent with default value overriden
			{
				$output = trim(substr($gfArrTV[\'value\'],8));
			}
			$gfIntDocId = $gfArrParent[\'parent\']; // move up one document in document tree
		} // end while

		return $output;
	}
}

# gfGetParentId
# Returns the parent document ID
if (!function_exists(gfGetParentId))
{
	function gfGetParentId($modx, $gfIntDocId, $gfIntTopDocId, $gfIntParentLevel)
	{
		# build an array of document ancestors IDs
		$gfArrParentIds = array (); // initialize;
		$gfArrParentIds[] = $gfIntDocId; // add the specified document ID on first place

		// get IDs of all parents back to root of the document tree
		while (($gfArrParent = $modx->getDocument($gfIntDocId,\'parent\')) && ($gfArrParent[\'parent\'] != 0))
		{
			$gfIntDocId = $gfArrParent[\'parent\']; // move up one document in the document tree
			$gfArrParentIds[] = $gfIntDocId; // add parent ID to the array
		} // end while
		unset($gfIntDocId, $gfArrParent);

		$gfIntParentsCount = count($gfArrParentIds); // number of the parents

		# determine the ID of the specified parent
		switch ($gfIntTopDocId)
		{
			case 0: // not set or set to the root of the document tree

				($gfIntParentLevel && ($gfIntParentLevel < $gfIntParentsCount)) ?
					($gfIntDocId = $gfArrParentIds[$gfIntParentLevel]) : // find parent in specified levels up
					($gfIntDocId = $gfArrParentIds[$gfIntParentsCount - 1]); // if not set return the topmost (ultimate) parent

				break;

			default: // set to any other document

				$gfIntParentKey = array_search($gfIntTopDocId, $gfArrParentIds); // find the index of parent
				switch ($gfIntParentKey)
				{
					case 0: // not an ancestor or the document itself

						$gfIntDocId = 0;
						break;

					default: // parent is above the document in document tree

						($gfIntParentLevel && ($gfIntParentLevel < $gfIntParentKey)) ?
							($gfIntDocId = $gfArrParentIds[$gfIntParentLevel]) : // find parent in specified levels up
							($gfIntDocId = $gfArrParentIds[$gfIntParentKey - 1]);
				} // end switch
		} // end switch
		unset($gfIntTopDocId, $gfIntParentLevel, $gfIntParentsCount, $gfIntParentKey, $gfArrParentIds);

		return $gfIntDocId;
	}
}



# Get parent document ID
if ($gfBoolParent)
	$gfIntDocId = gfGetParentId($modx, $gfIntDocId, $gfIntTopDocId, $gfIntParentLevel);

# Get content of the field
$output = gfGetFieldContent($modx,$gfIntDocId,$gfStrDocField);

unset($gfIntDocId, $gfStrDocField, $gfBoolParent, $gfIntParentLevel, $gfIntTopDocId);

return $output;

?>
';
$s['summary'] = '
return require MODX_BASE_PATH.\'assets/snippets/summary/snippet.summary.php\';
';
$s['multiTV'] = '
return include(MODX_BASE_PATH.\'assets/tvs/multitv/multitv.snippet.php\');
';
$s['ajaxSubmit'] = '

/**
 * ajaxSubmit
 *  
 * Ajax sending of any form
 *  
 * @category 	   snippet
 * @version 	   1.0
 * @license 	   http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	   @properties 
 * @internal	   @modx_category Forms
 */

return require MODX_BASE_PATH.\'assets/plugins/ajax_submit/ajax_submit.inc.php\';

';
$s['checkField'] = '
function checkField(&$fields,&$vMsg,&$rMsg) {
    if(!empty($fields[\'lastname\'])) {
        return false;
    } else {
    return true;
    }
}

';
$s['formRender'] = '
function formRender(){
	    global $modx;
	$dbname = $modx->db->config[\'dbase\']; //имя базы данных
	$zakazy = $modx->getFullTableName( \'zakazy\' ); //таблица с вопросами
	
	$fields = array(
	"date" => time(),
	"name" => $_POST[\'name\'],
	"email" => $_POST[\'email\'],
	"message" => $_POST[\'comment\'],
	"tel" => $_POST[\'telephone\'],
	"timecall" => $_POST[\'time_call\'],
	"region" => $_POST[\'region\'],
	"service" => $_POST[\'service\']
     );
	
	$query = $modx->db->insert($fields, $zakazy, ""); 
	
echo \'
			<script type="text/javascript">
jQuery(document).ready(function() {
   $.fancybox.open(\\\'<p style="text-align:center">Спасибо, что воспользовались формой обратной связи на нашем сайте.</p>\\\');
});
                </script>
\';
}
';
$s['zakazWrite'] = '
//этот сниппет для тех форм, которые размещены на странице, а не в айфрейме
function GetRealIp(){
	if (!empty($_SERVER[\'HTTP_CLIENT_IP\'])) {
		$ip=$_SERVER[\'HTTP_CLIENT_IP\'];
	}
	elseif (!empty($_SERVER[\'HTTP_X_FORWARDED_FOR\'])){
		$ip=$_SERVER[\'HTTP_X_FORWARDED_FOR\'];
	}
	else{
		$ip=$_SERVER[\'REMOTE_ADDR\'];
	}
	return $ip;
}

function zakazWrite(){
global $modx;
	$dbname = $modx->db->config[\'dbase\']; //имя базы данных
	$zakazy = $modx->getFullTableName( \'zakazy\' ); //таблица с вопросами
	
	$fields = array( //составляем массив для записи в базу
	"date" => time(),
	"name" => htmlspecialchars($_POST[\'name\'], ENT_QUOTES),
	"email" => htmlspecialchars($_POST[\'email\'], ENT_QUOTES),
	"message" => htmlspecialchars($_POST[\'comment\'], ENT_QUOTES),
	"tel" => htmlspecialchars($_POST[\'telephone\'], ENT_QUOTES),
	"timecall" => htmlspecialchars($_POST[\'time_call\'], ENT_QUOTES),
	"region" => htmlspecialchars($_POST[\'region\'], ENT_QUOTES),
	"service" => htmlspecialchars($_POST[\'service\'], ENT_QUOTES),
	"site" => htmlspecialchars($_POST[\'url\'], ENT_QUOTES),
	"ip" => GetRealIp()
     );
	
	$query = $modx->db->insert($fields, $zakazy, ""); //запись 

include_once "manager/includes/controls/class.phpmailer.php"; //подключаем phpmailer
function php_mail($attach, $areas) { //функция для отправки данных из форм во вложении к письму
        global $modx;
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(false); //тип письма 
        $mail->From      = "sales@top3seo.ru"; //от кого (адрес)
        $mail->FromName  = "Top3Seo"; //от кого (Имя)
        $mail->Subject   = "To CRM"; //тема письма
		$mail->Body      = " "; // содержимое письма
        $mail->AddAddress("sitereq@top3seo.ru"); //кому
		$mail->AddBCC("ndmitrievich@upsales.ru", $name = "Никита Дмитриевич"); // скрытая копия
		$mail->AddBCC("vbayda@andata.ru", $name = "Василий Байда"); // скрытая копия
		$mail->AddStringAttachment($attach, "report.xml", $encoding = \'base64\', $type = \'application/octet-stream\'); //создаём вложение
        $mail->Send(); //отправка
}
	
	
	 //создаём содержимое xml файла
if ($_POST[\'ids\'] != 1) {
	$msg = ($_POST[\'message\']) ? $_POST[\'message\'] : $_POST[\'comment\'];
	$subj = ($_POST[\'subject\']) ? \'Тема:\' . $_POST[\'subject\'] . \'.\' : \'\';
	$mesg = ($msg) ? \'Сообщение:\' . $msg . \'.\' : \'\';
	$mail = ($_POST[\'email\']) ? \'Эл.адрес:\' . $_POST[\'email\'] . \'.\' : \'\';
	$tel = ($_POST[\'telephone\']) ? \'Тел:\' . $_POST[\'telephone\'] . \'.\' : \'\';
	
$row[0] = "<?xml version=\'1.0\' encoding=\'UTF-8\' ?>
<Document from=\'top3seo.ru\'>
       <Action EntityLogicalName=\'contact\' Message=\'find\'>
              <RulesCollection>
                     <Rule SchemaName=\'statuscode\' Value=\'1\'/>
                     <Rule SchemaName=\'emailaddress1\' Value=\'{$_POST[\'email\']}\'/>
              </RulesCollection>
              <CRMFieldsCollection>
                     <CRMField Sitefield=\'Email\' Type=\'String\'>
                            <Value>{$_POST[\'email\']}</Value>
                            <SchemaName>emailaddress1</SchemaName>
                     </CRMField>
                     <CRMField Sitefield=\'Name\' Type=\'String\'>
                            <Value>{$_POST[\'name\']}</Value>
                            <SchemaName>lastname</SchemaName>
                     </CRMField>
                     <CRMField Sitefield=\'Telephone\' Type=\'String\'>
                            <Value>{$_POST[\'telephone\']}</Value>                                                                                                            
                            <SchemaName>mobilephone</SchemaName>
                     </CRMField>
              </CRMFieldsCollection>
              <Action EntityLogicalName=\'opportunity\' Message=\'create\' RelationField=\'customerid\'>
                     <CRMFieldsCollection>
                            <CRMField Sitefield=\'FormID+FormTitle\' Type=\'String\'>
                                   <Value>SEO: запрос с сайта на {$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                                   <SchemaName>name</SchemaName>
                            </CRMField>
                            <CRMField Sitefield=\'Url+Subject+Message+Email+Telephone\' Type=\'String\'>
                                   <Value>{$_POST[\'url\']} {$subj} {$mesg} {$mail} {$tel}</Value>
                                   <SchemaName>description</SchemaName>
                            </CRMField>
                     </CRMFieldsCollection>
              </Action>
       </Action>
</Document>
";
	/*Формируем данные для CRM*/
	$dataCRM = array(\'category\' => \'category\',
	\'PhoneNumber\' => $_POST[\'telephone\'],
	\'ScheduledEnd\' => \'ScheduledEnd\',
	\'Subject\' => $subj,
	\'Description\' => $_POST[\'url\'].\' \'.$subj.\' \'.$mesg.\' \'.$mail.\' \'.$tel,
	\'Lead\' => array(\'FirstName\' => \'SEO: запрос с сайта на \'.$_POST[\'ids\'].\'-\'.$_POST[\'service\'],
		\'LastName\' => $_POST[\'name\'],
		\'Telephone1\' => $_POST[\'telephone\'],
		\'CompanyName\' => \'CompanyName\',
		\'Subject\' => $subj
		)
	);
	$url = \'http://crm.andata.ru:33334/api/Opportunity\';
	/*END Формируем данные для CRM*/
	
} else { 
$row[0] = "<?xml version=\'1.0\' encoding=\'UTF-8\' ?>
<Document from=\'top3seo.ru\'>
       <Action EntityLogicalName=\'lead\' Message=\'create\'>
              <CRMFieldsCollection>
                     <CRMField sitefield=\'Telephone\' Type=\'String\'>
                            <Value>{$_POST[\'telephone\']}</Value>
                            <SchemaName>telephone1</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'Name\' Type=\'String\'>
                            <Value>{$_POST[\'name\']}</Value>
                            <SchemaName>firstname</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'top3seo.ru+FormID+FormTitle\' Type=\'String\'>
                            <Value>top3seo.ru-{$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                            <SchemaName>companyname</SchemaName>                          
                     </CRMField>
                     <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                            <Value>SEO: запрос с сайта на звонок на №: {$_POST[\'telephone\']}, предпочт.время: {$_POST[\'time_call\']}, на имя: {$_POST[\'name\']}. Блок услуг: {$_POST[\'service\']}</Value>
                            <SchemaName>subject</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'FormID+FormTitle\' Type=\'String\'>
                            <Value>{$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                            <SchemaName>lastname</SchemaName>
                     </CRMField>
              </CRMFieldsCollection>          
              <Action EntityLogicalName=\'phonecall\' Message=\'create\' RelationField=\'regardingobjectid\'>
                     <CRMFieldsCollection>
                            <CRMField sitefield=\'Telephone\' Type=\'String\'>
                                   <Value>{$_POST[\'telephone\']}</Value>
                                   <SchemaName>phonenumber</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'FormID\' Type=\'String\'>
                                   <Value>{$_POST[\'ids\']}</Value>
                                   <SchemaName>category</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                                   <Value>SEO: {$_POST[\'telephone\']}; {$_POST[\'time_call\']}; {$_POST[\'name\']}; {$_POST[\'service\']}</Value>
                                   <SchemaName>subject</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                                   <Value>запрос с сайта на звонок на №: {$_POST[\'telephone\']}, предпочт.время: {$_POST[\'time_call\']}, на имя: {$_POST[\'name\']}. Блок услуг: {$_POST[\'service\']}</Value>
                                   <SchemaName>description</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'none\' Type=\'DateTime\'>
                                   <Value>".gmdate("m/d/Y H:i:s", mktime(date("H")+6, date("i"), date("s"), date("m")  , date("d"), date("Y")))."</Value>
                                   <SchemaName>scheduledend</SchemaName>
                            </CRMField>
                     </CRMFieldsCollection>
              </Action>
       </Action>
</Document>";
	
	/*Формируем данные для CRM*/
	$dataCRM = array(\'Name\'=>$_POST[\'name\'],
	\'Description\'=> \'запрос с сайта на звонок на №: \'.$_POST[\'telephone\'].\', предпочт.время: \'.$_POST[\'time_call\'].\', на имя: \'.$_POST[\'name\'].\'. Блок услуг: \'.$_POST[\'service\'],
	\'Contact\'=>array(\'EmailAddress1\'=>$_POST[\'email\'],
		\'LastName\'=>$_POST[\'ids\'].\'-\'.$_POST[\'service\'],
		\'MobilePhone\'=>$_POST[\'telephone\']
		)
	);
	$url = \'http://crm.andata.ru:33334/api/PhoneCall\';
	/*END Формируем данные для CRM*/
}
	
	
$xml_file = implode("\\n",$row); //преобразуем массив блоков в строку
	 
	 php_mail ($xml_file, $areas); //вызов самой функции
	
/*****
*Формируем json запрос в CRM и отаравляем его.
******/
$dataCRM_json = json_encode($dataCRM);
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(\'Content-Type: application/json\'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $dataCRM_json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_exec($ch);	
/*****
*END Формируем json запрос в CRM и отаравляем его.
******/
	
	
function noreply($email, $body, $subject) { //функция для отправки репорта клиенту
        global $modx;
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(true); //тип письма 
        $mail->From      = "noreply@top3seo.ru"; //от кого (адрес)
        $mail->FromName  = "Автоматическое сообщение"; //от кого (Имя)
        $mail->Subject   = $subject; //тема письма
		$mail->Body      = $body; // содержимое письма
        $mail->AddAddress($email); //кому
        $mail->Send(); //отправка
}

	//Отправка репорта клиенту 	

function getdata() { //выбираем только те, которые были отправлены
$postdata[] = (isset($_POST[\'subject\'])) ? \'<tr valign="top"><td>Тема сообщения: </td><td>\'.$_POST[\'subject\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'name\'])) ? \'<tr valign="top"><td>Имя: </td><td>\'.$_POST[\'name\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'email\'])) ? \'<tr valign="top"><td>Email: </td><td>\'.$_POST[\'email\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'url\'])) ? \'<tr valign="top"><td>Адрес сайта: </td><td>\'.$_POST[\'url\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'telephone\'])) ? \'<tr valign="top"><td>Телефон: </td><td>\'.$_POST[\'telephone\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'time_call\'])) ? \'<tr valign="top"><td>Время звонка: </td><td>\'.$_POST[\'time_call\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'comment\'])) ? \'<tr valign="top"><td>Сообщение: </td><td></td></tr><tr valign="top"><td colspan="2">\'.$_POST[\'comment\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'message\'])) ? \'<tr valign="top"><td>Сообщение: </td><td></td></tr><tr valign="top"><td colspan="2">\'.$_POST[\'message\'].\'</td></tr>\' : \'\';

$postdatastr = implode("\\n",$postdata); //преобразуем массив блоков в строку
return $postdatastr;
}
	
	
$noreplyTpls = array( //массив с названиями чанков шаблонов репортов и их тем id=>назание+тема
	1 => array(
		"name"=>"noreplyOrderCall",
		"subject"=>"Заявка на звонок"
		),
	6 => array(
		"name"=>"noreplyFeedbackForm",
		"subject"=>"Обратная связь «".$_POST[\'subject\']."»"
		),
	57 => array(
		"name"=>"noreplyHowSeo",
		"subject"=>"Заявка на возможность продвижения сайта ".$_POST[\'url\']
		),
	60 => array(
		"name"=>"noreplyOrderAudit",
		"subject"=>"Заявка на экспресс-аудит сайта ".$_POST[\'url\']
		),
	61 => array(
		"name"=>"noreplyOrderSeo",
		"subject"=>"Заявка на продвижение сайта"
		),
	86 => array(
		"name"=>"noreplyServicesSeo",
		"subject"=>"Заявка на продвижение сайта ".$_POST[\'url\']
		),
	87 => array(
		"name"=>"noreplyServicesAudit",
		"subject"=>"Заявка на аудит сайта ".$_POST[\'url\']
		),
	88 => array(
		"name"=>"noreplyServicesKontekst",
		"subject"=>"Заявка на контекстную рекламу"
		),
	89 => array(
		"name"=>"noreplyServicesText",
		"subject"=>"Заявка на ".$_POST[\'service\']
		),
	108 => array(
		"name"=>"noreplyServicesKontekstAudit",
		"subject"=>"Заявка на аудит рекламной компании"
		)
);
	
$body = $modx->parseChunk($noreplyTpls[$_POST[\'ids\']][\'name\'], array( //парсим чанк , заменяя плейсхолдеры и делаем его телом письма
		\'name\' => $_POST[\'name\'],
		\'url\' => $_POST[\'url\'],
		\'output\' => getdata(),
		\'subject\' => $_POST[\'subject\'],
		\'service\' => $_POST[\'service\']
	),
	\'[+\',
	\'+]\'
);

$email = $_POST[\'email\'];
$subject = $noreplyTpls[$_POST[\'ids\']][\'subject\'];

noreply ($email, $body, $subject); //вызов самой функции
}
';
$s['SiteMap'] = '//<?php
/**
 * Sitemap
 * 
 * Outputs a machine readable site map for search engines and robots.
 *
 * @category snippet
 * @version 1.0.11 (2012-10-01)
 * @license LGPL
 * @author Grzegorz Adamiak [grad], ncrossland, DivanDesign (http://www.DivanDesign.biz)
 * @internal @modx_category Navigation
 * 
 * @param startid {integer} - Id of the \'root\' document from which the sitemap starts. Default: 0.
 * @param format {string} - Which format of sitemap to use: sp (Sitemap Protocol used by Google), txt (text file with list of URLs), ror (Resource Of Resources). Default: sp.
 * @param seeThruUnpub {0; 1} - See through unpublished documents. Default: 1.
 * @param priority {string} - Name of TV which sets the relative priority of the document. If there is no such TV, this parameter will not be used. Default: \'sitemap_priority\'.
 * @param changefreq {string} - Name of TV which sets the change frequency. If there is no such TV this parameter will not be used. Default: \'sitemap_changefreq\'.
 * @param excludeTemplates {comma separated string} - Documents based on which templates should not be included in the sitemap. Comma separated list with names of templates. Default: \'\'.
 * @param excludeTV {string} - Name of TV (boolean type) which sets document exclusion form sitemap. If there is no such TV this parameter will not be used. Default: \'sitemap_exclude\'.
 * @param xsl {string; integer} - URL to the XSL style sheet or doc ID of the XSL style sheet. Default: \'\'.
 * @param excludeWeblinks {0; 1} - Should weblinks be excluded? You may not want to include links to external sites in your sitemap, and Google gives warnings about multiple redirects to pages within your site. Default: 0.
 */
 
 
/*
Supports the following formats:

- Sitemap Protocol used by Google Sitemaps
  (http://www.google.com/webmasters/sitemaps/)

- URL list in text format
  (e.g. Yahoo! submission)


Changelog:
# 1.0.11 (2012-10-01) by DivanDesign (http://www.DivanDesign.biz)
+ Document will be excluded from sitemap when changefreq parameter equals \'exclude\'.
* [(site_url)] (without alias) is using now for the start page ($modx->config[\'start_page\']) document url.
# 1.0.10 (2012-02-08) by DivanDesign (http://www.DivanDesign.biz)
+ Snippet can see through unpublished documents (by default). See the «seeThruUnpub» parameter.
* Minor changes of code and comments (see the code).
# 1.0.9 (2010-06-09) by ncrossland
- update metadata format for use in ModX 1.0.x installer
# 1.0.8 (2008-08-21)
- excludeTemplates can now also be specified as a template ID instead of template name. 
  Useful if you change the names of your templates frequently. (ncrossland)
  e.g. &excludeTemplates=`myTemplateName,3,4`
# 1.0.7 (2008-07-30)
- Unpublished and deleted documents were showing up in the sitemap. Even though they could not be viewed, 
  they were showing up as broken links to search engines. (ncrossland)
# 1.0.6 (2008-02-28)
- Add optional parameter (excludeWeblinks) to exclude weblinks from the sitemap, since they often point to external
  sites (which don\'t belong on your sitemap), or redirecting to other internal pages (which are already
  in the sitemap). Google Webmaster Tools generates warnings for excessive redirects.	
  Default is false - e.g. default behaviour remains unchanged. (ncrossland)
# 1.0.5 (2008-02-24)
- Modification about non searchable documents, as suggested by forum user JayBee
  (http://modxcms.com/forums/index.php/topic,5754.msg99895.html#msg99895)
# 1.0.4 (2008-02-06) by Bert Catsburg, bert@catsburg.com
- Added display option \'ulli\'. 
  An <ul><li> list of all published documents.
# 1.0.3 (2007-05-16)
- Added ability to specify the XSL URL - you don\'t always need one and it 
  seems to create a lot of support confusion!
  It is now a parameter (&xsl=``) which can take either an alias or a doc ID (ncrossland)
- Modifications suggested by forum users Grad and Picachu incorporated
  (http://modxcms.com/forums/index.php/topic,5754.60.html)
# 1.0.2 (2006-07-12)
- Reworked fetching of template variable value to
  get INHERITED value.
# 1.0.1
- Reworked fetching of template variable value,
  now it gets computed value instead of nominal;
  however, still not the inherited value.
# 1.0
- First public release.

TODO:
- provide output for ROR
--------------------------------------------------
*/

/* Parameters */
$startid = (isset($startid)) ? $startid : 0;
$seeThruUnpub = (isset($seeThruUnpub) && $seeThruUnpub == \'0\') ? false : true;
$format = (isset($format) && ($format != \'ror\')) ? $format : \'sp\';
$priority = (isset($priority)) ? $priority : \'sitemap_priority\';
$changefreq = (isset($changefreq)) ? $changefreq : \'sitemap_changefreq\';
$excludeTemplates = (isset($excludeTemplates)) ? $excludeTemplates : array();
$excludeTV = (isset($excludeTV)) ? $excludeTV : \'sitemap_exclude\';
$xsl = (isset($xsl)) ? $xsl : \'\';
if (is_numeric($xsl)){ $xsl = $modx->makeUrl($xsl); }
$excludeWeblinks = (isset($excludeWeblinks)) ? $excludeWeblinks : false;
/* End parameters */

# get list of documents
$docs = getDocs($modx, $startid, $priority, $changefreq, $excludeTV, $seeThruUnpub);


# filter out documents by template or TV
# ---------------------------------------------
// get all templates
$select = $modx->db->select("id, templatename", $modx->getFullTableName(\'site_templates\'));
while ($query = $modx->db->getRow($select)){
	$allTemplates[$query[\'id\']] = $query[\'templatename\'];
}

$remainingTemplates = $allTemplates;

// get templates to exclude, and remove them from the all templates list
if (!empty ($excludeTemplates)){
	
	$excludeTemplates = explode(",", $excludeTemplates);	
	
	// Loop through each template we want to exclude
	foreach ($excludeTemplates as $template){
		$template = trim($template);
		
		// If it\'s numeric, assume it\'s an ID, and remove directly from the $allTemplates array
		if (is_numeric($template) && isset($remainingTemplates[$template])){
			unset($remainingTemplates[$template]);
		}else if (trim($template) && in_array($template, $remainingTemplates)){ // If it\'s text, and not empty, assume it\'s a template name
			unset($remainingTemplates[array_search($template, $remainingTemplates)]);			
		}
	}
}

$output = array();
// filter out documents which shouldn\'t be included
foreach ($docs as $doc){
	//by template, excludeTV, published, searchable
	if (isset($remainingTemplates[$doc[\'template\']]) && !$doc[$excludeTV] && $doc[$changefreq] != \'exclude\' && $doc[\'published\'] && $doc[\'template\'] != 0 && $doc[\'searchable\']){
		//exclude weblinks
		if (!$excludeWeblinks || ($excludeWeblinks && $doc[\'type\'] != \'reference\')){
			$output[] = $doc;
		}
	}
}
$docs = $output;
unset ($output, $allTemplates, $excludeTemplates);


# build sitemap in specified format
# ---------------------------------------------

switch ($format){
	// Next case added in version 1.0.4
	case \'ulli\': // UL List
		$output .= "<ul class=\\"sitemap\\">\\n";
		// TODO: Sort the array on Menu Index
		// TODO: Make a nested ul-li based on the levels in the document tree.
		foreach ($docs as $doc){
			$s  = "  <li class=\\"sitemap\\">";
			$s .= "<a href=\\"".($doc[\'id\'] != $modx->config[\'site_start\']) ? \'[(site_url)][~\'.$doc[\'id\'].\'~]\' : \'[(site_url)]\'."\\" class=\\"sitemap\\">" . $doc[\'pagetitle\'] . "</a>";
			$s .= "</li>\\n";
			$output .= $s;
		}
		
		$output .= "</ul>\\n";
	break;
		
	case \'txt\': // plain text list of URLs

		foreach ($docs as $doc){
			$url = ($doc[\'id\'] != $modx->config[\'site_start\']) ? \'[(site_url)][~\'.$doc[\'id\'].\'~]\' : \'[(site_url)]\';

			$output .= $url."\\n";
		}
		
	break;

	case \'ror\': // TODO
	default: // Sitemap Protocol
		$output = \'<?xml version="1.0" encoding="UTF-8"?>\'."\\n";
		if ($xsl != \'\'){
			$output .=\'<?xml-stylesheet type="text/xsl" href="\'.$xsl.\'"?>\'."\\n";
		}
		$output .=\'<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\'."\\n";
		
		
		foreach ($docs as $doc)	{
			$url = ($doc[\'id\'] != $modx->config[\'site_start\']) ? \'[(site_url)][~\'.$doc[\'id\'].\'~]\' : \'[(site_url)]\';
			$date = $doc[\'editedon\'];
			$date = date("Y-m-d", $date);
			$docPriority = ($doc[$priority]) ? $doc[$priority] : 0; // false if TV doesn\'t exist
			$docChangefreq = ($doc[$changefreq]) ? $doc[$changefreq] : 0; // false if TV doesn\'t exist
	
			$output .= "\\t".\'<url>\'."\\n";
			$output .= "\\t\\t".\'<loc>\'.$url.\'</loc>\'."\\n";
			$output .= "\\t\\t".\'<lastmod>\'.$date.\'</lastmod>\'."\\n";
			$output .= ($docPriority) ? ("\\t\\t".\'<priority>\'.$docPriority.\'</priority>\'."\\n") : \'\'; // don\'t output anything if TV doesn\'t exist
			$output .= ($docChangefreq) ? ("\\t\\t".\'<changefreq>\'.$docChangefreq.\'</changefreq>\'."\\n") : \'\'; // don\'t output anything if TV doesn\'t exist
			$output .= "\\t".\'</url>\'."\\n";
		}
		
		$output .= \'</urlset>\';

}

return $output;

# functions
# ---------------------------------------------

# gets (inherited) value of templat e variable
//TODO: wtf? In MODx 0.9.2.1 O_o Is this actually?
function getTV($modx, $docid, $doctv){
/* apparently in 0.9.2.1 the getTemplateVarOutput function doesn\'t work as expected and doesn\'t return INHERITED value; this is probably to be fixed for next release; see http://modxcms.com/bugs/task/464
	$output = $modx->getTemplateVarOutput($tv,$docid);
	return $output[$tv];
*/
	
	while ($pid = $modx->getDocument($docid, \'parent\')){
		$tv = $modx->getTemplateVar($doctv,\'*\',$docid);
		if (($tv[\'value\'] && substr($tv[\'value\'],0,8) != \'@INHERIT\') or !$tv[\'value\']){ // tv default value is overriden (including empty)
			$output = $tv[\'value\'];
			break;
		}else{ // there is no parent with default value overriden 
			$output = trim(substr($tv[\'value\'],8));
		}
		
		// move up one document in document tree
		$docid = $pid[\'parent\'];
	}
	
	return $output;
}

# gets list of published documents with properties
function getDocs($modx, $startid, $priority, $changefreq, $excludeTV, $seeThruUnpub){
	//If need to see through unpublished
	if ($seeThruUnpub){
		//Get all children documents, filter later
		$docs = $modx->getAllChildren($startid, \'menuindex\', \'asc\', \'id,editedon,template,published,searchable,pagetitle,type\');
	}else{
		//Get only published children documents
		$docs = $modx->getActiveChildren($startid, \'menuindex\', \'asc\', \'id,editedon,template,published,searchable,pagetitle,type\');
	} 

	// add sub-children to the list
	foreach ($docs as $key => $doc){
		$id = $doc[\'id\'];
		
		$docs[$key][$priority] = getTV($modx, $id, $priority); // add priority property
		$docs[$key][$changefreq] = getTV($modx, $id, $changefreq); // add changefreq property
		$docs[$key][$excludeTV] = getTV($modx, $id, $excludeTV); // add excludeTV property
		
		//TODO: $modx->getAllChildren & $modx->getActiveChildren always return the array
// 		if ($modx->getAllChildren($id)){
			$docs = array_merge($docs, getDocs($modx, $id, $priority, $changefreq, $excludeTV, $seeThruUnpub));
// 		}

	}
	return $docs;
}';
$s['servicevar'] = '
	if (empty($_GET[\'service\'])) 
	return "Продвижение";
	else
		return $_GET[\'service\'];
';
$s['easyForm'] = '//<?php
/**
 * easyForm
 * 
 * display easyForm on front
 * 
 * @author	    webber (web-ber12@yandex.ru)
 * @category	snippet
 * @version 	0.2
 * @license 	http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @internal	@guid easyForm
 * @internal	@modx_category Forms
 * @internal    @installset base, sample
 */
 
// пример совмещения с evoBabel для мультиязычности
// $eF->lang=$_SESSION[\'perevod\']; перед строкой $out=$eF->Run();
// не забудьте создать соответствующие переводы в модуля управления переводами

 
$out=\'\';
$eid=str_replace(\'f\',\'\',$params[\'formid\']);

if((int)$eid!=0){
	include_once(MODX_BASE_PATH."assets/snippets/easyForm/easyForm.class.php");
	$eF=new easyForm($modx,$params,$eid);
	$out=$eF->Run();
}
else{
	$out.=\'<p>Неверно задан id формы. Проверьте параметр &formid</p>\';
}

return $out;
';
$s['eFormPHxEvent'] = '
/*http://community.modx-cms.ru/blog/tips_and_tricks/1563.html*/
if (!class_exists(\'CChunkie\')) 
include_once (MODX_BASE_PATH.\'assets/snippets/jot/includes/chunkie.class.inc.php\'); 

function BeforeMailSent(&$fields) { 
global $modx; 

$phxOutput = new CChunkie($modx->getChunk(\'tplReportPhx\')); 
foreach ($fields as $field=>$value) { 
$phxOutput->AddVar($field, $value); 
} 
$fields[\'output\'] = $phxOutput->Render(); 
	

}
';
$s['test'] = '
function pre($data) {
echo \'<pre>\';
print_r ($data);
echo \'</pre>\';
}	
	

	pre($_SERVER);
';
$s['zakazWrite_iframe'] = '
//этот сниппет для тех форм, которые размещены на странице, а не в айфрейме
function GetRealIp(){
	if (!empty($_SERVER[\'HTTP_CLIENT_IP\'])) {
		$ip=$_SERVER[\'HTTP_CLIENT_IP\'];
	}
	elseif (!empty($_SERVER[\'HTTP_X_FORWARDED_FOR\'])){
		$ip=$_SERVER[\'HTTP_X_FORWARDED_FOR\'];
	}
	else{
		$ip=$_SERVER[\'REMOTE_ADDR\'];
	}
	return $ip;
}

function zakazWrite($fields){
	    global $modx;
	$dbname = $modx->db->config[\'dbase\']; //имя базы данных
	$zakazy = $modx->getFullTableName( \'zakazy\' ); //таблица с вопросами
	
	$fields = array( //составляем массив для записи в базу
	"date" => time(),
	"name" => htmlspecialchars($_POST[\'name\'], ENT_QUOTES),
	"email" => htmlspecialchars($_POST[\'email\'], ENT_QUOTES),
	"message" => htmlspecialchars($_POST[\'comment\'], ENT_QUOTES),
	"tel" => htmlspecialchars($_POST[\'telephone\'], ENT_QUOTES),
	"timecall" => htmlspecialchars($_POST[\'time_call\'], ENT_QUOTES),
	"region" => htmlspecialchars($_POST[\'region\'], ENT_QUOTES),
	"service" => htmlspecialchars($_POST[\'service\'], ENT_QUOTES),
	"site" => htmlspecialchars($_POST[\'url\'], ENT_QUOTES),
	"ip" => GetRealIp()
     );
	
	$query = $modx->db->insert($fields, $zakazy, ""); //запись 
	
	//вывод сообщения пользователю после отправки формы
	echo \' 
			<script type="text/javascript">
jQuery(document).ready(function() {
   $.fancybox.open(\\\'<p style="text-align:center">Спасибо, что обратились к нам. Наши менеджеры скоро свяжутся с Вами!</p>\\\');
});
                </script>
\';
	
	
include_once "manager/includes/controls/class.phpmailer.php"; //подключаем phpmailer
function php_mail($attach, $areas) { //функция для отправки данных из форм во вложении к письму
        global $modx;
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(false); //тип письма 
        $mail->From      = "sales@top3seo.ru"; //от кого (адрес)
        $mail->FromName  = "Top3Seo"; //от кого (Имя)
        $mail->Subject   = "To CRM"; //тема письма
		$mail->Body      = " "; // содержимое письма
        $mail->AddAddress("sitereq@top3seo.ru"); //кому
		$mail->AddBCC("ndmitrievich@upsales.ru", $name = "Никита Дмитриевич"); // скрытая копия
	$mail->AddBCC("vbayda@andata.ru", $name = "Василий Байда"); // скрытая копия
		$mail->AddStringAttachment($attach, "report.xml", $encoding = \'base64\', $type = \'application/octet-stream\'); //создаём вложение
        $mail->Send(); //отправка
}

	
	
	 //создаём содержимое xml файла
if ($_POST[\'ids\'] != 1) {
	$msg = ($_POST[\'message\']) ? $_POST[\'message\'] : $_POST[\'comment\'];
	$bonus = ($_POST[\'bonus\']) ? $_POST[\'bonus\'].\';\' : \'\';
	$subj = ($_POST[\'subject\']) ? \'Тема:\' . $_POST[\'subject\'] . \'.\' : \'\';
	$mesg = ($msg) ? \'Сообщение:\' . $bonus . $msg . \'.\' : \'\';
	$mail = ($_POST[\'email\']) ? \'Эл.адрес:\' . $_POST[\'email\'] . \'.\' : \'\';
	$tel = ($_POST[\'telephone\']) ? \'Тел:\' . $_POST[\'telephone\'] . \'.\' : \'\';
	
$row[0] = "<?xml version=\'1.0\' encoding=\'UTF-8\' ?>
<Document from=\'top3seo.ru\'>
       <Action EntityLogicalName=\'contact\' Message=\'find\'>
              <RulesCollection>
                     <Rule SchemaName=\'statuscode\' Value=\'1\'/>
                     <Rule SchemaName=\'emailaddress1\' Value=\'{$_POST[\'email\']}\'/>
              </RulesCollection>
              <CRMFieldsCollection>
                     <CRMField Sitefield=\'Email\' Type=\'String\'>
                            <Value>{$_POST[\'email\']}</Value>
                            <SchemaName>emailaddress1</SchemaName>
                     </CRMField>
                     <CRMField Sitefield=\'Name\' Type=\'String\'>
                            <Value>{$_POST[\'name\']}</Value>
                            <SchemaName>lastname</SchemaName>
                     </CRMField>
                     <CRMField Sitefield=\'Telephone\' Type=\'String\'>
                            <Value>{$_POST[\'telephone\']}</Value>
                            <SchemaName>mobilephone</SchemaName>
                     </CRMField>
              </CRMFieldsCollection>
              <Action EntityLogicalName=\'opportunity\' Message=\'create\' RelationField=\'customerid\'>
                     <CRMFieldsCollection>
                            <CRMField Sitefield=\'FormID+FormTitle\' Type=\'String\'>
                                   <Value>SEO: запрос с сайта на {$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                                   <SchemaName>name</SchemaName>
                            </CRMField>
                            <CRMField Sitefield=\'Url+Subject+Message+Email+Telephone\' Type=\'String\'>
                                   <Value>{$_POST[\'url\']} {$subj} {$mesg} {$mail} {$tel}</Value>
                                   <SchemaName>description</SchemaName>
                            </CRMField>
                     </CRMFieldsCollection>
              </Action>
       </Action>
</Document>
";
	}
else { 
$row[0] = "<?xml version=\'1.0\' encoding=\'UTF-8\' ?>
<Document from=\'top3seo.ru\'>
       <Action EntityLogicalName=\'lead\' Message=\'create\'>
              <CRMFieldsCollection>
                     <CRMField sitefield=\'Telephone\' Type=\'String\'>
                            <Value>{$_POST[\'telephone\']}</Value>
                            <SchemaName>telephone1</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'Name\' Type=\'String\'>
                            <Value>{$_POST[\'name\']}</Value>
                            <SchemaName>firstname</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'top3seo.ru+FormID+FormTitle\' Type=\'String\'>
                            <Value>top3seo.ru-{$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                            <SchemaName>companyname</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                            <Value>SEO: запрос с сайта на звонок на №: {$_POST[\'telephone\']}, предпочт.время: {$_POST[\'time_call\']}, на имя: {$_POST[\'name\']}. Блок услуг: {$_POST[\'service\']}</Value>
                            <SchemaName>subject</SchemaName>
                     </CRMField>
                     <CRMField sitefield=\'FormID+FormTitle\' Type=\'String\'>
                            <Value>{$_POST[\'ids\']}-{$_POST[\'service\']}</Value>
                            <SchemaName>lastname</SchemaName>
                     </CRMField>
              </CRMFieldsCollection>          
              <Action EntityLogicalName=\'phonecall\' Message=\'create\' RelationField=\'regardingobjectid\'>
                     <CRMFieldsCollection>
                            <CRMField sitefield=\'Telephone\' Type=\'String\'>
                                   <Value>{$_POST[\'telephone\']}</Value>
                                   <SchemaName>phonenumber</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'FormID\' Type=\'String\'>
                                   <Value>{$_POST[\'ids\']}</Value>
                                   <SchemaName>category</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                                   <Value>SEO: {$_POST[\'telephone\']}; {$_POST[\'time_call\']}; {$_POST[\'name\']}; {$_POST[\'service\']}</Value>
                                   <SchemaName>subject</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'Telephone+TimeCall+Name+FormTitle\' Type=\'String\'>
                                   <Value>запрос с сайта на звонок на №: {$_POST[\'telephone\']}, предпочт.время: {$_POST[\'time_call\']}, на имя: {$_POST[\'name\']}. Блок услуг: {$_POST[\'service\']}</Value>
                                   <SchemaName>description</SchemaName>
                            </CRMField>
                            <CRMField sitefield=\'none\' Type=\'DateTime\'>
                                   <Value>".gmdate("m/d/Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m")  , date("d"), date("Y")))."</Value>
                                   <SchemaName>scheduledend</SchemaName>
                            </CRMField>
                     </CRMFieldsCollection>
              </Action>
       </Action>
</Document>";
}
	

$xml_file = implode("\\n",$row); //преобразуем массив блоков в строку
	 
	 php_mail ($xml_file, $areas); //вызов самой функции

	//Отправка репорта клиенту 	
	
function noreply($email, $body, $subject) { //функция для отправки репорта клиенту
        global $modx;
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(true); //тип письма 
        $mail->From      = "noreply@top3seo.ru"; //от кого (адрес)
        $mail->FromName  = "Автоматическое сообщение"; //от кого (Имя)
        $mail->Subject   = $subject; //тема письма
		$mail->Body      = $body; // содержимое письма
        $mail->AddAddress($email); //кому
        $mail->Send(); //отправка
}

/*****
*Формируем json запрос в CRM и отаравляем его.
******/
$dataCRM = array(\'Name\'=>$_POST[\'name\'],
	\'Description\'=> \'запрос с сайта на звонок на №: \'.$_POST[\'telephone\'].\', предпочт.время: \'.$_POST[\'time_call\'].\', на имя: \'.$_POST[\'name\'].\'. Блок услуг: \'.$_POST[\'service\'],
	\'Contact\'=>array(\'EmailAddress1\'=>$_POST[\'email\'],
		\'LastName\'=>$_POST[\'ids\'].\'-\'.$_POST[\'service\'],
		\'MobilePhone\'=>$_POST[\'telephone\']
	)
);
$dataCRM_json = json_encode($dataCRM);
$url = \'http://crm.andata.ru:33334/api/PhoneCall\';
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(\'Content-Type: application/json\'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $dataCRM_json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_exec($ch);	
/*****
*END Формируем json запрос в CRM и отаравляем его.
******/

function getdata() { //выбираем только те, которые были отправлены
$postdata[] = (isset($_POST[\'subject\'])) ? \'<tr valign="top"><td>Тема сообщения: </td><td>\'.$_POST[\'subject\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'name\'])) ? \'<tr valign="top"><td>Имя: </td><td>\'.$_POST[\'name\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'email\'])) ? \'<tr valign="top"><td>Email: </td><td>\'.$_POST[\'email\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'url\'])) ? \'<tr valign="top"><td>Адрес сайта: </td><td>\'.$_POST[\'url\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'telephone\'])) ? \'<tr valign="top"><td>Телефон: </td><td>\'.$_POST[\'telephone\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'time_call\'])) ? \'<tr valign="top"><td>Время звонка: </td><td>\'.$_POST[\'time_call\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'bonus\'])) ? \'<tr valign="top"><td>Выбранный подарок </td><td>\'.$_POST[\'bonus\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'comment\'])) ? \'<tr valign="top"><td>Сообщение: </td><td></td></tr><tr valign="top"><td colspan="2">\'.$_POST[\'comment\'].\'</td></tr>\' : \'\';
$postdata[] = (isset($_POST[\'message\'])) ? \'<tr valign="top"><td>Сообщение: </td><td></td></tr><tr valign="top"><td colspan="2">\'.$_POST[\'message\'].\'</td></tr>\' : \'\';

$postdatastr = implode("\\n",$postdata); //преобразуем массив блоков в строку
return $postdatastr;
}
	
	
$noreplyTpls = array( //массив с названиями чанков шаблонов репортов и их тем id=>назание+тема
	1 => array(
		"name"=>"noreplyOrderCall",
		"subject"=>"Заявка на звонок"
		),
	6 => array(
		"name"=>"noreplyFeedbackForm",
		"subject"=>"Обратная связь «".$_POST[\'subject\']."»"
		),
	57 => array(
		"name"=>"noreplyHowSeo",
		"subject"=>"Заявка на возможность продвижения сайта ".$_POST[\'url\']
		),
	60 => array(
		"name"=>"noreplyOrderAudit",
		"subject"=>"Заявка на экспресс-аудит сайта ".$_POST[\'url\']
		),
	61 => array(
		"name"=>"noreplyOrderSeo",
		"subject"=>"Заявка на продвижение сайта"
		),
	86 => array(
		"name"=>"noreplyServicesSeo",
		"subject"=>"Заявка на продвижение сайта ".$_POST[\'url\']
		),
	87 => array(
		"name"=>"noreplyServicesAudit",
		"subject"=>"Заявка на аудит сайта ".$_POST[\'url\']
		),
	88 => array(
		"name"=>"noreplyServicesKontekst",
		"subject"=>"Заявка на контекстную рекламу"
		),
	89 => array(
		"name"=>"noreplyServicesText",
		"subject"=>"Заявка на ".$_POST[\'service\']
		),
	108 => array(
		"name"=>"noreplyServicesKontekstAudit",
		"subject"=>"Заявка на аудит рекламной компании"
		)
);
	
$body = $modx->parseChunk($noreplyTpls[$_POST[\'ids\']][\'name\'], array( //парсим чанк , заменяя плейсхолдеры и делаем его телом письма
		\'name\' => $_POST[\'name\'],
		\'url\' => $_POST[\'url\'],
		\'output\' => getdata(),
		\'subject\' => $_POST[\'subject\'],
		\'service\' => $_POST[\'service\']
	),
	\'[+\',
	\'+]\'
);

$email = $_POST[\'email\'];
$subject = $noreplyTpls[$_POST[\'ids\']][\'subject\'];

noreply ($email, $body, $subject); //вызов самой функции
}
';
$s['rds'] = '
	/*
include_once(MODX_BASE_PATH.\'assets/modules/audit/parser/simple_html_dom.php\');

$html = file_get_html(\'http://hrscan.ru/\');



foreach($html->find(\'a\') as $element) {
echo $element->href .\' (\'. $element->innertext. \')<br>\';
}
*/
';
$s['Metrika'] = '
/**

Для вызова:
///[!Metrika?!]

Параметры:
///&tpl - Шаблон (имя чанка) ОБЯЗАТЕЛЕН
///&delimiter - Разделять ли результат на группы (1 или 0, по дефолту 0)
///&separator - Разделитель групп (любой символ, по дефолту пробел)


/Пример вызова:
///[!Metrika? &tpl=`чанк` &delimiter=`1` &separator=`.`!]

/плейсхолдеры для шаблона:
///[+start+] - дата начала периода
///[+finish+] - конец периода
///[+visits+] - визиты
///[+visitors+] - посетители
///[+newvisitors+] - новые посетители
///[+links+] - по ссылкам
///[+advertising+] - по рекламе
///[+search+] - органика
///[+request+] - прямые переходы
///[+social+] - из соц сетей

**/

	
$tpl = ($tpl) ? $tpl : \'\';
$delimiter = ($delimiter == 1) ? $delimiter : "0";
$separator = ($separator) ? $separator : " ";




if (!empty($tpl)) {
	
	$counters = $modx->getFullTableName( \'module_metrika_counters\' );
	$settings = $modx->getFullTableName( \'module_metrika_settings\' );
	
	$visits_total = 0;
	$visitors_total = 0;
	$new_visitors_total = 0;
	$referrals_total = 0;
	$advertising_total = 0;
	$searchengines_total = 0;
	$request_total = 0;
	$social_total = 0;

	$select_counters = $modx->db->select("*", $counters, "active = \'1\'");
	while ($value = mysql_fetch_array($select_counters)){//выводим записи

		$json = (array) json_decode($value[\'stats_total\'], true);
	
		$visits_total = $visits_total + $json[\'visits\'];
		$visitors_total = $visitors_total + $json[\'visitors\'];
		$new_visitors_total = $new_visitors_total + $json[\'new_visitors\'];
		$referrals_total = $referrals_total + $json[\'referrals\'];
		$advertising_total = $advertising_total + $json[\'advertising\'];
		$searchengines_total = $searchengines_total + $json[\'searchengines\'];
		$request_total = $request_total + $json[\'request\'];
		$social_total = $social_total + $json[\'social\'];
	
		$counters_arr[] = $value;
	
		$period = (array) json_decode($value[\'period\'], true);
	}

	if (empty($settings_data)) {
		$select_settings = $modx->db->select("*", $settings);
		while ($setting = mysql_fetch_array($select_settings)){//выводим записи
			$settings_data[$setting[\'name\']] = $setting[\'value\'];
		}
	}
	
	if ($delimiter == "0") {
		$parse = $modx->parseChunk($tpl, array(
			\'visits\' => $visits_total,
			\'visitors\' => $visitors_total,
			\'newvisitors\' => $new_visitors_total,
			\'links\' => $referrals_total,
			\'advertising\' => $advertising_total,
			\'search\' => $searchengines_total,
			\'request\' => $request_total,
			\'social\' => $social_total,
			\'start\' => date("d.m.Y",$period[\'start\']),
			\'finish\' => date("d.m.Y H:i",$period[\'finish\'])
			),
			\'[+\',
			\'+]\'
		);
	}
	else {
		$parse = $modx->parseChunk($tpl, array(
			\'visits\' => number_format($visits_total, 0, \'\', $separator),
			\'visitors\' => number_format($visitors_total, 0, \'\', $separator),
			\'newvisitors\' => number_format($new_visitors_total, 0, \'\', $separator),
			\'links\' => number_format($referrals_total, 0, \'\', \' \'),
			\'advertising\' => number_format($advertising_total, 0, \'\', $separator),
			\'search\' => number_format($searchengines_total, 0, \'\', $separator),
			\'request\' => number_format($request_total, 0, \'\', $separator),
			\'social\' => number_format($social_total, 0, \'\', $separator),
			\'start\' => date("d.m.Y",$period[\'start\']),
			\'finish\' => date("d.m.Y H:i",$period[\'finish\'])
			),
			\'[+\',
			\'+]\'
		);
		
	}
	
	echo $parse;
	
}
else {
	echo \'Не указан шаблон вывода!\';	
}
';
$s['getClients'] = '
$content = $modx->getFullTableName( \'site_content\' );
	

	echo \'Выбрать...==0\';
	
	$select_clients = $modx->db->select("*", $content, "parent = \'43\' OR parent = \'44\' AND published= \'1\' AND deleted= \'0\'");
	while ($client = mysql_fetch_array($select_clients)){
		
		echo \'||\'.$client[\'pagetitle\'].\'==\'.$client[\'id\'];
		
	}


	echo \'||Другой==other\';
';
$p = &$this->pluginCache;
$p['CodeMirror'] = '$_CM_BASE = \'assets/plugins/codemirror/\';

$_CM_URL = $modx->config[\'site_url\'] . $_CM_BASE;

require(MODX_BASE_PATH. $_CM_BASE .\'codemirror.plugin.php\');

';
$p['CodeMirrorProps'] = '&theme=Theme;list;default,ambiance,blackboard,cobalt,eclipse,elegant,erlang-dark,lesser-dark,midnight,monokai,neat,night,rubyblue,solarized,twilight,vibrant-ink,xq-dark,xq-light; &indentUnit=Indent unit;int;4 &tabSize=The width of a tab character;int;4 &lineWrapping=lineWrapping;list;true,false;true &matchBrackets=matchBrackets;list;true,false;false &activeLine=activeLine;list;true,false;false &emmet=emmet;list;true,false;true &search=search;list;true,false;true ';
$p['FileSource'] = 'require MODX_BASE_PATH.\'assets/plugins/filesource/plugin.filesource.php\';';
$p['Forgot Manager Login'] = 'require MODX_BASE_PATH.\'assets/plugins/forgotmanagerlogin/plugin.forgotmanagerlogin.php\';';
$p['ManagerManager'] = '$js_default_url_local = $modx->config[\'site_url\']. \'/assets/js/jquery.min.js\';
$js_default_url_remote = \'http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js\';
$asset_path = $modx->config[\'base_path\'] . \'assets/plugins/managermanager/mm.inc.php\';
include($asset_path);
';
$p['ManagerManagerProps'] = '&config_chunk=Configuration Chunk;text;mm_rules; &remove_deprecated_tv_types_pref=Remove deprecated TV types;list;yes,no;yes &which_jquery=jQuery source;list;local (assets/js),remote (google code),manual url (specify below);local (assets/js) &js_src_type=jQuery URL override;text; ';
$p['TinyMCE Rich Text Editor'] = 'require MODX_BASE_PATH.\'assets/plugins/tinymce/plugin.tinymce.php\';
';
$p['TinyMCE Rich Text EditorProps'] = '&customparams=Custom Parameters;textarea;valid_elements : "*[*]", &mce_formats=Block Formats;text;p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre &entity_encoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &mce_path_options=Path Options;list;Site config,Absolute path,Root relative,URL,No convert;Site config &mce_resizing=Advanced Resizing;list;true,false;true &disabledButtons=Disabled Buttons;text; &link_list=Link List;list;enabled,disabled;enabled &webtheme=Web Theme;list;simple,editor,creative,custom;simple &webPlugins=Web Plugins;text;style,advimage,advlink,searchreplace,contextmenu,paste,fullscreen,xhtmlxtras,media &webButtons1=Web Buttons 1;text;undo,redo,selectall,|,pastetext,pasteword,|,search,replace,|,hr,charmap,|,image,link,unlink,anchor,media,|,cleanup,removeformat,|,fullscreen,code,help &webButtons2=Web Buttons 2;text;bold,italic,underline,strikethrough,sub,sup,|,|,blockquote,bullist,numlist,outdent,indent,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,|,styleprops &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr &width=Width;text;95% &height=Height;text;500 ';
$p['TransAlias'] = 'require MODX_BASE_PATH.\'assets/plugins/transalias/plugin.transalias.php\';';
$p['TransAliasProps'] = '&table_name=Trans table;list;common,russian,dutch,german,czech,utf8,utf8lowercase;russian &char_restrict=Restrict alias to;list;lowercase alphanumeric,alphanumeric,legal characters;legal characters &remove_periods=Remove Periods;list;Yes,No;Yes &word_separator=Word Separator;list;dash,underscore,none;dash &override_tv=Override TV name;string; ';
$p['PHx'] = '//<?php
/**
 * phx 
 * 
 * (Placeholders Xtended) Adds the capability of output modifiers when using placeholders, template variables and settings tags
 *
 * @category    plugin
 * @version     2.2.0
 * @author      Armand "bS" Pondman (apondman@zerobarrier.nl)
 * @internal    @properties &phxdebug=Log events;int;0 &phxmaxpass=Max. Passes;int;50
 * @internal    @events OnParseDocument
 * @internal    @modx_category Manager and Admin
 */

include_once $modx->config[\'rb_base_dir\'] . "plugins/phx/phx.parser.class.inc.php";

$e = &$modx->Event;

$PHx = new PHxParser($phxdebug,$phxmaxpass);

switch($e->name) {
	case \'OnParseDocument\':
		$PHx->OnParseDocument();
		break;

}';
$p['TVimageResizer'] = 'require_once("../assets/plugins/tvimageresizer/TVimageResizer.inc.php");';
$p['TVimageResizerProps'] = '&tv_ids=TV IDs;string;8 &dirs=Thumb folders;string;small~medium~large &width=Width;string;70~220~ &height=Height;string;70~175~ &rcorner=Corners percentage of clipping;string; &backgroundColor=Background color;string;#F8F8F8 &watermark=Watermark image path (png);string; &watermarkPos=Watermark position;string;90% 90% &cprighttext=Copyright text;string; &quality=Quality;int;90 &mirror=Mirror effect;list;yes,no;no &crop=Cropping;list;yes,no,crop_resized,fill_resized;fill_resized &save_o_name=Save only name;list;yes,no;no &rename_images=Rename images;list;yes,no;no &refresh_all_images=Refresh all images;list;yes,no;no ';
$p['ajaxSubmit'] = '
/**
 * ajaxSubmit
 *
 * Ajax sending of any form
 *
 * @category    plugin
 * @version     1.0
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     modx
 * @author      Andchir
 * @internal    @properties &post_signal=Post signal name;string;ajax_submit &check_referer=Check referer;list;yes,no;yes
 * @internal    @events OnLoadWebDocument,OnLoadWebPageCache
 * @internal    @modx_category Forms
 */

if(!isset($post_signal)) $post_signal = \'ajax_submit\';
if(!isset($check_referer)) $check_referer = \'yes\';

$e = $modx->Event;

if ($e->name == \'OnLoadWebDocument\' || $e->name == \'OnLoadWebPageCache\'){
  
  $output = \'\';
  
  $referer_valid = $check_referer==\'yes\' ? in_array(strpos($_SERVER[\'HTTP_REFERER\'],$_SERVER[\'HTTP_HOST\']),array(7,8)) : true;
  
  if(isset($_SERVER[\'HTTP_X_REQUESTED_WITH\']) && strtolower($_SERVER[\'HTTP_X_REQUESTED_WITH\']) == \'xmlhttprequest\' && $referer_valid){
    
    if(isset($_POST[$post_signal])){
        
        class exDocumentParser extends DocumentParser {
          function sendRedirect() {
            return true;
          }
          function innerHTML($node){
            $doc = new DOMDocument();
            foreach ($node->childNodes as $child)
                $doc->appendChild($doc->importNode($child, true));
            return $doc->saveHTML();
          }
        }
        
        $xpath = $_POST[$post_signal];
        
        define(AS_PATH, MODX_BASE_PATH.\'assets/plugins/ajax_submit/\');
        define(AS_URL_PATH, MODX_BASE_URL.\'assets/plugins/ajax_submit/\');
        
        $parser = new exDocumentParser;
        $parser->db->connect();
        $parser->getSettings();
        $parser->config = $modx->config;
        $parser->documentObject = $modx->documentObject;
        $parser->documentIdentifier = $modx->documentIdentifier;
        $parser->aliasListing = $modx->aliasListing;
        $parser->snippetCache = $modx->snippetCache;
        $parser->chunkCache = $modx->chunkCache;
        $parser->documentListing = $modx->documentListing;
        $parser->documentMap = $modx->documentMap;
        
        $html = $modx->documentContent;
        $html = $parser->mergeChunkContent($html);
        $html = $parser->mergeDocumentContent($html);
        $html = $parser->mergeSettingsContent($html);
        $html = $parser->parseDocumentSource($html);
        if(strpos($html, \'[!\') > -1){
            $html = str_replace(array(\'[!\',\'!]\'),array(\'[[\',\']]\'),$html);
            $html = $parser->parseDocumentSource($html);
        }
        $html = $parser->rewriteUrls($html);
        
        if($modx->config[\'modx_charset\']=="UTF-8"){
            $html = mb_convert_encoding($html, \'HTML-ENTITIES\', \'utf-8\');
        }
        
        //search snippet content in html
        require_once AS_PATH.\'Dom/Query.php\';
        $dom = new Zend_Dom_Query($html);
        $results = $dom->query($xpath);
        if (count($results) > 0){
            $output = $parser->innerHTML($results->current());
            $output = trim($output);
        }
        
        if(!$output) $output = \'success\';
        
        echo $output;
        exit;
        
    }
    
  }
  
}
';
$p['ajaxSubmitProps'] = '&post_signal=Post signal name;string;ajax_submit &check_referer=Check referer;list;yes,no;yes ';
$p['SEO Strict URLs'] = '//<?php
// Strict URLs
// version 1.0.1
// Enforces the use of strict URLs to prevent duplicate content.
// By Jeremy Luebke @ www.xuru.com
// Contributions by Brian Stanback @ www.stanback.net

// On Install: Check the "OnWebPageInit" & "OnWebPagePrerender" boxes in the System Events tab.
// Plugin configuration: &editDocLinks=Edit document links;int;1 &makeFolders=Rewrite containers as folders;int;1 &emptyFolders=Check for empty container when rewriting;int;0 &override=Enable manual overrides;int;0 &overrideTV=Override TV name;string;seoOverride

// For overriding documents, create a new template variabe (TV) named seoOverride with the following options:
//    Input Type: DropDown List Menu
//    Input Option Values: Disabled==-1||Base Name==0||Append Extension==1||Folder==2
//    Default Value: -1

//  # Include the following in your .htaccess file
//  # Replace "example.com" &  "example\\.com" with your domain info
//  RewriteCond %{HTTP_HOST} .
//  RewriteCond %{HTTP_HOST} !^www\\.example\\.com [NC]
//  RewriteRule (.*) http://www.example.com/$1 [R=301,L] 

// Begin plugin code
$e = &$modx->event;

if ($e->name == \'OnWebPageInit\') 
{
   $documentIdentifier = $modx->documentIdentifier;

   if ($documentIdentifier)  // Check for 404 error
   {
      $myProtocol = ($_SERVER[\'HTTPS\'] == \'on\') ? \'https\' : \'http\';
      $s = $_SERVER[\'REQUEST_URI\'];
      $parts = explode("?", $s);  

      $alias = $modx->aliasListing[$documentIdentifier][\'alias\'];
      if ($makeFolders)
      {
         if ($emptyFolders)
         {
            $result = $modx->db->select(\'isfolder\', $modx->getFullTableName(\'site_content\'), \'id = \' . $documentIdentifier);
            $isfolder = $modx->db->getValue($result);
         }
         else
         {
            $isfolder = (count($modx->getChildIds($documentIdentifier, 1)) > 0) ? 1 : 0;
         }
      }

      if ($override && $overrideOption = $modx->getTemplateVarOutput($overrideTV, $documentIdentifier))
      {
         switch ($overrideOption[$overrideTV])
         {
            case 0:
               $isoverride = 1;
               break;
            case 1:
               $isfolder = 0;
               break;
            case 2:
               $makeFolders = 1;
               $isfolder = 1;
         }
      }

      if ($isoverride)
      {
         $strictURL = preg_replace(\'/[^\\/]+$/\', $alias, $modx->makeUrl($documentIdentifier));
      }
      elseif ($isfolder && $makeFolders)
      {
         $strictURL = preg_replace(\'/[^\\/]+$/\', $alias, $modx->makeUrl($documentIdentifier)) . "/";
      }
      else
      {
         $strictURL = $modx->makeUrl($documentIdentifier);
      }

      $myDomain = $myProtocol . "://" . $_SERVER[\'HTTP_HOST\'];
      $newURL = $myDomain . $strictURL;
      $requestedURL = $myDomain . $parts[0];

      if ($documentIdentifier == $modx->config[\'site_start\'])
      {
         if ($requestedURL != $modx->config[\'site_url\'])
         {
            // Force redirect of site start
            header("HTTP/1.1 301 Moved Permanently");
            $qstring = preg_replace("#(^|&)(q|id)=[^&]+#", \'\', $parts[1]);  // Strip conflicting id/q from query string
            if ($qstring) header(\'Location: \' . $modx->config[\'site_url\'] . \'?\' . $qstring);
            else header(\'Location: \' . $modx->config[\'site_url\']);
            exit(0);
         }
      }
      elseif ($parts[0] != $strictURL)
      {
         // Force page redirect
         header("HTTP/1.1 301 Moved Permanently");
         $qstring = preg_replace("#(^|&)(q|id)=[^&]+#", \'\', $parts[1]);  // Strip conflicting id/q from query string
         if ($qstring) header(\'Location: \' . $strictURL . \'?\' . $qstring);
         else header(\'Location: \' . $strictURL);
         exit(0);
      }
   }
}
elseif ($e->name == \'OnWebPagePrerender\')
{
   if ($editDocLinks)
   {
      $myDomain = $_SERVER[\'HTTP_HOST\'];
      $furlSuffix = $modx->config[\'friendly_url_suffix\'];
      $baseUrl = $modx->config[\'base_url\'];
      $o = &$modx->documentOutput; // get a reference of the output

      // Reduce site start to base url
      $overrideAlias = $modx->aliasListing[$modx->config[\'site_start\']][\'alias\'];
      $overridePath = $modx->aliasListing[$modx->config[\'site_start\']][\'path\'];
      $o = preg_replace("#((href|action)=\\"|$myDomain)($baseUrl)?($overridePath/)?$overrideAlias$furlSuffix#", \'${1}\' . $baseUrl, $o);

      if ($override)
      {
         // Replace manual override links
         $sql = "SELECT tvc.contentid as id, tvc.value as value FROM " . $modx->getFullTableName(\'site_tmplvars\') . " tv ";
         $sql .= "INNER JOIN " . $modx->getFullTableName(\'site_tmplvar_templates\') . " tvtpl ON tvtpl.tmplvarid = tv.id ";
         $sql .= "LEFT JOIN " . $modx->getFullTableName(\'site_tmplvar_contentvalues\') . " tvc ON tvc.tmplvarid = tv.id ";
         $sql .= "LEFT JOIN " . $modx->getFullTableName(\'site_content\') . " sc ON sc.id = tvc.contentid ";
         $sql .= "WHERE sc.published = 1 AND tvtpl.templateid = sc.template AND tv.name = \'$overrideTV\'";
         $results = $modx->dbQuery($sql);
         while ($row = $modx->fetchRow($results))
         {
            $overrideAlias = $modx->aliasListing[$row[\'id\']][\'alias\'];
            $overridePath = $modx->aliasListing[$row[\'id\']][\'path\'];
            switch ($row[\'value\'])
            {
               case 0:
                  $o = preg_replace("#((href|action)=\\"($baseUrl)?($overridePath/)?|$myDomain$baseUrl$overridePath/?)$overrideAlias$furlSuffix#", \'${1}\' . $overrideAlias, $o);
                  break;
               case 2:
                  $o = preg_replace("#((href|action)=\\"($baseUrl)?($overridePath/)?|$myDomain$baseUrl$overridePath/?)$overrideAlias$furlSuffix/?#", \'${1}\' . rtrim($overrideAlias, \'/\') . \'/\', $o);
                  break;
            }
         }
      }

      if ($makeFolders)
      {
         if ($emptyFolders)
         {
            // Populate isfolder array
            $isfolder_arr = array();
            $result = $modx->db->select(\'id\', $modx->getFullTableName(\'site_content\'), \'published > 0 AND isfolder > 0\');
            while ($row = $modx->db->getRow($result))
               $isfolder_arr[$row[\'id\']] = true;
         }

         // Replace container links
         foreach ($modx->documentListing as $id)
         {
            if ((is_array($isfolder_arr) && isset($isfolder_arr[$id])) || count($modx->getChildIds($id, 1)))
            {
               $overrideAlias = $modx->aliasListing[$id][\'alias\'];
               $overridePath = $modx->aliasListing[$id][\'path\'];
               $o = preg_replace("#((href|action)=\\"($baseUrl)?($overridePath/)?|$myDomain$baseUrl$overridePath/?)$overrideAlias$furlSuffix/?#", \'${1}\' . rtrim($overrideAlias, \'/\') . \'/\', $o);
            }
         }
      }
   }
}';
$p['SEO Strict URLsProps'] = '&editDocLinks=Edit document links;int;1 &makeFolders=Rewrite containers as folders;int;0 &emptyFolders=Check for empty container when rewriting;int;0 &override=Enable manual overrides;int;1 &overrideTV=Override TV name;string;seoOverride ';
$p['DirectResize'] = 'define(DIRECTRESIZE_PATH, "assets/plugins/directresize/");
define(DIRECTRESIZE_GALLERYDIR, "assets/drgalleries/");
include_once $modx->config[\'base_path\'].DIRECTRESIZE_PATH."directResize.php";

global $content;
		
$e = &$modx->Event;
switch ($e->name) {
  case "OnBeforeDocFormSave":
  		$content = ConvertFromBackend($_POST[\'ta\']);
  		//if (isset($_POST[which_editor]) && $_POST[which_editor]!="none" && strlen($_POST[\'ta\'])>0) $content[\'content\']  = ConvertFromBackend($_POST[\'ta\'], false);
    break;
    
	case "OnWebPagePrerender":
			//$modx->documentObject[content] = RenderOnFrontend($modx->documentObject[content], $config);
			$modx->documentOutput = RenderOnFrontend($modx->documentOutput, $config);
	break;
		
	case "OnDocFormPrerender":
		// Плагин инициируется только в визуальном редакторе. Без редактора замены больших картинок на превью нет
		if (($modx->config[which_editor] != "none" && empty($_POST)) || (isset($_POST[which_editor]) && $_POST[which_editor]!="none"))
		{		
			$content[\'content\'] = RenderOnFrontend($content[\'content\'], $config);
		}
		else
		{
			if (strlen($_POST[\'ta\'])>0)  $content[\'content\']  = ConvertFromBackend($_POST[\'ta\'], false);
		}
	break;

	case "OnCacheUpdate":
			ClearDRCache($clearCache);
	break;	
	
	default :
		return;
	break;
}';
$p['DirectResizeProps'] = '&config=Конфигурация;string;fancy &clearCache=Очистка кеша;list;0,1,2;0 ';
$p['ymdw'] = '//<?php
/**
 * Yandex Metrika Dashboard Widget
 *
 * show Visitors, Visits and Pagevisits
 *
 * @category    plugin
 * @version     0.2
 * @license     http://www.gnu.org/copyleft/gpl.html GNU Public License (GPL)
 * @package     modx
 * @author      Dmi3yy (dmi3yy@gmail.com), Pathologic (maxx@np.by)
 * @internal    @events OnManagerWelcomePrerender
 * @internal    @modx_category Manager and Admin
 * @internal    @properties &app_id = Id приложения;text; &app_pass = Пароль приложения;text; &ym_login = Логин Яндекс;text; &ym_pass = Пароль Яндекс;text; &counter_id = Номер счетчика;text; &counter_range = Количество дней;text;10 &widget_height = Высота виджета;text;350 &rotate_xlabels = Поворот подписей оси X;text;-90
 * @internal    @installset base
 * @internal    @disabled 1
 */
 
$e = &$modx->Event;
if($e->name == \'OnManagerWelcomePrerender\'){	
	if(!file_exists(MODX_BASE_PATH . \'assets/cache/ymdw.widgetCache-\'.date(\'z\').\'.php\')){
		require (MODX_BASE_PATH.\'assets/plugins/ymdw/yandexapi.class.php\');
		$ym = new YandexAPI($app_id, $app_pass);
		$ym->LogIn($ym_login, $ym_pass);
		if ($ym->success) {
			$counter_range = empty($counter_range) ? 7 : $counter_range;
			$date2 = time();
			$date1 = $date2 - $counter_range*24*60*60;
			$date2 = date(\'Ymd\',$date2);
			$date1 = date(\'Ymd\',$date1);
			$ym->MakeQuery(\'/stat/traffic/summary\', array(\'id\'=>$counter_id,\'date1\'=>$date1,\'date2\'=>$date2));
			if ($ym->success) {
				$results = $ym->result;
				$visitors = $visits = $views = $dates = array();
				$results = array_reverse($results[\'data\']);
				foreach ($results as $result) {
					$dates[] = \'[\'.$i.\',"\'.substr($result[\'date\'],6,2).\'.\'.substr($result[\'date\'],4,2).\'.\'.substr($result[\'date\'],0,4).\'"]\';
					$visitors[] = \'[\'.$i.\',\'.$result[\'new_visitors\'].\']\';
					$visits[] = \'[\'.$i.\',\'.$result[\'visits\'].\']\';
					$views[] = \'[\'.$i.\',\'.$result[\'page_views\'].\']\';
					$i++;											 
				}
				$flot_ticks = \'[\'.implode(\',\',$dates).\']\';
				$flot_data_visitors = \'[\'.implode(\',\',$visitors).\']\';
				$flot_data_visits = \'[\'.implode(\',\',$visits).\']\';
				$flot_data_views = \'[\'.implode(\',\',$views).\']\';
				$output = \' <div class="sectionHeader">Яндекс.Метрика</div>
					<div class="sectionBody" id="ymdw" style="height:\'.$widget_height.\'px"></div>
					<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
					<script language="javascript" type="text/javascript" src="../assets/plugins/ymdw/jquery.flot.min-time.js"></script>
					<script language="javascript" type="text/javascript" src="../assets/plugins/ymdw/jquery.flot.spline.min.js"></script>
					<style>
					.flot-x-axis .flot-tick-label {
					-o-transform: rotate(\'.$rotate_xlabels.\'deg);
					-webkit-transform: rotate(\'.$rotate_xlabels.\'deg);
					-moz-transform: rotate(\'.$rotate_xlabels.\'deg);
					-ms-transform: rotate(\'.$rotate_xlabels.\'deg);
  					transform: rotate(\'.$rotate_xlabels.\'deg);
					padding:25px;
					}
					</style>
					<script type="text/javascript">
						$(document).ready(function() {
							var visitors = \'.$flot_data_visitors.\';
							var visits = \'.$flot_data_visits.\';
							var views = \'.$flot_data_views.\';
							var ticks = \'.$flot_ticks.\';
							$.plot($("#ymdw"),[{ label: "Визиты", data: visits, color:"#FCD202", lines: {show: false}, splines: {show: true, tension: 0.4}},
											   { label: "Просмотры", data: views, color:"#FF7711", lines: {show: false}, splines: {show: true, tension: 0.4}},
											   { label: "Посетители", data: visitors, color:"#C3B0FA", lines: {show: false}, splines: {show: true, tension: 0.4}}],
								{xaxis: {ticks : ticks}, points: { show: true },grid: {hoverable: true, backgroundColor: "#fffaff" }, legend: {margin: [20,10]}
							});
							function showTooltip(x, y, contents) {
								$("<div id=\\\'tooltip\\\'>" + contents + "</div>").css({
									position: "absolute",
									"z-index": 100,
									display: "none",
									top: y + 5,
									border: "1px solid #fdd",
									padding: "10px",
									"background-color": "#fee",
									opacity: 0.80
									}).appendTo("body");
									w = $("#tooltip").width();
									ow = $(".flot-base").width();
									x = (x + w + 5 > ow) ? (x - w - 10) : (x + 5);
									$("#tooltip").css("left",x).fadeIn(200);
								}

							var previousPoint = null;
							$("#ymdw").bind("plothover", function (event, pos, item) {
								if (item) {
									if (previousPoint != item.dataIndex) {
										previousPoint = item.dataIndex;
										$("#tooltip").remove();
										y = item.datapoint[1];
										showTooltip(item.pageX, item.pageY,
										item.series.label + ": " + y);
									}
								} else {
									$("#tooltip").remove();
									previousPoint = null;            
								}
							});
						});
					</script>\';
				foreach (glob(MODX_BASE_PATH . \'assets/cache/ymdw.widgetCache-*.php\') as $filename) {
   					unlink($filename);
				}
				file_put_contents(MODX_BASE_PATH . \'assets/cache/ymdw.widgetCache-\'.date(\'z\').\'.php\', $output);		
			}
		}
	}
	else{
		$output = file_get_contents( MODX_BASE_PATH . \'assets/cache/ymdw.widgetCache-\'.date(\'z\').\'.php\');
	}
	$e->output($output);
}';
$e = &$this->pluginEvent;
$e['OnBeforeDocFormSave'] = array('TVimageResizer','DirectResize');
$e['OnBeforeManagerLogin'] = array('Forgot Manager Login');
$e['OnBeforePluginFormSave'] = array('FileSource');
$e['OnBeforeSnipFormSave'] = array('FileSource');
$e['OnCacheUpdate'] = array('DirectResize');
$e['OnChunkFormRender'] = array('CodeMirror');
$e['OnDocFormPrerender'] = array('ManagerManager','DirectResize');
$e['OnDocFormRender'] = array('CodeMirror','ManagerManager','TVimageResizer');
$e['OnInterfaceSettingsRender'] = array('TinyMCE Rich Text Editor');
$e['OnLoadWebDocument'] = array('ajaxSubmit');
$e['OnLoadWebPageCache'] = array('ajaxSubmit');
$e['OnManagerAuthentication'] = array('Forgot Manager Login');
$e['OnManagerLoginFormRender'] = array('Forgot Manager Login');
$e['OnModFormRender'] = array('CodeMirror');
$e['OnParseDocument'] = array('PHx');
$e['OnPluginFormPrerender'] = array('FileSource');
$e['OnPluginFormRender'] = array('CodeMirror','FileSource');
$e['OnRichTextEditorInit'] = array('TinyMCE Rich Text Editor');
$e['OnRichTextEditorRegister'] = array('TinyMCE Rich Text Editor');
$e['OnSnipFormPrerender'] = array('FileSource');
$e['OnSnipFormRender'] = array('FileSource','CodeMirror');
$e['OnStripAlias'] = array('TransAlias');
$e['OnTempFormRender'] = array('CodeMirror');
$e['OnTVFormRender'] = array('ManagerManager');
$e['OnWebPageInit'] = array('SEO Strict URLs');
$e['OnWebPagePrerender'] = array('SEO Strict URLs','DirectResize');

