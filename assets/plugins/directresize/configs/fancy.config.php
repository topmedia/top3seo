<?php

$tpl = <<<HTML
<a class="fancy" href="[+dr.bigPath+]">
	<img src="[+dr.thumbPath+]" width="[+dr.thumbWidth+]" height="[+dr.thumbHeight+]" style="[+dr.style+]" alt="[+dr.alt+]" title="[+dr.title+]">
</a>
HTML;

$header  = 	'
';

/*
Uncomment the line below if you use Maxigallery with lightboxv2 effect in the same site
Раскомментируйте строку ниже, если вы используете Maxigallery с эффектом lightboxv2 на одном сайте
*/
//$maxigallery_jscss_packs = "lightboxv2";

$lightbox_mode = 2;

$allow_from="assets/images";

$resize_method = 0;
?>