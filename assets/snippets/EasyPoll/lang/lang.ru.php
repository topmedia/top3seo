<?php
/**
 * ------------------------------------------------------------------------------
 * English Language File for EasyPoll Snippet. Keep this File UTF-8 encoded
 * ------------------------------------------------------------------------------
 * @author banal
 * @version 0.3 <2008-02-20>
 */
$_lang['vote']			= 'Голос';
$_lang['results']		= 'Результаты';
$_lang['back']			= 'Назад к голосованию';
$_lang['alreadyvoted']	= 'Голосовать возможно один раз';
$_lang['totalvotes']	= 'Число голосов';
$_lang['error']			= 'Вы не выбрали ответ!';
?>