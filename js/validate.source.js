$(document).ready(function(){    
jQuery.validator.addMethod(
	"sites", function(value, element) {
		return this.optional(element) || /^((http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.|(.))(([a-z0-9\-]+(!?\.[a-z]{2,4}))|([\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(!?\.рф))))/.test(value);
	});
jQuery.validator.addMethod(
	"email", function(value, element) {
		return this.optional(element) || /^(([a-z0-9\-\_])+((!?\@[a-z0-9\-]+(!?\.[a-z]{2,4}))|(!?\@[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF\-]+(!?\.рф))))/.test(value);
	});


    $("#how_seoform").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			email:{
                required: true,
				email: true,
				maxlength: 100
            },
			url:{
                required: true,
				sites: true,
				maxlength: 1000
            }
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            email:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                email: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Пример Email: box@mail.com <br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			url:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				sites: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Адрес сайта введён некоректно<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 1000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
       }

    });	
	
	$("#orderseo").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			email:{
                required: true,
				email: true,				
				maxlength: 100
            },
			comment:{
			maxlength: 2000
			}
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            email:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                email: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Пример Email: box@mail.com <br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			comment:{
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 2000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
       }

    });	
	
	
		$("#orderCall").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			telephone:{
                required: true
            },
			time_call: {
			maxlength: 100
			}
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            telephone:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			time_call:{
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
       }

    });	
	
	
	$("#ServicesForm").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			email:{
                required: true,
				email: true,
				maxlength: 100
            },
			url:{
                required: true,
				sites: true,
				maxlength: 1000
            },
			comment:{
			maxlength: 2000
			}
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            email:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                email: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Пример Email: box@mail.com <br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			url:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				sites: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Адрес сайта введён некоректно<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 1000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
			},
			comment:{
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 2000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
            
       }

    });	
	
	$("#ServicesKontekstForm").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			email:{
                required: true,
				email: true,
				maxlength: 100
            },
			url:{
                required: false,
				sites: true,
				maxlength: 1000
            },
			comment:{
			maxlength: 2000
			}
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            email:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                email: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Пример Email: box@mail.com <br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			url:{
				sites: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Адрес сайта введён некоректно<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 1000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
			},
			comment:{
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 2000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
       }

    });	
	
	
	
	$("#feedback").validate({
	
		focusCleanup: true,
		success: "valid",
		focusInvalid: false,
   
       rules:{ 
            name:{
                required: true,
                minlength: 2,
				maxlength: 50
            },
			message:{
                required: true,
                minlength: 1,
				maxlength: 2000
            },
			subject:{
                required: true,
                minlength: 4,
				maxlength: 250
            },
			email:{
                required: true,
				email: true,
				maxlength: 100
            }
       },
       messages:{
            name:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Имя должно быть минимум 2 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 50 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			subject:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Тема сообщения должна быть минимум 4 символа<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 250 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
			message:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                minlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Сообщение не может быть пустым<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 2000 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            },
            email:{
                required: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Это поле обязательно для заполнения<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
                email: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Пример Email: box@mail.com <br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>",
				maxlength: "<div class=\"reqplaceholderformError parentFormformID formError\"><div class=\"formErrorContent\">Не больше 100 символов<br></div><div class=\"formErrorArrow\"><div class=\"line10\"><!-- --></div><div class=\"line9\"><!-- --></div><div class=\"line8\"><!-- --></div><div class=\"line7\"><!-- --></div><div class=\"line6\"><!-- --></div><div class=\"line5\"><!-- --></div><div class=\"line4\"><!-- --></div><div class=\"line3\"><!-- --></div><div class=\"line2\"><!-- --></div><div class=\"line1\"><!-- --></div></div></div>"
            }
       }

    });

}); //end of ready